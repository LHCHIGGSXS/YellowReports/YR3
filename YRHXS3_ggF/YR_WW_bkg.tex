\label{sec:WWbackground}

The most relevant background to the $H \to \PWp\PWm \to \Pl\PGnl \Pl\PGnl$ channel is the non resonant $\Pp\Pp \to \PWp\PWm \to \Pl\PGnl \Pl\PGnl$ process.
 
In the decay of the Higgs boson to W pairs, $\PW$ bosons 
have opposite spin orientation, since the Higgs has spin zero.  
In the weak decay of the $\PW$ boson, due to the V-A nature of the interaction, the 
positive lepton is preferably emitted in the direction of the $\PWp$ spin and the 
negative lepton in the opposite direction of the $\PWm$ spin.  Therefore the two 
leptons are emitted close to each other and their invariant mass $m_{\Pl\Pl}$ is small.  
This feature is used  in the ATLAS\cite{ATLAS-CONF-2013-030} analysis to define a low-signal control region 
(CR) through a cut on $m_{\Pl\Pl}$ of $50{-}100\UGeV$.  The event yield of the $\PW\PW$ background 
is computed in the control region  and extrapolated to 
the signal regions.  The signal region is divided in two $m_{\Pl\Pl}$ bins when the outcoming leptons belong to different families (different flavour in the following),  each one defining one signal region (SR1,2 in the following), while if the outcoming leptons belong to the same family only one signal region (SR) is defined.
 
The $\PW\PW$ yield in the signal regions  is therefore:

\begin{eqnarray}
  \label{eqn:alpha}
  N^{\mathrm{WW 0j}}_{\mathrm{SR} (1,2)} & = & \alpha^{1,2}_{\mathrm{0j}} N^{\mathrm{WW 0j}}_{\mathrm{CR}}, \nonumber\\
  N^{\mathrm{WW 1j}}_{\mathrm{SR (1,2)}} & = & \alpha_{\mathrm{1j}}
  N^{\mathrm{WW 1j}}_{\mathrm{CR}} \nonumber
\end{eqnarray}

where alpha is the ratio (evaluated with the MC simulation) of expected events in the signal region and the $\PW\PW$ control region. 
The uncertainty on $\alpha$ is dominated by theoretical uncertainties, since it is 
defined using only well-measured charged-lepton quantities. There are two separate 
$\PW\PW$ production processes to consider: $\PAQq\PQq \rightarrow \PW\PW$ and $gg \rightarrow 
\PW\PW$.  Since the $\PAQq\PQq \rightarrow \PW\PW$ process contributes $95\% (93\%)$ of the 
total $\PW\PW$ background in the 0-jet (1-jet) channel, uncertainties on this process 
are the most important and are evaluated in Sec.~\ref{sec:alpha}.

\subsubsection{Uncertainties for the 0-jet and 1-jet analyses}
\label{sec:alpha}

The $\PW\PW$ background is estimated  using event counts in a 
control region (CR) defined using cuts on the $m_{\Pl\Pl}$ variable.  In \Tref{tab:WW_preselection} we describe the preselection cuts, and in \Tref{tab:WW_cuts} we show the cuts used to define the signal regions (SR) and the 
$\PW\PW$ CR. Note that for both the different flavour (DF) and same flavour (SF) 
analyses, a DF CR is used to normalise to data.

\begin{table}
  \begin{center}
  \caption{Definition of the preselection cuts for the $\PW\PW$ studies.} 
  \label{tab:WW_preselection}
    \begin{tabular}{lcc}
      \hline
      & Different flavour & Same flavour \\
      \hline
      Exactly 2 leptons      & \multicolumn{2}{c}{\textbf{lepton:} $\pT > 15\UGeV$, $|\eta| < 2.47$} \\
      Leading lepton $\pT$   & \multicolumn{2}{c}{$> 25\UGeV$} \\
      $m_{\Pl\Pl}$               & $> 10\UGeV$     & $> 12\UGeV$ \\
      $\ET^{\mathrm miss}$       & $> 25\UGeV$     & $> 45\UGeV$ \\
      Jet binning            & \multicolumn{2}{c}{\textbf{jet:} $\pT > 25\UGeV$, $|\eta| < 4.5$} \\
      $\pT^{\Pl\Pl}$ (0~jet only) & \multicolumn{2}{c}{$> 30\UGeV$} \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
   \caption{Definitions of the signal regions (SR), control regions (CR), and validation 
    region (VR).  The cuts are in addition to the preselection cuts described in 
    Table~\ref{tab:WW_preselection}. Note that, for both the different-flavour and same-flavour 
    analyses, the CR is always defined in the different flavour channel.} 
  \label{tab:WW_cuts}
   \begin{tabular}{lcc}
      \hline
      & $m_{\Pl\Pl}$ & $\Delta \phi_{\Pl\Pl}$ \\
      \hline
      SR1 (DF)    & $12 - 30\UGeV$     & $< 1.8$ \\
      SR2 (DF)    & $30 - 50\UGeV$  & $< 1.8$ \\
      SR  (SF)    & $12 - 50\UGeV$     & $< 1.8$ \\      
      CR (DF 0j)  & $50 - 100\UGeV$ & -- \\
      CR (DF 1j)  & $> 80\UGeV$     & -- \\
      VR (DF 0j)  & $>100\UGeV$     & -- \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

The parameter $\alpha$ defined by:
\begin{equation}
\alpha_{\PW\PW} = \frac{N_{\PW\PW}^{\mathrm SR/VR}}{N_{\PW\PW}^{\mathrm{CR}}}
\label{eq:alpha}
\end{equation}

is used to predict the amount of $\PW\PW$ background in each signal or validation region (VR) from the 
data counts in the control region. The Validation Region is a signal free region non overlapping with the $\PW\PW$ CR, this region is used to test the validity of the extrapolation procedure on data. The $\alpha$ parameters are evaluated independently for the 0-jet and 1-jet bins, the same-flavor and 
different-flavor analyses, and in the two signal regions in the different-flavor analysis.  \\

\noindent
The non resonant $\Pp\Pp \to \PW\PW^{(*)}$ process is simulated with the \textsc{Powheg} Monte Carlo program interfaced 
to \textsc{pythia}{}8 for parton showering. \textsc{Powheg} computes the process $\Pp\Pp \to \PW\PW^{(*)} \to \Pl\PGnl \Pl\PGnl$ 
at NLO including off-shell  contributions.
The calculation includes the ``single-resonant'' process where the process $\Pl \to \PW \PGnl, 
\PW\to \Pl \PGnl$  
happens from a lepton of a ``single resonant'' $\PZ$ boson decay.  Uncertainties on 
the $\alpha$ parameters arise from PDF modelling, missing orders in the perturbative 
calculation, parton shower modelling, and the merging of the fixed-order calculation with 
the parton-shower model.  

\subsubsection{PDF uncertainties}
In order to evaluate the PDF uncertainties, we used $90\%$ C.L. CT10 PDF eigenvectors
and the  PDF parametrizations from MSTW2008 and NNPDF2.3. The last two are significantly smaller than the CT10 uncertainty. 
We take the quadrature sum of the 
CT10 uncertainties and the differences with respect to other parametrizations as the PDF uncertainty.  
A summary of $\alpha$ values and uncertainties are shown in \Tref{tab:pdfs}. This methods gives uncertainty bands close enough to the envelope method but allows
to compute the spread respect to the central PDF set that is used in the full MC simulation. The envelope method cannot be applied because it is not possible to generate a
sample with a PDF set that exactly matches the central value.


\begin{table}
  \begin{center}
  \caption{The $\alpha$ parameters for the standard analysis computed using 
    different PDF sets and the spread obtained using the CT10 error set.  The signs 
    indicate the difference with respect to the CT10 central value and show the 
    correlated differences in the different regions.  }
  \label{tab:pdfs}
    \begin{tabular}{lccc}
      \hline
      & CT10 error set & MSTW2008 & NNPDF2.3 \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & 1.4\% & 0.01\%  & -0.5\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & 1.0\% & -0.02\% & -0.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & 1.1\% & -0.01\% & -0.4\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & 1.8\% &  0.6\%  & -0.5\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & 1.6\% &  0.5\%  & -0.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & 1.6 \% & 0.5\%  & -0.4\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{The $\alpha$ parameters for the low $\pT$~analysis computed using different PDF 
    sets and the spread obtained using the CT10 error set.}
  \label{tab:pdfslowpt}
    \begin{tabular}{lccc}
      \hline
      & CT10 error set & MSTW2008 & NNPDF2.3 \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & 1.7\% & 0.00\% & -0.6\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & 1.2\% & 0.04\% & -0.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & 1.4\% & 0.01\% & -0.6\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & 1.9\% & 0.4\%  & -0.5\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & 1.7\% & 0.6\%  & -0.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & 1.7\% & 0.4\%  & -0.6\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\subsubsection{Renormalization and factorization scales}
The uncertainty from the missing higher-order terms in the calculation of the production process 
can affect the $\pT$ distribution of the $\PW\PW^{*}$ pair and the $\alpha$ values.
The MCFM generator was used to estimate their effect  producing samples with renormalisation 
($\muR$) and factorisation ($\muF$) scale variations. The renormalisation scales are defined
as $\muR = \xi_{\mathrm R} \mu_{0}$ and $\muF = \xi_{\mathrm F} \mu_0$, where $\mu_0$ is a dynamic scale 
defined as $\mu_0 = m_{\PW\PW}$.

The scale variations were calculated using 20  million `virtual' integrations 
and $\approx 100$ million `real' integrations. In order to obtain an estimate of the 
statistical uncertainty on the $\alpha$ value produced by each scale variation, each sample 
was used individually to give an estimate of $\alpha$ and the central limit theorem was 
used to evaluate the uncertainty on the mean value. This gives a statistical uncertainty 
of around $0.4\%$ for the 0-jet case (the 1 jet case has negligible statistical uncertainty).

The nominal scale is obtained with $\xi_{\mathrm R} = \xi_{\mathrm F} = 1$ and the scale uncertainties
are obtained by varying $\xi_{\mathrm R}$ and $\xi_{\mathrm F}$ in the range $1/2 - 2$ while keeping
$\xi_{\mathrm R}/\xi_{\mathrm F}$ between $1/2$ and $2$; the maximum deviation from the nominal value 
is then taken as the scale uncertainty.  The scale uncertainties on $\alpha$ are shown 
in \Tref{tab:scale_and_pdfs}, where we summarize also the PDF, parton-shower, and 
modelling uncertainties.  The correlation between the $\alpha$ parameters of the 0-jet 
and 1-jet analyses is also evaluated in the calculation and their values are found to be 
fully correlated.

In order to ensure that we are not missing a localised large deviation in $\alpha$ on 
the $\muR - \muF$ plane and hence underestimating the scale uncertainty, we used the MCFM 
to simulate cases where $\xi_R$ and $\xi_F$ were equal to $3/4$ and $3/2$, but still 
fulfilling the same requirements as mentioned previously.  This corresponded to 12 more 
points in the $\muR - \muF$ plane, and for each point we generated 5 files with the 
same number of events as before.  Although this resulted in maximum deviations that were 
slightly larger than in the nominal variations, these deviations are within the statistical 
uncertainty evaluated using the central limit theorem. \Tref{tab:WW-extrascaleunc-alphaDF1}, 
\ref{tab:WW-extrascaleunc-alphaDF2} and \ref{tab:WW-extrascaleunc-alphaSF} summarise the results.

\begin{table}
  \begin{center}
  \caption{Values of $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) as the renormalisation scale 
    (columns) and factorisation scale (rows) were varied. Statistical uncertainties
    in these vales are also shown.}
  \label{tab:WW-extrascaleunc-alphaDF1}
    \makebox[\linewidth]{
      \begin{tabular}{lccccc}
        & $\mu_0/2$         & $3\mu_0/2$        & $\mu_0$           & $3\mu_0/2$        & $2\mu_0$          \\
        \hline
        $\mu_0/2$  & $0.2261\pm0.21\%$ & $0.2250\pm0.26\%$ & $0.2248\pm0.17\%$ & --                & --                \\
        $3\mu_0/2$ & $0.2274\pm0.67\%$ & $0.2250\pm0.42\%$ & $0.2244\pm0.22\%$ & $0.2242\pm0.36\%$ & --                \\
        $\mu_0$    & $0.2264\pm0.32\%$ & $0.2254\pm0.26\%$ & $0.2247\pm0.22\%$ & $0.2243\pm0.26\%$ & $0.2232\pm0.23\%$ \\
        $3\mu_0/2$ & --                & $0.2248\pm0.24\%$ & $0.2255\pm0.48\%$ & $0.2237\pm0.19\%$ & $0.2233\pm0.19\%$ \\
        $2\mu_0$   & --                & --                &
  $0.2249\pm0.21\%$ & $0.2240\pm0.43\%$ & $0.2239\pm0.24\%$ \\
\hline
      \end{tabular}
    }
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{Values of $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) as the renormalisation scale 
    (columns) and factorisation scale (rows) were varied. Statistical uncertainties
    in these vales are also shown.}
  \label{tab:WW-extrascaleunc-alphaDF2}
    \makebox[\linewidth]{
      \begin{tabular}{lccccc}
        & $\mu_0/2$ & $3\mu_0/4$ & $\mu_0$ & $3\mu_0/2$ & $2\mu_0$\\
        \hline
        $\mu_0/2$ & $0.3846\pm0.22\%$ & $0.3816\pm0.19\%$ & $0.3828\pm0.23\%$ & -- & --\\
        $3\mu_0/4$ & $0.3856\pm0.64\%$ & $0.3839\pm0.40\%$ & $0.3821\pm0.29\%$ & $0.3801\pm0.37\%$ & --\\
        $\mu_0$ & $0.3849\pm0.24\%$ & $0.3819\pm0.42\%$ & $0.3833\pm0.24\%$ & $0.3805\pm0.30\%$ & $0.3810\pm0.24\%$\\
        $3\mu_0/2$ & -- & $0.3810\pm0.28\%$ & $0.3823\pm0.41\%$ & $0.3805\pm0.32\%$ & $0.3794\pm0.24\%$\\
        $2\mu_0$ & -- & -- & $0.3848\pm0.43\%$ & $0.3808\pm0.45\%$ & $0.3826\pm0.24\%$\\
\hline
      \end{tabular}
    }
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{Values of $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$ as the renormalisation scale 
    (columns) and factorisation scale (rows) were varied. Statistical uncertainties
    in these vales are also shown.}
  \label{tab:WW-extrascaleunc-alphaSF}
    \makebox[\linewidth]{
      \begin{tabular}{lccccc}
        & $\mu_0/2$ & $3\mu_0/4$ & $\mu_0$ & $3\mu_0/2$ & $2\mu_0$\\
        \hline
        $\mu_0/2$ & $0.4694\pm0.45\%$ & $0.4665\pm0.43\%$ & $0.4675\pm0.39\%$ & -- & --\\
        $3\mu_0/4$ & $0.4713\pm0.63\%$ & $0.4667\pm0.43\%$ & $0.4650\pm0.42\%$ & $0.4640\pm0.42\%$ & --\\
        $\mu_0$ & $0.4698\pm0.40\%$ & $0.4675\pm0.47\%$ & $0.4689\pm0.52\%$ & $0.4645\pm0.39\%$ & $0.4657\pm0.48\%$\\
        $3\mu_0/2$ & -- & $0.4663\pm0.39\%$ & $0.4659\pm0.44\%$ & $0.4647\pm0.37\%$ & $0.4639\pm0.40\%$\\
        $2\mu_0$ & -- & -- & $0.4674\pm0.41\%$ & $0.4655\pm0.59\%$ & $0.4659\pm0.40\%$\\
\hline
      \end{tabular}
    }
  \end{center}
\end{table}

Scale uncertainties were alternatively evaluated with the \textsc{aMC@NLO} generator 
 varying $\xi_{\mathrm R}$ and $\xi_{\mathrm F}$ in the range $1/2 - 2$ while keeping
$\xi_{\mathrm R}/\xi_{\mathrm F}$ between $1/2$.  These uncertainties are summarized in 
\Tref{tab:WW_amcatnlo_scale_unc} and are statistically consistent with those of 
MCFM.  

\begin{table}
  \begin{center}
  \caption{The uncertainty on the $\PW\PW$ extrapolation parameters, $\alpha$, calculated by varying the renormalisation
    and factorisation scales with the a\textsc{MC@NLO} generator.  The statistical uncertainty is included as an uncertainty
    on the uncertainty.}
  \label{tab:WW_amcatnlo_scale_unc}
    \begin{tabular}{lc}
      \hline
      & Maximum Deviation \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & $1.7 \pm 0.7\%$ \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & $0.6 \pm 0.6\%$ \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & $1.0 \pm 0.5\%$ \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & $3.4 \pm 1.1\%$ \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & $1.4 \pm 0.9\%$ \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & $2.3 \pm 0.8\%$ \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\subsubsection{Generator modelling}
The $\alpha$ predictions using various generators was studied to get a range
encompassing different orders of perturbative calculation and different models of
parton showering and the associated merging with the fixed-order calculation. 
We conservatively assign a modelling uncertainty on the difference in $\alpha$ predicted 
between the best available pair of generators in terms of the fixed-order calculation.
\textsc{Powheg + Pythia8} and \textsc{MCFM} have been compared for this study. 
\textsc{MCMF} is a pure parton level MC that is not matched to a parton showering algorithm.  However, other effects included in this uncertainty 
might involve e.g. different renormalisation and factorisation scales, or different 
electroweak schemes.  The differences in $\alpha$ between the generators and the 
assigned uncertainties are shown in \Tref{tab:WW_powheg_mcfm_comparison}.

\begin{table}
  \begin{center}
  \caption{The $\PW\PW$ extrapolation parameters, $\alpha$, calculated using \textsc{powheg + pythia 8} 
    and MCFM.  The 0-jet and 1-jet, different-flavour (DF) and same-flavour (SF) values are each 
    calculated.}
  \label{tab:WW_powheg_mcfm_comparison}
    \begin{tabular}{lccc}
      \hline
      & POWHEG + Pythia 8 & MCFM & $\delta\alpha$ \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & 0.2277  & 0.225 &  -1.2\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & 0.3883  & 0.383 &  -1.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & 0.4609  & 0.469 &  +1.7\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & 0.1107  & 0.105 &  -5.1\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & 0.1895  & 0.180 &  -5.0\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & 0.2235  & 0.217 &  -3.1\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{The $\PW\PW$ extrapolation parameters, $\alpha$, calculated using \textsc{powheg + herwig} 
    and \textsc{aMC@NLO}.  The 0-jet and 1-jet, different-flavour (DF) and same-flavour (SF) values 
    are each calculated.}
  \label{tab:WW_powheg_amcatnlo_comparison}
    \begin{tabular}{lccc}
      \hline
      & POWHEG + Herwig & aMC@NLO & $\delta\alpha$ \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & 0.2277 & 0.2274 & -0.4\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & 0.3914 & 0.3845 & -1.7\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & 0.4623 & 0.4581 & -0.9\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & 0.1113 & 0.1064 & -4.3\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & 0.1904 & 0.1840 & -3.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & 0.2247 & 0.2122 & -5.6\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

The modelling uncertainties were computed also for the extrapolation from the signal region to an higher $m_{\Pl\Pl}$ region (validation region)
used to test the uncertainty prescription. The validation region is  defined by $m_{\Pl\Pl} > 100\UGeV$ after preselection. The results are 
shown in \Tref{tab:WW_powheg_mcfm_comparison_validation}.

\begin{table}
  \begin{center}
  \caption{The $\PW\PW$ extrapolation parameters, $\alpha$, calculated for the validation 
    region using \textsc{Powheg + Pythia8} and \textsc{MCFM}.  The 0 jet and 1 jet, different 
    flavour (DF) values are each calculated. }
  \label{tab:WW_powheg_mcfm_comparison_validation}
    \begin{tabular}{lccc}
      \hline
      & \textsc{POWHEG + Pythia} 8 & \textsc{MCFM} & $\delta\alpha$ \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (VR) & 0.9420 & 0.961 & 2.0\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{The $\PW\PW$ extrapolation parameters, $\alpha$, calculated with respect to the 
    validation region using \textsc{powheg + Herwig} and \textsc{aMC@NLO}.  The 0 jet and 1 jet, 
    different flavour (DF) values are each calculated. }
  \label{tab:WW_powheg_amcatnlo_comparison_validation}
    \begin{tabular}{lccc}
      \hline
      & POWHEG + Herwig & aMC@NLO & $\delta\alpha$ \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (VR) &  0.9177 & 0.9738 & 6.1\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
The uncertainty in the shape of the $m_{\mathrm T}$ distribution in the signal region arising from 
higher order corrections was estimated by varying the renormalisation and factorisation 
scales in MCFM, in a similar fashion to that used when calculating the uncertainty in 
$\alpha$. In addition, the uncertainty in the shape of the \mT~distribution due to the 
underlying event and parton showering is evaluated by comparing events generated with 
\textsc{Powheg} and showered with \textsc{pythia}{}8 to those same events showered with \textsc{herwig}.
These shape systematics are shown in Figures~\ref{fig:ww_mT_scaleunc},  
~\ref{fig:ww_mT_psueunc}, and ~\ref{fig:ww_mll_psueunc}. The largest
systematics are at low $m_{\mathrm T}$ and at high $m_{\mathrm T}$, on the tails of the distribution, while the core
part and in particular the region around the Higgs mass $m = 125 \UGeV$ is not affected by large systematics.


\begin{figure}[h!]
  \centering
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_DF_SR1_0jet}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_DF_SR2_0jet}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_DF_SR1_1jet}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_DF_SR2_1jet}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_SF_SR_0jet}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/WW_SF_SR_1jet}
  \caption{Scale uncertainties in the $m_{\mathrm T}$ distribution in (top left) SR1 in DF 0j 
    analysis, (top right) SR2 in DF 0j analysis, (middle left) SR1 in DF 1j analysis, 
    (middle right) SR2 in DF 1j analysis, (bottom left) SR in SF 0j analysis, and (bottom 
    right) SR in SF 1j analysis.}
  \label{fig:ww_mT_scaleunc}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr1_df_0j}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr2_df_0j}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr1_df_1j}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr2_df_1j}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr_sf_0j}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape/h_trans_mass_sr_sf_1j}
  \caption{Parton showering/modelling uncertainties in the $m_{\mathrm T}$ distribution in (top left) SR1 
    in DF 0j analysis, (top right) SR2 in DF 0j analysis, (middle left) SR1 in DF 1j analysis, 
    (middle right) SR2 in DF 1j analysis, (bottom left) SR in SF 0j analysis, and (bottom 
    right) SR in SF 1j analysis.  \textsc{powheg} + \textsc{pythia}{}8/\textsc{Herwig} is shown along with \textsc{mcfm}. 
    All ratios are with respect to \textsc{powheg} + \textsc{pythia}{}8.}
  \label{fig:ww_mT_psueunc}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mllshape/h_mll10_sr1_df_0j_nodphi}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mllshape/h_mll10_sr1_df_1j_nodphi}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mllshape/h_mll10_sr_sf_0j_nodphi}
  \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mllshape/h_mll10_sr_sf_1j_nodphi}
  \caption{Parton showering/modelling uncertainties in the $m_{\Pl\Pl}$ distribution in (top left) SR1/2 
    in DF 0j analysis, (top right) SR1/2 in DF 1j analysis, (bottom left) SR in SF 0j analysis, and (bottom 
    right) SR in SF 1j analysis.  \textsc{powheg} + \textsc{pythia}{}8/\textsc{Herwig} is shown along with \textsc{mcfm}.
    All ratios are with respect to \textsc{powheg} + \textsc{pythia}{}8.}
  \label{fig:ww_mll_psueunc}
\end{figure}

Various additional generators have been used to investigate the modelling of the $\PW\PW$ background,
Events generated with \textsc{POWHEG} are compared to those generated with Sherpa, \textsc{MC@NLO} + \textsc{Herwig}, and \textsc{MCFM}.
Between those generators, some differences are existing.
The generators have the following features:
\begin{itemize}
\item \textsc{Powheg}: NLO calculation matched to Sudakov factor for first emission
\item \textsc{mcfm}: NLO calculation with no parton shower
\item \textsc{mc@nlo} + \textsc{Herwig}: NLO calculation with parton shower but no
  ``singly resonant'' diagrams
\item Sherpa: LO calculation with parton shower
\end{itemize}
These studies offer insight into the effect of including NLO contributions, 
single-resonant diagrams, or the parton shower in the model.

\noindent
The events are generated at a center of mass energy of 7 TeV using CTEQ 6.6 parton
distribution functions (CTEQ 6.1 for Sherpa).  
Figure~\ref{fig:mtsr_dphicr} shows the $m_{\mathrm T}$ distribution in the signal region.
%%  and
%% the $\Delta\phi (ll)$ distribution in the control region.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[width=0.475\textwidth]{YRHXS3_ggF/wwbackground/mTshape2/mT_standard_emu}
    %%\includegraphics[width=0.475\textwidth]{mTshape2/dphicr}
  \end{center}
%%   \caption{Distributions of $m_{\mathrm T}$ in the signal region (left) and $\Delta \phi(ll)$ in the
%%     control region (right), for the four generators considered.}
  \caption{Distributions of $m_{\mathrm T}$ in the signal region for the four generators considered.}
  \label{fig:mtsr_dphicr}
\end{figure}
%
The ratio of the \textsc{mc@nlo} to \textsc{mcfm} $m_{\mathrm T}$ distributions is taken as an uncertainty
in the final $m_{\mathrm T}$ shape fit (the uncertainty is symmetrized).  
\clearpage
\subsubsection{Parton shower modelling}

Parton showering effect is studied by comparing the measured $\alpha$ values 
(eqn.~\ref{eqn:alpha}) when using the nominal matrix element event
generator, \textsc{PowHeg}, interfaced with either \textsc{pythia}{}8, \textsc{pythia}{}6, or
\textsc{Herwig}.
One million events were generated with \textsc{PowHeg} in each of the
$\PW\PW\rightarrow \Pe\Pe/\PGm\PGm/\Pe\PGm/\PGm \Pe$ final states, yielding a total of 4 million events.
The $\alpha$ values are computed in both the same flavor and different flavor analyses
separately for 0, 1, and $\ge 2$ jet events.  The jet multiplicity distributions after
preselection for the different generators are shown in Fig.~\ref{fig:jetmult}.

\begin{figure}[h!]
  \includegraphics[width=0.9\textwidth]{YRHXS3_ggF/wwbackground/WWbackground/h_jet_n__pre_cut_logY_lp}
  \caption{The jet multiplicity distributions for the four generators considered. }
  \label{fig:jetmult}
\end{figure}

\Tref{tab:alpha_shower} shows the measured alpha values for each showering
program split into the two jet bins for both the same flavor and opposite flavor
analyses.  \Tref{tab:alpha_shower_diff} shows the relative difference
between each showering program and the nominal, \textsc{pythia}{}8. In general,
\textsc{pythia}{}{}8 and \textsc{pythia}{}6 are consistent with each other.  
The measured systematic uncertainties for choice of parton showering is determined from
the difference between \textsc{pythia}{}8 and \textsc{Herwig}.  For both analyses, the
observed shift is about 4.5\% for the 0-jet bin, while for the other jet bins, the
observed shift is consistent with zero (except for the $\ge 2$-jet bin in the
opposite flavor analysis).  We conservatively take 4.5\% as a correlated uncertainty
for both 0-jet and 1-jet bins, since further study is required to appropriately assess
the uncertainty in the 1-jet bin.

\begin{table}[htbp]
  \begin{center}
  \caption{The $\alpha_{\PW\PW}$ values for \textsc{PowHeg} generated events showered with
    \textsc{pythia}{}6, \textsc{pythia}{}8, or \textsc{Herwig} are shown.  The values are shown both
    for same flavor and opposite flavor analyses in either the 0, 1, or $\ge$ 2 jet-bins.
    For reference, the $\alpha$ value is also shown in events generated with \textsc{mc@nlo}
    and showered with \textsc{Herwig}.}
  \label{tab:alpha_shower}
    \makebox[\linewidth]{
      \begin{tabular}{llccc}
        \hline
            {SR vs CR} & UEPS & \multicolumn{2}{c}{DF} &   SF \\
            &      &                                  SR1   & SR2   &    \\
            \hline
            \multirow{3}{*}{0-jet} & \textsc{PowHeg}+\textsc{pythia}{}6 & $0.2275 \pm 0.0006$ &$0.3877 \pm 0.0008$ & $0.4602 \pm 0.0009$\\
            & \textsc{PowHeg}+\textsc{pythia}{}8 & $0.2277 \pm 0.0003$ &$0.3883 \pm 0.0004$ & $0.4609 \pm 0.0004$ \\
            & \textsc{PowHeg}+\textsc{Herwig}  & $0.2277 \pm 0.0003$ &$0.3914 \pm 0.0004$ & $0.4623 \pm 0.0004$ \\
            & \textsc{mc@nlo}+\textsc{Herwig}  & $0.229 \pm 0.003$   &$0.385 \pm 0.004$   & $0.4597 \pm 0.0043$ \\
            \hline                                                                                       
            \multirow{3}{*}{1-jet} & \textsc{PowHeg}+\textsc{pythia}{}6 & $0.1098 \pm 0.0004$ &$0.1895 \pm 0.0006$ & $0.2232 \pm 0.0007$ \\
            & \textsc{PowHeg}+\textsc{pythia}{}8 & $0.1107 \pm 0.0002$ &$0.1895 \pm 0.0003$ & $0.2235 \pm 0.0003$ \\
            & \textsc{PowHeg}+\textsc{Herwig}  & $0.1113 \pm 0.0002$ &$0.1904 \pm 0.0003$ & $0.2247 \pm 0.0003$ \\
            & \textsc{mc@nlo}+\textsc{Herwig}  & $0.1071 \pm 0.0021$ &$0.179 \pm 0.003$   & $0.211 \pm 0.003$ \\
            \hline
      \end{tabular}
    }
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{Systematic uncertainty between nominal (\textsc{PowHeg} + \textsc{pythia}{}8) vs. the
    other PDF showering tools and \textsc{mc@nlo}+\textsc{Herwig}.  The systematic uncertainty is
    computed by subtracting \textsc{pythia}{}6 or \textsc{Herwig} from \textsc{pythia}{}8 and then dividing
    the difference by \textsc{pythia}{}8.}
  \label{tab:alpha_shower_diff}
    \makebox[\linewidth]{
      \begin{tabular}{llcccc}
        \hline
            {SR vs CR} & UEPS & \multicolumn{2}{c}{DF} &   SF \\
            &      &                                  SR1   & SR2   &          \\
            \hline
            \multirow{3}{*}{0-jet} & \textsc{PowHeg}+\textsc{pythia}{}6 &  $0.1   \pm 0.3 \%$  & $0.2  \pm 0.2 \%$   & $0.14 \pm 0.2 \%$\\
            & \textsc{PowHeg}+\textsc{Herwig}  &  $-0.02 \pm 0.2 \%$  & $-0.8 \pm 0.1 \%$   & $-0.3 \pm 0.1 \%$\\
            & \textsc{mc@nlo}+\textsc{Herwig}  &  $-0.3  \pm 1.2 \%$  & $0.8 \pm 1.0 \%$    &  $0.3 \pm 0.9 \%$\\
            \hline
            \multirow{2}{*}{1-jet} & \textsc{PowHeg}+\textsc{pythia}{}6 & $0.8 \pm 0.4 \%$  & $-0.1 \pm 0.4 \%$ &  $0.1 \pm 0.3 \%$\\
            & \textsc{PowHeg}+\textsc{Herwig} &  $-0.5 \pm 0.3 \%$ & $-0.5 \pm 0.2 \%$ &  $-0.6 \pm 0.2 \%$\\
            & \textsc{mc@nlo}+\textsc{Herwig} &  $3.2 \pm 2.0 \%$  & $5.8 \pm 1.6 \%$  &  $5.6 \pm 1.5 \%$\\
            \hline
      \end{tabular}
    }
  \end{center}
\end{table}


\subsubsection{Summary}

The theoretical uncertainties on the normalization in the signal and validation regions are 
summarized in Tables~\ref{tab:scale_and_pdfs} and~\ref{tab:alpha_validation_uncertainties}.  
The modelling uncertainty has been checked with aMC@NLO; the differences between \textsc{Powheg} 
and aMC@NLO are generally smaller than, or the same within uncertainties, those between 
\textsc{Powheg} and \textsc{mcfm}.

\begin{table}
  \begin{center}
  \caption{Scale, PDF, parton-shower/underlying event, and modelling uncertainties on the $\PW\PW$
    extrapolation parameters $\alpha$ for the NLO $\PAQq\PQq,\PQq\Pg \to \PW\PW$ processes; the errors are taken
    to be fully correlated between the 0-jet and 1-jet bins.  The correlations in the parton-shower
    and modelling variations are shown explicitly by including the signed difference in the comparison.}
  \label{tab:scale_and_pdfs}
    \begin{tabular}{lcccc}
      \hline
      & Scale & Parton-shower & PDFs & Modelling  \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR1) & 0.9\% & +0.2 \% & 1.5\% & -1.2\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (SR2) & 0.9\% & +0.8\% & 1.1\% & -1.4\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{0j}}$       & 1.0\% & +0.3\% & 1.1\% & 1.7\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR1) & 1.6\% & +0.5\% & 2.0\% & -5.1\% \\
      $\alpha^{\mathrm{DF}}_{\mathrm{1j}}$ (SR2) & 1.5\% & +0.5\% & 1.8\% & -5.0\% \\
      $\alpha^{\mathrm{SF}}_{\mathrm{1j}}$       & 1.4\% & +0.6\% & 1.7\% & -3.1\% \\
      \hline
      $\alpha^{\mathrm{0j}}_{\mathrm{WW}}$, $\alpha^{\mathrm{1j}}_{\mathrm{WW}}$ correlation & \multicolumn{4}{c}{1} \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}
  \begin{center}
  \caption{Scale, PDF, parton-shower/underlying event, and modelling uncertainties on the $\PW\PW$
    extrapolation parameters $\alpha$ calculated relative to the validation region
    for the NLO $\PQq\PQq,\PQq\Pg \to \PW\PW$ processes.}
  \label{tab:alpha_validation_uncertainties}
    \begin{tabular}{lcccc}
      \hline
      & scale & parton-shower & PDFs & modelling  \\
      \hline
      $\alpha^{\mathrm{DF}}_{\mathrm{0j}}$ (VR) & 1.0\% & 2.6\%  & 2.2\% & 2.0\% \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

