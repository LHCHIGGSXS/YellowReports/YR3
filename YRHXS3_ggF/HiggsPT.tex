In the search  of the Higgs boson  and the measurement of the Higgs boson
production yield in the $\PH \to \PGg\PGg$ and $\PH \to \PZ\PZ^* \to 4\Pl$
channel, 
the Higgs transverse momentum is of particular interest because it affects the
signal acceptance due to the cuts applied on the photon and lepton
momenta. Moreover the signal purity can be increased by cutting on the
transverse momentum of the di-boson system  that is on average larger in the
Higgs decay than the non resonant background due to the higher  jet activity
in the Higgs gluon fusion production process than the di-photon and $\PZ\PZ$ non
resonant backgrounds. Such techniques are applied in the $\PH \to \PGg \PGg$
search \cite{ATLAS-CONF-2012-091} and under study in the $\PH \to \PZ\PZ \to 4\Pl$ and
$\PH \to \PGt \PGt$ searches. It is therefore important to understand the impact
of several contributions to the Higgs $\pT$ and how they affect the
$\pT$ spectrum predicted  by the MC generators. 

In the present section we show a comparison of the Higgs $\pT$
distribution between \textsc{ MC@NLO 4.09} \cite{Frixione:2003ei},
using \textsc{ HERWIG} 6.5 \cite{Corcella:2000bw} for the showering,
and  \textsc{POWHEG} \cite{Bagnaschi:2011tu,Nason:2004rx,Frixione:2007vw,Alioli:2010xd} that has been
showered with both \textsc{PYTHIA8} \cite{Sjostrand:2006za,Sjostrand:2007gs}
and \textsc{HERWIG 6.5}. The signal process is $\Pp\Pp \to \PH \to \PZ\PZ^{*}$ at the mass $\MH = 125.5\UGeV$. This value has been chosen being the 
last ATLAS best fit value \cite{ATLAS-CONF-2013-014}. \textsc{ HERWIG} 6.5 has been interfaced to  \textsc{ Jimmy} \cite{Butterworth:1996zw} for the underlying event simulation.
The ATLAS AUET2\cite{ATLAS-PHYS-PUB-2011-008} tune using the CTEQ 10 NLO pdfs in the showering algorithm has been used. Both \textsc{ MC@NLO 4.09} and \textsc{ POWHEG} include the heavy quark mass effect
in the gluon gluon loop for the Higgs $\pT$ determination. The contribution is available for t,b and  c quarks in  \textsc{ POWHEG} and for the $t$ and  $b$ quarks in \textsc{ MC@NLO}.
In the present section the contribution from the $c$ quark has been switched off in  \textsc{ POWHEG} so that the quark mass effects refer to the contribution 
from the top and bottom quarks.
The $\pT$ dampening factor \emph{hfact} has been set to $m_H/1.2$ in the \textsc{ POWHEG} case. A configuration  without the \emph{hfact} has also been studied.
The top mass has been set at $m_{\PQt} = 172.5\UGeV$   and the bottom quark mass at $m_{\PQb} = 4.75\UGeV$. The generation has been performed using the CT10 pdf set.

In \Fref{fig:pT_long_range} the Higgs $\pT$ distribution is shown with different configurations.
\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS3_ggF/pT_v1.eps}
\includegraphics[width=0.45\textwidth]{YRHXS3_ggF/pT_v1_b_ratio_0_40.eps}
\end{center}
\caption{Higgs $\pT$ distribution using different MC generators and
parton shower configurations. POWHEGPythia indicates \textsc{POWHEG + PYTHIA8}, POWHEGJimmy indicates \textsc{POWHEG + HERWIG6} and \textsc{Jimmy} for the underlying event simulation. The \emph{no q mass} distribution corresponds to a configuration where both \emph{hfact} and the Heavy Quark mass effect are switched off.} \label{fig:pT_long_range}
\end{figure}
In the  range, $0{-}400\UGeV$, we observe that the \textsc{ MC@NLO} spectrum is softer than the \textsc{ POWHEG + PYTHIA8} one. Comparing \textsc{ POWHEG+HERWIG}  with \textsc{ POWHEG + PYTHIA8} is possible to see that the parton shower doesn't affect the high Higgs $\pT$ tail
as expected.  The use of the \emph{hfact} dampening factor 
makes the \textsc{ POWHEG} spectrum closer to the \textsc{ MC@NLO} one at high $\pT$ but still significantly harder. The heavy quark mass effect has been switched off
in the \textsc{ POWHEG + HERWIG} sample in order to estimate the size of the effect. The contribution  is visible in the very high $\pT$ tail ($\pT > 250\UGeV$) but doesn't seem responsible for the main
high $\pT$ behaviour. In order to compare the generators at low $\pT$ the same figure has been zoomed in the range 0-40 GeV. For $\pT < 15\UGeV$ differences between the \textsc{ HERWIG} and \textsc{ PYTHIA8} showering are visible and in the very low $\pT$ region ($\pT < 10\UGeV$), \textsc{ MC@NLO + HERWIG} and \textsc{ POWHEG + HERWIG} with the \emph{hfact} are compatible while both the \textsc{ PYTHIA8} showering and the \emph{no hfact} configuration are different from the \textsc{ MC@NLO} prediction. This shows that at low $\pT$ the parton shower has a relevant role but the dampening factor correction is still important and brings the \textsc{ POWHEG + HERWIG} spectrum in  agreement with \textsc{ MC@NLO 4.09}.
 
