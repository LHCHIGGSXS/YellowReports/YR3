We describe in this section a first calculation of Higgs-boson production in association with a jet at next-to-next-to-leading order in perturbative QCD~\cite{Boughezal:2013uia}.  This result is urgently needed in order to reduce the theoretical uncertainties hindering a precise extraction of the Higgs properties at the LHC.  Currently, the theoretical errors in the one-jet bin comprise one of the largest systematic errors in Higgs analyses, particularly in the WW final state.  There are two theoretical methods one can pursue to try to reduce these uncertainties.  The first is to resum sources of large logarithmic corrections to all orders in QCD perturbation theory.  An especially pernicious source of large logarithmic corrections comes from dividing the final state into bins of exclusive jet multiplicities.  An improved theoretical treatment of these terms has been pursued in both the zero-jet~\cite{Berger:2010xi, Banfi:2012yh, Becher:2012qa, Tackmann:2012bt, Banfi:2012jm} and one-jet~\cite{Liu:2012sz, Liu:2013hba} bins (see \refS{sec:jets}).  The second, which we discuss here, is to compute the higher-order corrections to the next-to-next-to-leading order (NNLO) in perturbative QCD.  Both are essential to produce the reliable results necessary in experimental analyses.   In this contribution we give a brief overview of the calculational framework that was used to obtain the NNLO calculation for Higgs plus jet production, and present initial numerical results arising from gluon-fusion.

\subsubsection{Notation and setup}
\label{subsubsec:ggF_HjNNLO-setup}

We begin by presenting the basic notation needed to describe our calculation.  We use the QCD 
Lagrangian, supplemented with a dimension-five non-renormalizable 
operator that describes the interaction of the Higgs boson with gluons 
in the limit of very large top quark mass:
%
\begin{equation}
{\mathcal L} = -\frac{1}{4} G_{\mu \nu}^{(a)} G^{(a)\mu \nu}
- \lambda_{\PH\Pg\Pg} \PH G_{\mu \nu}^{(a)} G^{(a)\mu \nu}.
\label{ggF_HjNNLO:lag}
\end{equation}
%
Here, $G_{\mu \nu}^{(a)}$ is the field-strength tensor 
of the gluon field and $\PH$ is the Higgs-boson field. 
Matrix elements computed  with the Lagrangian of Eq.~(\ref{ggF_HjNNLO:lag}) 
need to be renormalized.  Two renormalization constants 
are required to do so: one which relates the bare and renormalized strong coupling constants, and another which ensures that matrix elements of the $\PH\Pg\Pg$ dimension-five operator are finite.  The expressions for these quantities are given in \Bref{Boughezal:2013uia}.  We note that the Lagrangian of Eq.~(\ref{ggF_HjNNLO:lag}) neglects light fermions, as will 
the initial numerical results presented.  We comment on the phenomenological impact of this approximation later in this section.

Renormalization of the strong coupling constant and of the effective 
Higgs-gluon coupling removes ultraviolet divergences from the matrix 
elements. The remaining divergences are of infrared origin. To 
remove them, we must both define and compute infrared-safe observables, 
and absorb the remaining collinear singularities by renormalizing 
parton distribution functions. Generic infrared safe observables are defined using jet algorithms. 
For the calculation described here we employ the $k_{\mathrm T}$-algorithm. 

Collinear singularities associated with 
gluon radiation by incoming partons must be removed by additional renormalization of parton 
distribution functions. We describe how to perform this renormalization in what follows. 
We denote the ultraviolet-renormalized 
partonic cross section by ${\bar \sigma}(x_1,x_2)$, and the collinear-renormalized 
partonic cross section by  $\sigma(x_1,x_2)$. Once we know $\sigma(x_1,x_2)$, we 
can compute the hadronic cross sections by integrating 
the product of $\sigma$  and 
the gluon  distribution  functions  over $x_1$ and $x_2$:
%
\begin{equation}
\sigma(\Pp + \Pp \to \PH+j) = \int {\mathrm d} x_1 {\mathrm d}x_2 \;g(x_1) g(x_2) \; \sigma(x_1,x_2).
\end{equation}
%
The relation between $\sigma$ and ${\bar \sigma}$ is given by the following formula:
%
\begin{equation}
\sigma = \Gamma^{-1} \otimes {\bar \sigma} \otimes \Gamma^{-1},
\label{ggF_HjNNLO:eq_basic}
\end{equation}
%
where the convolution sign stands for 
%
\begin{equation}
\left [ f \otimes g \right](x) = 
\int \limits_{0}^{1} {\mathrm d} z \, {\mathrm d} y \delta( x - yz) f(y) g(z).
\end{equation}
%
The collinear counterterm can be expanded in the strong coupling constant as
%
\begin{equation}
\Gamma = \delta(1-x) - \left ( \frac{\alphas}{2\pi} \right ) \Gamma_1 
+ \left ( \frac{\alphas}{2\pi} \right )^2  \Gamma_2.
\end{equation}
%
We write the ultraviolet-renormalized partonic cross section through NNLO as  
%
\begin{equation}
{\bar \sigma} = {\bar \sigma}^{(0)} 
+
\left ( \frac{\Gamma(1+\epsilon) \alphas}{2\pi} \right ) 
 {\bar \sigma}^{(1)} 
+ \left (\frac{ \Gamma(1+\epsilon)  \alphas}{2\pi} \right )^2 {\bar \sigma}^{(2)}, 
\end{equation}
%
and the collinear-renormalized partonic cross section as 
%
\begin{equation}
 \sigma = \sigma^{(0)} + 
\left ( \frac{ \alphas}{2\pi} \right )   {\sigma}^{(1)} 
+ \left ( \frac{ \alphas}{2\pi} \right )^2 {\sigma}^{(2)}. 
\end{equation}
%
Using these results, we can solve to find the following results for the finite cross section 
expanded in $\alphas$:
%
\begin{equation}
\begin{split} 
& 
\sigma^{(0)} = {\bar \sigma}^{(0)},\;\;\;\;\;\;\;\;\;\;\;
\sigma^{(1)} = {\bar \sigma}^{(1)}
 + \frac{\Gamma_1 \otimes {\sigma}^{(0)}}{\Gamma(1+\epsilon)} 
 + \frac{{\sigma}^{(0)} \otimes \Gamma_1 }{\Gamma(1+\epsilon)},
\\
& \sigma^{(2)} = {\bar \sigma}^{(2)} 
- \frac{\Gamma_2 \otimes {\sigma}^{(0)} }{\Gamma(1+\epsilon)^2} 
- \frac{{\sigma}^{(0)} \otimes \Gamma_2  }{\Gamma(1+\epsilon)^2} 
- \frac{  \Gamma_1 \otimes {\sigma}^{(0)} \otimes \Gamma_1  }{\Gamma(1+\epsilon)^2} 
%\\
%&~~~~~~~~~~~ 
+ \frac{ \Gamma_1 \otimes \sigma^{(1)} }{\Gamma(1+\epsilon)}   
+ \frac{\sigma^{(1)} \otimes \Gamma_1}{\Gamma(1+\epsilon)}.    
\end{split}
\label{ggF_HjNNLO:eq2}
\end{equation}
%
We note that although finite, the $\sigma^{(i)}$ still depend on unphysical renormalization and
factorization scales because of the truncation of the perturbative expansion. 
In the following, we will consider for simplicity 
the case of equal renormalization and factorization scales ,
$\muR=\muF=\mu$. 
The residual $\mu$ dependence is easily determined by solving the 
renormalization-group equation order-by-order in $\alphas$.

\subsubsection{Calculational framework}
\label{subsubsec:ggF_HjNNLO-framework}

It follows from the previous section that in order to 
obtain $\sigma^{(2)}$ at a generic scale, apart from lower-order results
we need to know the NNLO renormalized cross section ${\bar \sigma}^{(2)}$ 
and convolutions of NLO and 
LO cross sections with the various splitting functions which appear in the collinear counterterms.
Up to terms induced by the renormalization, 
there are three contributions to  ${\bar \sigma}^{(2)}$ that are  required:
\begin{itemize} 
\item the two-loop virtual corrections to $\Pg\Pg \to \PH\Pg$;
\item   the one-loop virtual corrections to 
$\Pg\Pg \to \PH\Pg\Pg$; 
\item the double-real contribution  $\Pg\Pg \to \PH\Pg\Pg\Pg$.  
\end{itemize} 
%
We note that helicity amplitudes for 
all of these processes  are available in the literature. The 
two-loop amplitudes for $\Pg\Pg \to \PH\Pg$ were recently 
computed  in 
\Bref{Gehrmann:2011aa}. 
The one-loop corrections to $\Pg\Pg \to \PH\Pg\Pg$ and the tree amplitudes for $ \Pg\Pg \to \PH\Pg\Pg\Pg$
are also known, and are available in the form 
of a Fortran code in the program 
MCFM~\cite{Campbell:2010ff}.  

Since all ingredients for the 
NNLO computation of $\Pg\Pg \to \PH+{\mathrm{jet}}$ have 
been available for some time, it is interesting  
to understand what has prevented this calculation from being performed.
The main
difficulties  with NNLO calculations appear when one attempts
to combine the different contributions, since integration 
over phase space introduces 
additional singularities if the required number of jets 
is lower than the parton multiplicity.  
To perform the phase-space  integration, 
we  must  first isolate singularities in tree- and loop amplitudes.  
%It required a long time to establish a convenient way to do this.

The computational method that we will explain shortly  is based 
on the idea that relevant singularities can be isolated 
using appropriate parameterizations of  phase space 
and expansions in plus-distributions.  To use this approach for computing   NNLO QCD corrections, we need 
to map the relevant phase space  to a unit hypercube 
in such a way that extraction of singularities  is 
straightforward.  %It is clear that t
The correct variables 
to use  are the re-scaled energies of unresolved 
partons and the relative angles between 
two unresolved collinear partons. However, 
the problem is that  different partons become unresolved in different parts of the phase space.  It is not immediately clear how to switch between different 
sets of coordinates and cover the full phase space.  

We note that for  NLO QCD computations, this  problem was 
solved in  \Bref{Frixione:1995ms}, where it was 
explained that the  full phase space can be partitioned into 
sectors in such a way 
that in each sector only one parton ($i$) can  produce 
a soft singularity and only one pair 
of partons ($ij$) can produce a collinear singularity. 
In each sector,  
the proper variables are  the energy of the parton $i$ and the relative 
angle between partons $i$ and $j$. 
Once the partitioning of the phase space is established and proper variables are chosen for each sector, 
we can use an expansion in plus-distributions to construct  
relevant subtraction terms for each 
sector. With the  subtraction terms in place,  
the Laurent expansion of  cross sections in  $\epsilon$ can be constructed, 
and each 
term in such an expansion can be integrated 
over the phase space independently. 
Therefore, partitioning of the 
phase space into suitable sectors and proper parameterization of the 
phase space in each of these sectors are the two crucial elements 
needed to extend this method to NNLO.  It was first suggested in \Bref{Czakon:2010td} how to construct this 
extension for double real-radiation processes at NNLO.  We give a brief overview of this technique in the next section.

\subsubsection{An example: double-real emission corrections}
\label{subsubsec:ggF_HjNNLO-doublereal}

We briefly present the flavor of our calculational methods by outlining how the double-real emission corrections are handled.  To start, we follow the logic used at NLO in \Bref{Frixione:1995ms} and partition the phase space for the $\Pg(p_1)\Pg(p_2) \to \PH \Pg(p_3) \Pg(p_4)\Pg(p_5)$ process into separate structures that we call `pre-sectors' where only a given set of singularities can occur:
%
\begin{equation}
\frac{1}{3!} {\mathrm d}{\mathrm{Lips}}_{12 \to \PH 345 }
= \sum \limits_{\alpha}^{} {\mathrm d}{\mathrm{Lips}}_{12 \to \PH 345 }^{(\alpha)},
\end{equation}
%
Here, $\alpha$ is a label which denotes which singularities can occur, and dLIPS denotes the standard Lorentz-invariant phase space.  At NNLO we can have at most two soft singularities and two collinear singularities in each pre-sector, so as an example there will be an $\alpha$ labeling where $p_4$ and $p_5$ can be soft, and where both can be collinear to $p_1$.  Within this particular pre-sector, which we label as a `triple-collinear' pre-sector, it is clear that the appropriate variables to describe the phase space are the energies of gluons $p_4$ and $p_5$, and the angles between these gluons and the direction of $p_1$. 

Our goal in introducing a parameterization is to have all singularities appear in the following form:
%
\begin{equation}
I(\epsilon) = \int \limits_{0}^{1} {\mathrm d} x x^{-1-a\epsilon} F(x), 
\label{ggF_HjNNLO:fact}
\end{equation}
%
where the function $F(x)$ has a well-defined limit $\lim \limits_{x \to 0}^{} F(x) = F(0)$.  Here, the $F(x)/x$ structure comes from the matrix elements, while the $x^{-a\epsilon}$ comes from the phase space.  When such a structure is obtained, we can extract singularities using the plus-distribution expansion
%
\begin{equation}
\frac{1}{x^{1+a\epsilon}} = -\frac{1}{a\epsilon} \delta(x) + \sum_{n=0}^{\infty}
\frac{(-\epsilon a)^n}{n!}\left[\frac{\ln^n(x)}{x}\right]_+ \, ,
\end{equation}
%
so that 
%
\begin{equation}
I(\epsilon) = \int \limits_{0}^{1} {\mathrm d} x \left ( -\frac{F(0)}{a\epsilon}  + \frac{F(x) - F(0)}{x}
 -a\epsilon  \frac{F(x) - F(0)}{x} \ln(x) +...\right ).
 \label{ggF_HjNNLO:ints}
\end{equation}
%
The above equation provides the required Laurent  expansion of the integral $I(\epsilon)$.  We note that each term in such an expansion can be calculated numerically, and independently from the other terms.   

Unfortunately, no phase-space parameterization for double-real emission processes can immediately achieve the structure of Eq.~(\ref{ggF_HjNNLO:fact}).  Each pre-sector must be further divided into a number of sectors using variable changes designed to produce the structure 
of Eq.~(\ref{ggF_HjNNLO:fact}) in each sector.  Following \Bref{Czakon:2010td}, we further split the triple-collinear pre-sector mentioned above into five sectors so that all singularities in the matrix elements appear in the form of Eq.~(\ref{ggF_HjNNLO:fact}).  Explicit details for these variables changes, and those for all other pre-sectors needed for the NNLO calculation of Higgs plus jet, are presented in \Bref{Boughezal:2013uia}.

Once we have performed the relevant variables changes, we are left with a set of integrals of the form shown in Eq.~(\ref{ggF_HjNNLO:ints}).  We now discuss how we evaluate the analogs of the $F(x)$ and $F(0)$ terms that appear in the full calculation.  When all $x_i$ variables that describe the final-state phase space are non-zero, we are then evaluating the $\Pg\Pg \to \PH\Pg\Pg\Pg$ matrix elements with all gluons resolved.  The helicity amplitudes for this process are readily available, as discussed above, and can be efficiently evaluated numerically.  When one or more of the $x_i$ vanish, we are then in a singular limit of QCD.  The factorization of the matrix elements in possible singular limits appearing in double-real emission corrections in QCD has been studied in detail~\cite{Catani:1999ss}, and we can appeal to this factorization to evaluate the analogs of the $F(0)$.  For example, one singular limit that appears in all pre-sectors is the so-called `double-soft' limit, in which both gluons $p_4$ and $p_5$ have vanishingly small energies.  The matrix elements squared factorize in this limit in the following way in terms of single $S_{ij}(p)$ and double $S_{ij}(p,q)$ universal eikonal factors~\cite{Catani:1999ss}:
%
\begin{equation}
\begin{split} 
& |{\mathcal M}_{\Pg_1 \Pg_2 \to \PH \Pg_3 \Pg_4 \Pg_5}|^2 
\approx  C_A^2 g_s^4 
 \Bigg [
\left ( \sum \limits_{ij \in S_p}^{} S_{ij}(p_4) \right ) 
\left ( \sum \limits_{kn \in S_p}^{} S_{kn}(p_5) \right ) 
\\
&
+ 
\sum \limits_{ij \in S_p}^{} S_{ij}(p_4,p_5) 
 - \sum \limits_{i=1}^{3}  S_{ii}(p_4,p_5) 
\Bigg ] |{\mathcal M}_{g_1 g_2 \to H g_3}|^2.
\end{split}
\label{ggF_HjNNLO:doublesoft}
\end{equation}
%
The advantage of using this factorization is that all structures on the right-hand side of Eq.~(\ref{ggF_HjNNLO:doublesoft}) are readily available in the literature and can be efficiently evaluated numerically; as discussed the helicity amplitudes for $\Pg\Pg \to \PH\Pg$ are known, and the $S_{ij}$ eikonal factors which appear are also well-known functions.  Using this and other such relations, the analogs of the integrals in Eq.~(\ref{ggF_HjNNLO:ints}) appearing in the full theory can be calculated using known results.  Similar techniques can be used to obtain the other structures needed for the NNLO computation of Higgs plus jet production.  For a discussion of all relevant details, we refer the reader to \Bref{Boughezal:2013uia}.

\subsubsection{Numerical results}
\label{subsubsec:ggF_HjNNLO-numerics}

We present here initial numerical results for Higgs production in association with one or more jets at NNLO, arising from the dominant gluon-fusion subprocess.  A detailed series of checks on the presented calculation were performed in \Bref{Boughezal:2013uia}, and we do not repeat this discussion here. 
We compute 
the hadronic cross section for the production of the Higgs boson 
in association with one or more jets  at the 8 TeV LHC 
through NNLO in perturbative QCD. 
We reconstruct jets using the $k_{\mathrm T}$-algorithm
with $\Delta R = 0.5$ and $\pT^j=30~\UGeV$. 
The Higgs mass is taken to be $\MH=125\UGeV$
and the top-quark mass $m_{\PQt}=172~\UGeV$. We use the 
latest NNPDF parton distributions~\cite{Ball:2011uy,Ball:2012cx} with the number of active fermion flavors 
set to five, and 
numerical values of the strong coupling constant $\alphas$ 
at various orders in QCD perturbation theory as provided 
by the NNPDF fit. We note that in this case  $\alphas(\MZ)=[0.130,0.118,0.118]$ 
at leading,  next-to-leading  and next-to-next-to-leading 
order, respectively. 
We choose the central renormalization
and factorization scales to be $\mu=\muR=\muF=\MH$. 

In \Fref{ggF_HjNNLO:fig:xsect} we show the partonic cross
section for $\Pg\Pg\to \PH + j$ multiplied by the gluon luminosity through NNLO in perturbative QCD:
%
\begin{equation}
\beta \frac{{\mathrm d}\sigma_{\mathrm{had}}}{{\mathrm d}\sqrt{s}} = \beta \frac{{\mathrm d}\sigma (s,\alphas,\muR,\muF)}{{\mathrm d}\sqrt{s}} \times
\mathcal L(\frac{s}{s_{\mathrm{had}}},\muF),
\end{equation}
%
where $\beta$ measures the distance from the partonic threshold,
%
\begin{equation}
\beta = \sqrt{1-\frac{E_{\mathrm{th}}^2}{s}},~~~~~~~ E_{\mathrm{th}} = \sqrt{\MH^2 + p_{\perp,j}^2} + p_{\perp,j}\approx 158.55~\UGeV.
\end{equation}
%
The partonic luminosity $\mathcal L$ is given by the integral 
of the product of two gluon distribution functions
% 
\begin{equation}
\mathcal L(z,\muF) = \int_z^1 \frac{{\mathrm d}x}{x} 
g(x,\muF) g\left(\frac{z}{x},\muF\right).
\end{equation}
%
It follows from \Fref{ggF_HjNNLO:fig:xsect} that NNLO QCD corrections are significant in the 
region $\sqrt{s} < 500\UGeV$. In particular, 
close to  partonic threshold $ \sqrt{s} \sim E_{\mathrm{th}}$, radiative corrections are enhanced 
by threshold logarithms $\ln \beta$ that originate from the incomplete 
cancellation of virtual and real corrections.  There seems to be no significant enhancement 
of these corrections at higher  energies, where  the  NNLO QCD prediction for 
the partonic cross section becomes almost indistinguishable from 
the NLO QCD one.  

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.42\textwidth,angle=270]{YRHXS3_ggF/petriello/fin_had}
\end{center}
\caption{Results for the product of partonic cross sections 
$\Pg\Pg \to \PH + {\mathrm{jet}}$ and  parton luminosity in consecutive  orders in perturbative QCD
at $\mu = \muR = \muF = \MH = 125\UGeV$. 
See the text for explanation.  }
\label{ggF_HjNNLO:fig:xsect}
\end{figure}

We now show the integrated hadronic cross sections in the all-gluon channel. 
We choose to vary the renormalization and factorization scale 
in the range $\mu = \muR=\muF = \MH/2,~\MH,~2\MH$. 
After convolution with the parton luminosities, we 
obtain
%
\begin{equation}
\begin{split} 
&\sigma_{\LO}(\Pp\Pp\to \PH j) = 2713^{+1216}_{-776}~ {\mathrm{fb}}, \\
&\sigma_{\NLO}(\Pp\Pp\to \PH j) = 4377^{+760}_{-738}~ {\mathrm{fb}}, \\
&\sigma_{\NNLO}(\Pp\Pp\to \PH j) = 6177^{+204}_{-242}~ {\mathrm{fb}}.
\end{split} 
\end{equation}
%
We note that NNLO corrections are sizable, as expected
from the large NLO $K-$factor, but the perturbative expansion shows  
marginal  convergence. We also evaluated PDF errors using the full set of NNPDF replicas,
and found it to be of order $5\%$ at LO, and of order $1-2\%$ at both NLO and NNLO, similarly
to the inclusive Higgs case~\cite{Ball:2012cx}. 
The cross section increases by about sixty percent when we move from LO to NLO 
and by thirty percent when we move from NLO to NNLO.  It is also clear that 
by accounting for the NNLO QCD corrections we reduce the dependence on the renormalization 
and factorization scales in a significant way.  The scale variation of the 
result decreases from almost $50\%$ at LO, to $20\%$ at NLO, to less than $5\%$ at NNLO. 
We also note that a perturbatively-stable result is obtained for the scale 
choice $\mu\approx \MH/2$. In this case the 
ratio of the NNLO over the LO cross section  is just 
$1.5$, to be compared 
with  $2.3$ for  $\mu=\MH$ and $3.06$ 
for $\mu=2\MH$, and the 
ratio of NNLO to  NLO is $1.2$.  A similar  trend was observed 
in the calculation of higher-order QCD corrections 
to the Higgs boson production cross section in gluon fusion. 
The reduced scale dependence is also apparent from \Fref{ggF_HjNNLO:fig:scale},
where we plot total cross section as a function of the renormalization and
factorization scale $\mu$ in the region $\pT^j<\mu<2 \MH$.

Finally, we comment on the phenomenological 
relevance of the results for the cross sections 
and $K$-factors reported here that refer only to the Higgs production through gluon-gluon collisions. We note that 
at leading and next-to-leading order, quark-gluon 
collisions increase the $\PH+j$ production cross section 
by about $30$ percent, for the input parameters that we use 
in this paper. At the same time, the NLO $K$-factors for the 
full $\PH + j$ cross section are smaller by about $10{-}15$\%
than the `gluons-only' $K$-factors,  presumably because quark color charges 
are smaller than the gluon ones.  Therefore, 
we conclude that the gluon-only results 
can be used   for reliable phenomenological estimates 
of perturbative $K$-factors but adding quark channels will be essential 
for achieving precise results for the $\PH + j$ cross section.

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.65\textwidth]{YRHXS3_ggF/petriello/plot_scale}
\end{center}
\caption{Scale dependence of the hadronic cross section in consecutive  orders in perturbative QCD.
See the text for details. 
 }\label{ggF_HjNNLO:fig:scale}
\end{figure}

