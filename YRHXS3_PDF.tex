\section{PDF \footnote{%
    S.~Forte, J.~Huston, R.~Thorne}}
\newcommand{\lp}{\left(}
\newcommand{\rp}{\right)}



Several of the PDF sets (at both NLO and NNLO) discussed in the previous Yellow
Reports~\cite{Dittmaier:2011ti,Dittmaier:2012vm}, and specifically
some of those recommended for inclusion in the PDF4LHC
prescription~\cite{Botje:2011sn,PDF4LHCwebpage} for the computation of
central value and uncertainty of a process have been
updated. Furthermore, a number of relevant studies and results from PDF
groups have become available in the last year. 

The NNLO CT10 PDFs have become available~\cite{Gao:2013xoa}. 
These PDFs use essentially the same data sets as in the
already existing CT10 NLO PDFs~\cite{Lai:2010vv}. In this new
analysis, the effects of finite quark masses have been implemented in
the S-ACOT-$\chi$ scheme (see e.g. \Bref{Binoth:2010ra},
Sect.~22 for a review)
at NNLO accuracy. A similar quality of
agreement with the fitted experimental data sets is obtained at NNLO
as at NLO. At low $x$ (<$10^{-2}$), the NNLO gluon distribution is
found to be suppressed, while the quark distributions increase,
compared to the same distributions at NLO. The GM-VFN scheme used in
the NNLO fit yields in changes to both the charm and bottom quark
distributions.  



Two of the MSTW group~\cite{Watt:2012tq} have
investigated the generation of their PDFs as a Monte Carlo set
obtained from  fits to
data replicas (as done by NNPDF, see
e.g. \Bref{Dittmaier:2011ti}, Sect.~8.2.2),
 rather than the Hessian eigenvector approach
used by default. For given $\Delta \chi^2$ the results
are compatible with the eigenvector approach. Furthermore, it was shown that 
Monte Carlo PDF sets could also be generated starting from PDF determined
using the Hessian approach. This has the advantage that 
it is then possible to determine combined uncertainties from
different PDF sets by merging Monte Carlo sets. Also, it is then
possible to use for all sets the reweighting 
approach for estimating the effect of new data on PDFs introduced by NNPDF~\cite{Ball:2010gb,Ball:2011gg}.    
In \Bref{Martin:2012da}, the MSTW group also presented in  
new PDF sets based on a modification and 
extension of their standard parameterization to one using Chebyshev 
polynomials (MSTW2008CP) (and also including modified deuteron corrections - MSTW08CPdeut), 
and studied the effect of LHC data on the 
MSTW2008 PDFs~\cite{Martin:2009iq} and the new PDFs using the reweighting procedure. 
The modifications to the PDFs result in a change to the low-$x$ valence quark 
distributions, and improves the comparison to data for central 
rapidity lepton asymmetry from $\PW$ decays. Little else of significance is 
changed, including $\alphas(\MZ)$, and for the overwhelming majority of 
processes a new PDF release 
would be redundant.

NNPDF have presented a new set, NNPDF2.3~\cite{Ball:2012cx} at NLO and
NNLO, 
which, besides introducing some
small methodological improvements, is  the first set to fully include
available  LHC data. It turns out, however, that the impact of LHC
data is only moderate for the time being, and thus differences in
comparison to the previous  NNPDF2.1
set~\cite{Ball:2011mu,Ball:2011uy} 
are small (consequently, the LO NNPDF2.1 set has not been updated and
its usage together with NNPDF2.3 NLO and NNLO is recommended by
NNPDF). The only significant impact is that of the CMS $\PW$ asymmetry
data on the up-down separation,
which leads to a slightly more accurate prediction of the $\PWp$/$\PWm$
cross section ratio, besides of course more precise predictions for
the $\PW$ asymmetry itself. This is in agreement with the findings of
\Bref{Martin:2009iq}, and indeed the prediction for the up-down
valence ratio in the $x\sim0.001$ region obtained using reweighted 
MSTW08 PDFs is
in much better agreement with NNPDF2.3 (and also
NNPDF2.1)~\cite{Forte:2013wc}.  NNPDF has also presented a first PDF
determination using only collider data (hence avoiding both nuclear
target and lower-energy data): whereas these PDFs are less subject to
theoretical uncertainties related to nuclear an higher twist
corrections, their statistical uncertainties are still not competitive.
A similar conclusion, though based on studies without the recent LHC 
data appeared in \cite{Watt:2012tq}.


The updated PDF sets to be used with the NLO PDF4LHC prescriptions are
thus currently CT10, MSTW08, and NNPDF2.3.

A new NLO and NNLO set, ABM11, has
 been made available by the ABM group~\cite{Alekhin:2012ig}, and a 
benchmarking exercise performed. As well as updating for the combined HERA
data~\cite{Aaron:2009aa} this fit uses the $\overline{\mathrm{MS}}$ renormalization 
scheme for heavy quark masses. In all important respects these PDFs remain
similar to those of ABKM09 \cite{Alekhin:2009ni}, though the gluon 
distribution is a little larger at small $x$. As before Tevatron jet data is
not included directly in the fit, though a comparison to this data 
is presented by
the same authors in~\cite{Alekhin:2012ce}. The authors make the PDFs 
available for a wide variety of $\alphas(\MZ)$ values, though the extracted
value, which is recommended by the authors, is $\alphas(\MZ)=0.1134\pm 0.0011$ at NNLO.      

The HERAPDF collaboration have 
released the HERAPDF1.5 NLO and 
NNLO PDF set~\cite{Radescu:2010zz,CooperSarkar:2011aa}, 
which in addition to the combined HERA-I dataset uses 
the inclusive HERA-II data 
from H1~\cite{Aaron:2012qi} and 
ZEUS~\cite{Abramowicz:2012bx} (though the PDF set is partially based on
a yet-unpublished combined data set). 

Furthermore, within the HERAPDF-HERAFITTER
framework~\cite{Aaron:2009aa}, 
the ATLAS collaboration performed~\cite{Aad:2012sb}
a study of the impact  on the strange quark PDF of the
inclusion of their data on differential $\PW$ and $\PZ$
production~\cite{Aad:2011dm}. This implied a significant increase
of the strange quark contribution to the sea. However, NNPDF2.3
instead finds that whereas the ATLAS do tend to pull the strange
distribution upwards, the effect is negligible withing current
uncertainties; MSTW find similar results. 
There is also a study of the sensitivity $\PW$ and $\PZ$ production to
the strange quark distribution in~\cite{Kusina:2012vh}, but no
explicit examination of the effect of the published data. 

A variety of studies  of theoretical uncertainties on PDFs have recently
appeared. As mentioned above, the study of
extended parameterizations 
in \Bref{Martin:2012da} has been generalized to include 
variation and optimization of the nuclear corrections to deuteron
structure functions. 
Hence, a further modified version of the MSTW08 set, MSTW2008CPdeut
was obtained.   
A study of nuclear corrections using CTEQ PDFs has also 
been presented~\cite{Owens:2012bv}, with broadly similar conclusions,
i.e. a slight increase 
(and increased uncertainty) on the high-$x$ down quark. An increase of
the $\PQd/\PQu$ ratio at the one-sigma level 
for $0.1\lsim x\lsim0.5$ as a consequence of the
inclusion of deuterium corrections was similarly found in
\Bref{Ball:2013gsa}, where it was also shown that their impact is
however otherwise negligible (and in particular negligible for larger
$x$) in the scale of current PDF uncertainties.

In~\cite{JimenezDelgado:2012zx}
it was shown that there can be sensitivity to input scale for PDFs, though 
this will always depend on the flexibility of the input
parameterization. In~\cite{Thorne:2012az} the uncertainty associated
with choices of  
general mass variable flavour number scheme (GM-VFNS) was studied. 
At NLO this can be 
a couple of percent, but as with other scheme choices it diminishes at 
increasing order and becomes $<1\%$ at NNLO. These changes were less than
those obtained in using either the zero mass approximation or using 
fixed flavour number scheme (FFNS). There were implications that the
differences in  
PDFs and the value of $\alphas(\MZ)$ obtaining using either (GM-VFNS) and FFNS 
can be significant, and this requires further (ongoing) study.      
Similar conclusions were reached in~\Bref{Ball:2013gsa}, where it
was shown that use of a FFN scheme affects significantly the total
quark and gluon PDFs, and leads to significant loss in fit quality,
especially to difficulties in reproducing the high
$Q^2$, low $x$ HERA data. In the same Ref. it was also shown that
higher twist corrections to the DIS data included in the NNPDF PDF
determination  have a negligible impact on all PDFs, similar to 
previous conclusions by MRST~\cite{Martin:2003sk} 
and more recent studies involving MSTW PDFs. 

Largely as a part of the work on CT10 NNLO, a number of theoretical
uncertainties related to the treatment of jet cross sections
has been examined, notably those that may have impact on
processes involving gluon scattering. A benchmark comparison of NLO
computations for inclusive jet production constraining the gluon PDF
has been performed ~\cite{Gao:2012he,Ball:2012wy}. A new version of
the program EKS for NLO jet cross sections was
developed~\cite{Ellis:1992en,Gao:2012he} that is entirely independent
from \textsc{NLOJET++}~\cite{Nagy:2001fj} as well as
\textsc{APPLGRID}~\cite{Carli:2010rw} and \textsc{FastNLO}~\cite{Kluge:2006xs} programs
that interpolate the NLOJET++ input. Specific input settings that
produce agreement of all these codes were identified, and
uncertainties in the gluon PDFs associated with fitting the jet data
were examined. It was pointed out, for example,  that correlated
systematic errors published by the jet experiments 
are interpreted differently by the various PDF fitting groups, which
leads to non-negligible differences between the PDF sets. This issue
is not widely known and will be considered in the future to avoid
biases in PDF fits. 

Hence, overall, although there have been a significant number of important 
and interesting updates, there has been no dramatic change in 
PDFs in the past year, mainly because it is clear that LHC data so far
published do not add add a great deal of extra constraint.
A comparison of results using NLO PDFs would be little
different to a year ago. However, especially in the quark sector,
there is some gradual improvement in
agreement between the sets included in the PDF4LHC prescription, which
follows prior 
improvement at the time of the previous report~\cite{Dittmaier:2012vm}
since the original prescription~\cite{Botje:2011sn}. 

Also, a more complete comparison of results 
using NNLO PDFs is now possible. Therefore, 
in this section, we compare NNLO PDF
luminosities (and their uncertainties) from five PDF fitting groups, 
i.e. those that are made available for a variety of $\alphas(\MZ)$
values including those similar to the world average,  
and the resulting predictions for both standard model and Higgs boson
cross sections at $8\UTeV$ at the LHC. We follow the recent benchmarking
exercise in \cite{Ball:2012wy}. 

Following \Bref{Campbell:2006wx}, we define the parton luminosity 
for production of a final state with mass $M_X$ as

\begin{equation}
\Phi_{ij}\lp M_X^2\rp = \frac{1}{s}\int_{\tau}^1
\frac{dx_1}{x_1} f_i\lp x_1,M_X^2\rp f_j\lp \tau/x_1,M_X^2\rp \ ,
\label{eq:lumdef}
\end{equation}
where $f_i(x,M^2)$ is a PDF at a scale $M^2$, 
and $\tau \equiv M_X^2/s$. 

In \Fref{fig:PDFlumi-gg}, the gluon-gluon (top) and gluon-quark
(bottom) parton luminosities from five PDF groups are plotted for the
production of a state of  mass $M_X$ (GeV), as a ratio to the PDF
luminosity of NNPDF2.3. For these comparisons, a common value of
$\alphas(\MZ)$ of $0.118$ has been used. In the region of the Higgs
mass ($125\UGeV$), the $\Pg\Pg$ luminosities (and uncertainties) of NNPDF2.3,
CT10 and MSTW2008 are reasonably close, with the error bands
overlapping, but the total uncertainty range, defined from the bottom
of the CT10 error band to the top of the NNPDF2.3 error band, is of
the order of 8\%. This is approximately twice the size of the error
bands of either CT10 or MSTW2008 (and a bit more than twice that of
NNPDF2.3). The $\Pg\Pg$ luminosities of HERAPDF1.5 and ABM11 are similar
to the three PDFs sets discussed above in the Higgs mass range, although the
$\Pg\Pg$ PDF luminosity for ABM11 falls faster with mass than any of the
other PDFs. The HERAPDF PDF uncertainty is larger, due to the more
limited data sets included in the fit.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
    \begin{center}
      \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/gg_8tev_as_0118.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/gg_8tev_as_0118_b.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qg_8tev_as_0118.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qg_8tev_as_0118_b.eps}
      \end{center}
     \caption{\small
The gluon-gluon (upper plots)
and quark-gluon (lower plots) 
luminosities, Eq.~(\ref{eq:lumdef}), for the production
of a final state of invariant mass $M_X$ (in GeV) at LHC $8\UTeV$.  The left plots
show the comparison between NNPDF2.3, CT10 and MSTW08, while
in the right plots we compare NNPDF2.3, HERAPDF1.5 and MSTW08. 
All luminosities are computed at a common value of $\alphas(\MZ)=0.118$.
    \label{fig:PDFlumi-gg} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%


The $\Pg\PQq$ PDF luminosity error bands overlap very well for CT10,
MSTW2008 and NNPDF2.3 in the Higgs mass range, and indeed at all masses except, 
to some extent, well above $1\UTeV$.  
Again the HERAPDF1.5 uncertainty band is larger. The $\PAQq\PQq$ (top) and
$\PQq\PQq$ (bottom) PDF luminosity comparisons are shown in
\Fref{fig:PDFlumi-qq}. For both $\PAQq\PQq$ and $\PQq\PQq$, there is a
very good overlap of the CT10, MSTW2008 and NNPDF2.3 error bands. The
central predictions of HERAPDF1.5 also agree well, with the
uncertainty band again being somewhat larger. The ABM11 luminosities
are somewhat higher in the low to medium mass range and fall more
quickly at high mass.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
    \begin{center}
      \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qq_8tev_as_0118.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qq_8tev_as_0118_b.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/q2_8tev_as_0118.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/q2_8tev_as_0118_b.eps}
      \end{center}
     \caption{\small
The same as  \Fref{fig:PDFlumi-gg} 
for the quark-antiquark (upper plots)
and quark-quark (lower plots) luminosities.
    \label{fig:PDFlumi-qq}} 
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the PDF4LHC report \cite{Botje:2011sn}, published at a time when NNLO PDFs were not
available from either CT or NNPDF, the recommendation at NNLO was to
use the MSTW2008 central prediction, and to multiply the MSTW2008 PDF
uncertainty by a factor of 2. This  factor of 2 appears to be an
overestimate  in this new comparison of the three global PDFs, for
$\PAQq\PQq$, $\PQq\PQq$ and $\Pg\PQq$ initial states, but is still needed for $\Pg\Pg$
initial states.  
A direct comparison of the parton luminosity uncertainties is shown in \Fref{fig:PDFlumi-rel}
 for $\PAQq\PQq$ (top) and $\Pg\Pg$ (bottom), where the points made
 previously about the relative size of the uncertainties can be more
 easily observed.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht]
    \begin{center}
      \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qq_8tev_default_as_rel.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/qq_8tev_default_as_rel_b.eps}\\
  \includegraphics[width=0.48\textwidth]{YRHXS3_PDF/gg_8tev_default_as_rel.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/gg_8tev_default_as_rel_b.eps}
      \end{center}
     \caption{\small
The relative PDF uncertainties in the quark-antiquark 
luminosity (upper plots)
and in the gluon-gluon luminosity (lower plots), 
for the production
of a final state of invariant mass $M_X$ (in GeV) at the LHC $8\UTeV$.  
All luminosities are computed at a common value of $\alphas=0.118$.
    \label{fig:PDFlumi-rel} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \Fref{fig:8tev-higgs}, we show predictions for Higgs production
at $8\UTeV$ in various channels, for $\alphas(\MZ)$ values of $0.117$
(left) and  $0.119$ (right) for the 5 different PDFs being
considered. As expected, the cross section predictions follow the
trends discussed for the PDF luminosities. The strongest
disagreements, perhaps, are from  the ABM11 predictions for VBF and
associated  (VH) Higgs production, though if the ABM11 PDFs with $\alphas(\MZ)=0.1134$
are used the disagreement in these channels is reduced, but increases for the gluon fusion 
channel.   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[ht!]
\centering
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8-as0117.eps}\quad
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8-as0119.eps}
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8vbf-as0117.eps}\quad
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8vbf-as0119.eps}
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8wh-as0117.eps}\quad
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8wh-as0119.eps}
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8tt-as0117.eps}\quad
\includegraphics[width=0.47\textwidth]{YRHXS3_PDF/h8tt-as0119.eps}
\caption{\small Comparison of the 
predictions for the LHC Standard Model Higgs boson
cross sections at $8\UTeV$ obtained using
various NNLO PDF sets. From top to bottom we show gluon fusion, vector 
boson fusion, associated production (with $\PW$), and associated 
production with a $\PAQt\PQt$ pair.
The left hand plots show 
results for $\alphas(\MZ)=0.117$, while on the right we have 
$\alphas(\MZ)=0.119$.
\label{fig:8tev-higgs}}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Cross section predictions for Higgs production (in the $\Pg\Pg$ channel at
$8\UTeV$, for values of $\alphas(\MZ)$ of $0.117$ and $0.119$) are shown in
\Fref{fig:h8nlo} and  
\Fref{fig:h8nnlo} for CTEQ, MSTW and NNPDF PDFs. In
\Fref{fig:h8nlo}, the predictions are at NLO, but using the PDFs
available in 2010 (left) and in 2012 (right). In
\Fref{fig:h8nnlo}, the cross sections are plotted using the 2012
NNLO PDFs. Here, we estimate the PDF+$\alphas(\MZ)$ uncertainty using
a small variation of the original PDF4LHC rubric; we take the envelope
of the predictions from CT/CTEQ, MSTW and NNPDF including their PDF
uncertainties, and using values of $\alphas(\MZ)$ of $0.117$ and
$0.119$. The uncertainty bands are given by the dashed lines. There is
little change at NLO with the evolution from CTEQ6.6 and NNPDF2.0 to
CT10 and NNPDF2.3, and the uncertainty at NNLO is very similar to the
uncertainty estimated for NLO. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
    \begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/h8-nlo-old.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/h8-nlo-new.eps}
      \end{center}
     \caption{\small The Higgs boson production
cross section in the gluon fusion channel using the 
NLO PDF sets included in the PDF4LHC prescription for $\alphas(\MZ)=0.117$ and
$0.119$. The left plot has been computed with 2010 PDFs and
the right plot with 2012 PDF sets. The envelope (dashed violet
horizontal lines) is defined by the upper and lower values
of the predictions from all the three PDF sets and the two values
of $\alphas$. The solid violet horizontal line is the midpoint
of the envelope.
    \label{fig:h8nlo} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
    \begin{center}
\includegraphics[width=0.49\textwidth]{YRHXS3_PDF/h8-nnlo.eps}
      \end{center}
     \caption{\small The same as \Fref{fig:h8nlo}, but using 2012 NNLO PDFs.
    \label{fig:h8nnlo} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


As a contrast, we show in  \Fref{fig:wpbench} predictions for
$\PWp$ production based on the 2010 NLO and 2012 NNLO PDFs from
CT/CTEQ, MSTW and NNPDF. The relative PDF+$\alphas(\MZ)$ uncertainty estimated
with the same prescription used for Higgs production has a sizable
decrease from the 2010 NLO predictions to the 2012 NNLO
predictions. Similar improvements should be expected for all
quark-initiated processes, including those involved in Higgs
production.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
    \begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/wp-bench-nlo.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/wp-bench-nnlo.eps}
      \end{center}
     \caption{\small The
$\PWp$ production
cross sections determined using the same PDFs and envelope as in
%Figs.~\ref{fig:h8nlo}-\ref{fig:h8nnlo}. The left plot shows 2010 NLO
\Frefs{fig:h8nlo}{fig:h8nnlo}. The left plot shows 2010 NLO
PDFs, the right plot 2012 NNLO PDFs. The recent $8\UTeV$ CMS measurement
is also shown.
    \label{fig:wpbench} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Finally, we demonstrate that although the previous proposal in the PDF4LHC
recommendation to use the envelope of the predictions using three PDF sets
does not strictly have a solid statistical basis, it certainly produces sensible results. Using
the techniques for generating random PDFs sets in \cite{Watt:2012tq} it was 
shown in section 4.1.3 of~\cite{Forte:2013wc} that similar results are 
obtained from combining the results from 100 random sets from MSTW2008, 
NNPDF2.3 and CT10 as from taking the envelopes. The results are shown in 
\Fref{fig:random}. The envelope procedure can be seen to be a little 
more conservative, and becomes more-so in comparison to the combination
of random sets as any discrepancy between sets becomes more
evident. However, for  
predictions using these three PDF sets there is generally not much 
differences between the two methods of calculation. In order to maintain
a conservative uncertainty the continuation of the envelope method is 
probably preferred.   


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
    \begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/combination_z0.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/combination_wpwmratio.eps}\\
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/combination_ttbar.eps}\quad
\includegraphics[width=0.48\textwidth]{YRHXS3_PDF/combination_ggh126.eps}
      \end{center}
     \caption{\small NNLO (a) $\PZ$, (b) $\PWp/\PW$, (c) $\PAQt\PQt$ and (d) $\Pg\Pg \to \PH$ cross sections from MSTW08,
CT10 and NNPDF2.3, combined either by taking the envelope of the three predictions, or from the
statistical combination of 100 random predictions from each group
(from \Bref{Forte:2013wc}).
    \label{fig:random} }
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

