\subsection{Jet studies in gluon-fusion production}
\label{Sec::NLOMC_Jets}
This section reports on an ongoing study concerning the gluon fusion
contribution to Higgs boson production in association with at least two jets.
Both rather inclusive cuts and cuts specific for the weak-boson fusion (WBF)
regime are applied and results compared between various approaches to the
description of higher-order corrections. The WBF region covers a notoriously
difficult regime of QCD radiation, where the perturbative stability of the
calculations are being put to a new test. It is therefore not surprising that while
the various approach agree well in the inclusive region, some significant
differences show up within the WBF cuts. The study here represents merely 
a snapshot of the current findings, which definitely deserve further studies
to better understand similarities and differences between the 
approaches and to quantify their intrinsic uncertainties.

The two kinematic regimes are defined as follows:
\begin{itemize}
\item Inclusive Selection:
  \begin{itemize}
  \item All jets are defined by the anti-$k_{\mathrm T}$
    algorithm\cite{Cacciari:2008gp} ($R=0.4$) and counted if their transverse
    momentum is greater than 20~GeV and their absolute rapidity is less than
    5.0.
  \item At least two hard jets above 25~GeV are required.
  \end{itemize}
\item WBF Selection: %Furthermore, require
  \begin{itemize}
  \item Furthermore require $|\eta_{j_1}-\eta_{j_2}|>2.8$, $m_{j1,j2}>400$~GeV. 
  \end{itemize}
\end{itemize}

\subsubsection{Monte-Carlo generators}
In this section the physics input of the Monte-Carlo generators used is
described and their setup, scale choices, etc.~is detailed\footnote{
  The beginning stages of this study displayed results also from 
  \aMCatNLO~\cite{Frederix:2011ig} using the multijet-merging algorithm 
  at NLO presented in~\cite{Frederix:2012ps}}. 
For this study,
which focuses on the multi-jet descriptions, all predictions are obtained with
no restrictions on the the Higgs boson momentum or decay channel.

\paragraph{HEJ}

High Energy Jets (\HEJ) gives a description to all orders and all
multiplicities of the effects of hard, wide-angle (high energy-) emissions,
by utilizing an approximation to the hard scattering
amplitudes~\cite{Andersen:2009nu,Andersen:2009he,Andersen:2011hs}, which in
this limit ensures leading logarithmic accuracy for both real and virtual
corrections. These logarithmic corrections are dominant in the region of
large partonic center of mass energy compared to the typical transverse
momentum. The resummation obtained is matched with a merging procedure to
full tree-level accuracy for multiplicities up to and including three
jets~\cite{Andersen:2008ue,Andersen:2008gc,hej_implement}. 

The predictions of \HEJ depend principally on the choice of factorization and
renormalization scale, and on the pdf set (MSTW2008nlo\cite{Martin:2009iq}). 
For this study, we allow different scales for individual occurrences of the
strong coupling.  We choose to evaluate two powers of the strong coupling at 
a scale given by the Higgs mass, and all remaining scales are identified with 
the transverse momentum of the hardest jet. Therefore, for $n$ jets,
\begin{equation}
  \alphas^{n+2}(\muR)
  = \alphas^2(\MH)\cdot\alphas^n(p_\perp^{\rm hardest})\,.
\end{equation}
The merging scale is set to 20~GeV, the minimum transverse momentum of the 
jets. 

\paragraph{MCFM}

For the purpose of this comparison \MCFM \cite{Campbell:2010cz,MCFMweb} was 
used to provide a fixed-order description of the observables in 
question. The transverse mass of the Higgs boson 
$m_{{\mathrm T},\PH}^2=\MH^2 + p_{{\mathrm T},\PH}^2$ has been used as the factorization 
and renormalization scale. The CTEQ6.6 parton distribution functions 
\cite{Nadolsky:2008zw} were used in conjunction with its accompanying value and evolution 
of the strong coupling constant. The effective $\Pg\Pg \PH$ vertex is
calculated in the $m_{\PQt}\to\infty$ limit.

\paragraph{PowhegBox}

The \PowhegBox{} generators for Higgs ($\PH$), Higgs plus one jet ($\PH J$) and
Higgs plus two jets ($\PH JJ$) have appeared in \Brefs{Alioli:2008tz}
and~\cite{Campbell:2012am}.  For the present work, the HJJ
generator has been augmented with the MiNLO
method~\cite{Hamilton:2012np,Hamilton:2012rf}. As shown in these references,
when using the MiNLO method, the {HJ} and {HJJ} generators also maintain
some level of validity when used to compute inclusive quantities that do
not require the presence of jets. More specifically, the HJ generator
remains valid also for describing the Higgs inclusive cross section, and
the HJJ generator remains valid for describing the Higgs plus one jet, and
the Higgs inclusive cross section. This is achieved without introducing any
unphysical matching scale. The generators become smoothly consistent with
the generators of lower multiplicities when the emitted parton become
unresolvable. The level of accuracy that they maintain is discussed
in detail in \Bref{Hamilton:2012rf}. By the arguments given there
one can show that the MiNLO procedure can be implemented in such a way that
the HJ generator maintains NLO accuracy when the emitted parton becomes
unresolved. The HJJ generator, at present, maintains only LO accuracy
when emitted partons become unresolvable.

In \refT{tab:incrates}
we show the inclusive production rates obtained with the different generators
according to the standard cuts described in this report.
Furthermore, we show the standard set of distributions obtained
with the HJJ generator interfaced with \PythiaSix \cite{Sjostrand:2006za},
without hadronization.
\begin{table}[htb]
 \caption{Total rates for the $\PH$ generator (H), the HJ generator with
           MiNLO (HJ), the HJ generator with MiNLO improved according to
           \cite{Hamilton:2012rf} (HJ-N) and the MiNLO improved HJJ
           generator.\label{tab:incrates}}
 \begin{center}
    \begin{tabular}{lccc}
      \hline
      $\vphantom{\int_a^b}$ & Inclusive & 2 jets, $p_\mathrm{T} > 25, \left| y \right| < 5$ & 
      $+ \; \eta_{\tmop{jj}} < 2.8, m_{\tmop{jj}} > 400 \tmop{GeV}$\\
      \hline
      $\vphantom{\int_a^b}$\tmtexttt{H} & 13.2 & 1.61 & 0.177\\
      $\vphantom{\int_a^b}$\tmtexttt{HJ} & 16.2 & 2.04 & 0.202\\
      $\vphantom{\int_a^b}$\tmtexttt{HJ-N} & 13.3 & 2.10 & 0.209\\
      $\vphantom{\int_a^b}$\tmtexttt{HJJ} & 17.8 & 2.41 & 0.239\\
      \hline
    \end{tabular}
  \end{center}
  \end{table}

\paragraph{Sherpa}

The \Sherpa \cite{Gleisberg:2008ta} predictions are calculated using the 
\MEPSatNLO method \cite{Hoeche:2012yf,Gehrmann:2012yg}. It combines multiple 
\NLOPS \cite{Frixione:2002ik,Nason:2004rx,Frixione:2007vw,Hoeche:2011fd} 
matched calculations of successive jet multiplicity into an inclusive sample 
such that both the fixed order accuracy of every subsample is preserved 
and the resummation properties of the parton shower w.r.t.\ the inclusive 
sample are restored.

For the purpose of this study the subprocesses $\Pp\Pp\to h+0,1$ jets are 
calculated at next-to-leading order accuracy using a variant of the 
\MCatNLO method \cite{Hoeche:2011fd}, while the subprocesses $pp\to h+2,3$ jets 
are calculated at leading order accuracy. Further emissions are effected 
at parton shower accuracy only. 
The $\Pp\Pp\to \PH$ and $\Pp\Pp\to \PH+1$ jet virtual matrix elements are taken from 
\cite{Dawson:1990zj,Djouadi:1991tka,Ravindran:2002dc,Schmidt:1997wr}. 
While not included in this study, the subprocess $\Pp\Pp\to \PH+2$ jets can also 
be calculated at next-to-leading order accuracy by interfacing \GoSam 
\cite{vanDeurzen:2013rv} through the Binoth-Les Houches accord 
\cite{Binoth:2010xt} interface, see also the description in the following
section, \ref{NLOMC_H3j}.
The effective $\Pg\Pg h$ vertices are computed in the $m_{\PQt}\to\infty$ limit.

Scales are chosen in the usual way of 
\MEPS merging \cite{Hoeche:2009rj}, i.e.\ they are set to the relative 
transverse momenta of parton splittings reconstructed through backwards 
clustering the respective final states using inverted parton shower 
kinematics and splitting probabilities. Thus,
\begin{equation}
  \alphas^{n+k}(\muR)
  = \alphas^k(\mu_0)\cdot\prod\limits_{i=1}^n\alphas(\mu_i)\,,
\end{equation}
wherein the $\mu_i$ are in the individual splitting scales and $\mu_0$ 
is the scale of the underlying $\Pp\Pp\to \PH$ production process, here $\mu_0=\MH$, 
$k=2$. This plays an integral part in restoring the overall resummation 
properties. The CT10 \cite{Guzzi:2011sv} parton distributions, with 
the respective value and evolution of the strong coupling, are used 
throughout. The merging scale is set to $Q_{\mathrm{cut}}=20\UGeV$.

\subsubsection{Comparison}
Figures~\ref{Fig:Yj1_Yj2_dijet}--\ref{Fig:mj1j2_dYj1j2_dijet} exhibit 
important jet distributions in the inclusive regime.  Results of \HEJ, \MCFM, 
\PowhegBox, and \Sherpa agree fairly well for distributions of the individual 
two hardest jets, like for instance rapidity distributions
(\Fref{Fig:Yj1_Yj2_dijet}). The
notable exception is the fixed-order calculation.  However, this
is easily understood by the renormalization scales in 
the NLO calculation, which tends to pick higher scales for all strong
couplings and therefore leads to a smaller cross section.  Interestingly
enough, though, this does not lead to large visible shape differences in
the inclusive rapidity distributions of the two jets, 
\cf~\Fref{Fig:Yj1_Yj2_dijet}.
This picture somewhat changes when considering observables sensitive to
the kinematics of both leading jets, like their invariant mass or
rapidity distance (\Fref{Fig:mj1j2_dYj1j2_dijet}).  Again, the fixed-order 
result is below the two DGLAP-type
resummations provided by the parton showers and Sudakov form factors in 
the \PowhegBox and \Sherpa, with only minor, but now more visible differences 
in shape.  Again, this can be attributed to different choices for the
renormalization and factorization scales.  On the other hand, for the
invariant mass and rapidity difference distributions of the two leading
jets now the shape of the results from \HEJ start to strongly deviate.  This 
may be understood from the Regge-type suppression factors systematically 
resummed in \HEJ, but which are not present in the other approaches, but it
definitely deserves further and more detailed studies. These effects disfavor
large empty rapidity gaps, see \Fref{Fig:mj1j2_dYj1j2_dijet}.  

\begin{figure}[t]
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet1_y_2jet}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet2_y_2jet}
  \caption{
	    Rapidity of the leading (left) and subleading (right) 
	    jets in the dijet selection.
	    \label{Fig:Yj1_Yj2_dijet}
	  }
\end{figure}

\begin{figure}[t]
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet12_mass_2jet}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet12_dy_2jet}
  \caption{
	    Invariant mass (left) and rapidity separation (right) of 
	    the leading jets in the dijet selection.
	    \label{Fig:mj1j2_dYj1j2_dijet}
	  }
\end{figure}

\Fref{Fig:mj1j2_dYj1j2_dijet} also illustrates how the \HEJ cross
section in the WBF regime will deviate from the other Monte Carlos,
since the region of large rapidity separation between the two hardest jets is
suppressed. This is further quantified in \Tref{Tab:XS}. The relative
effect of imposing the WBF cuts is largest for the predictions from \HEJ.
\begin{table}[t]
  \caption{
	    Cross sections predicted by the individual generators for 
	    the dijet and WBF selections and the predicted relative reduction
            in cross section by applying WBF selection.
	    \label{Tab:XS}
          }
  \begin{center}
    \begin{tabular}{lccc}
      \hline
      $\vphantom{\int_a^b}$& Dijet selection & WBF selection & Effect of WBF cut\\\hline
      \MCFM      & 1.73 pb & 0.192 pb & 0.111 \\ 
      \HEJ       & 2.20 pb & 0.127 pb & 0.058 \\
      % \aMCatNLO  & 1.65 pb & 0.125 pb & 0.076 \\\hline
      \PowhegBox & 2.41 pb & 0.237 pb & 0.098 \\
      \Sherpa    & 2.38 pb & 0.225 pb & 0.094 \\ \hline
    \end{tabular}
  \end{center}
\end{table}

An interesting difference, apart from the overall
normalization, can be seen in the shape of the rapidity distribution of the leading jet
after WBF cuts: \HEJ has a much less developed dip at central 
rapidities; the leading jet is typically more 
central in rapidity than in the other approaches, cf.~\refF{Fig:Yj1_Yj2_WBF}.
Such aspects of the QCD radiation pattern  for large 
rapidity separations are currently investigated also experimentally, and a
better understanding could be used to further 
discriminate QCD and WBF production.  

A similar trend is observed in the transverse momentum distributions of the
two leading jets exhibited in \Fref{Fig:pTj1_pTj2_WBF}, where \HEJ
exhibits differences in normalization and shape, with slightly harder
spectrum for the two hardest jets.  Conversely, the rapidity of the
third hardest jet displayed in the left panel of
\Fref{Fig:Yj3_dPhij1j2_WBF}, tends to be a bit wider at tree-level
(from \textsc{MCFM}) and
in \HEJ than in
the parton-shower based approaches.  This, however, is an observable which for
all approaches 
is given at leading order only, potentially supported by some resummation.
It is thus not a surprise that here the largest differences between different
approaches show up.  

The right panel of \Fref{Fig:Yj3_dPhij1j2_WBF} shows the difference in
the azimuthal angle of the two leading jets. \Sherpa exhibits
a visible shape difference for small angles, while the other approaches are
more similar.  \HEJ shows a slightly
reduced correlation of the two hardest jets, possibly caused by a larger jet
radiation activity than in the shower-based approaches\cite{Andersen:2010zx}.


\begin{figure}[t]
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet1_y_WBF}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet2_y_WBF}
  \caption{
	    Rapidity of the leading (left) and subleading (right) 
	    jets in the WBF selection.
	    \label{Fig:Yj1_Yj2_WBF}
	  }
\end{figure}

\begin{figure}[t]
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet1_pT_WBF}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet2_pT_WBF}
  \caption{
	    Transverse momenta of the leading (left) and subleading (right) 
	    jets in the WBF selection.
	    \label{Fig:pTj1_pTj2_WBF}
	  }
\end{figure}

\begin{figure}[t]
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet3_y_WBF}\hfill
  \includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/figures/jet12_dphi_WBF}
  \caption{
	    Rapidity of the third jet (left) and azimuthal separation 
	    of the two leading jets (right) in the WBF selection.
	    \label{Fig:Yj3_dPhij1j2_WBF}
	  }
\end{figure}


\subsubsection{Outlook}
The results presented here tend to open more questions than to provide
answers, with differences between approaches being somewhat larger than 
expected.  Further studies are clearly important, since the gluon fusion
background to WBF production of a Higgs boson will mostly be estimated from
Monte Carlo techniques and a Monte Carlo driven extrapolation from control to
signal regions.  In this respect the apparent similarity of approaches for
the inclusive selection and the visible and sometimes even large differences
in the WBF region necessitates a much better understanding of the
QCD radiation pattern in the extreme phase space region of the WBF selection.
In addition to an understanding on the level of central values, it will also
be of utmost importance to gain a well-defined handle on the theory 
uncertainties related to the extrapolation.  This will be the subject of
ongoing and further studies.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../YRHXS3"
%%% End: 
