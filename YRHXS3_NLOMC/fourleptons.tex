%% \newcommand{\NLOacc}{N\scalebox{0.8}{LO}\xspace}
%% \newcommand{\MEPSatLO}{M\scalebox{0.8}{E}P\scalebox{0.8}{S}@L\scalebox{0.8}{O}\xspace}
%% \newcommand{\MEPSatNLO}{M\scalebox{0.8}{E}P\scalebox{0.8}{S}@N\scalebox{0.8}{LO}\xspace}
%% \newcommand{\MCatNLO}{M\scalebox{0.8}{C}@N\scalebox{0.8}{LO}\xspace}
%% \newcommand{\Sherpa}{S\scalebox{0.8}{HERPA}\xspace}
%% \newcommand{\OpenLoops}{O\scalebox{0.8}{PEN}L\scalebox{0.8}{OOPS}\xspace}
%% \newcommand{\Collier}{C\scalebox{0.8}{OLLIER}\xspace}
%% \newcommand{\Rivet}{R\scalebox{0.8}{IVET}\xspace}
%% \newcommand{\ATLASexp}{{\sc ATLAS}\xspace}
%% \newcommand{\CMSexp}{{\sc CMS}\xspace}

%% \newcommand{\flfs}{\mu^+\nu_\mu \Pe^-\bar\nu_{\Pe}}
%% \newcommand{\rF}{\mathrm{F}}
%% \newcommand{\delres}{\Delta_\mathrm{res}}
%% \newcommand{\delqcd}{\Delta_\mathrm{QCD}}
%% \newcommand{\sigsr}{\sigma_\mathrm{S2s}}
%% \newcommand{\sigcr}{\sigma_\mathrm{S2c}}


\subsection{Irreducible background to $\PH\to\PW\PW^*$ in exclusive 0- and 1-jets 
bins with MEPS@NLO}
\label{Sec::NLOMC_4l}

%\subsubsection*{Introduction}
Final states involving four leptons played an important role in the 
discovery of the Higgs-like boson in 2012 and will continue to be crucial in 
the understanding of its coupling structure.  By far and large, there are two 
classes of final states of interest, namely those consistent with decays 
$\PH\to \PZ\PZ^*$ yielding four charged leptons and those related to $\PH\to \PW\PW^*$ 
resulting in two charged leptons and two neutrinos.  They have quite different 
backgrounds, and for the latter, the dominant and large top-pair production 
and decay background necessitates the introduction of jet vetoes to render 
the signal visible.  In addition, in order to study the weak-boson fusion 
production channel of the Higgs boson, it is important to understand jet 
production in association with the Higgs boson in the gluon-fusion channel 
as well, a topic discussed in Section~\ref{Sec::NLOMC_Jets} of this report.  
In this section, rather than focusing on the signal, the irreducible 
background to $\PH\to\PW\PW^*$ in the exclusive 0-jets and 1-jet bins will be 
discussed.

\subsubsection{Monte Carlo samples}
As the tool of choice the \Sherpa event generator~\cite{Gleisberg:2008ta} is
employed, using the recently developed multijet merging at next-to-leading
order accuracy~\cite{Gehrmann:2012yg,Hoeche:2012yf}.  Predictions obtained 
with this \MEPSatNLO technology will be contrasted with inclusive \MCatNLO 
and parton-level \NLOacc results for the production of four leptons plus 0 or 1 
jets, all taken from the corresponding implementations within \Sherpa.  
While the latter guarantee \NLOacc accuracy in the 0- and 1-jets bins, 
but do not resum the potentially large Sudakov logarithms arising in the 
presence of jet vetos, inclusive \MCatNLO simulations provide a better 
description of such Sudakov logarithms in the 0-jet bin, but are only LO or 
leading-log accurate in bins with 1 or more jets.  This is overcome by the
new \MEPSatNLO algorithm, which effectively combines \MCatNLO-type
simulations~\cite{Frixione:2002ik} in the fully color-correct algorithm
of~\cite{Hoeche:2011fd,Hoeche:2012ft} for increasing jet multiplicities,
preserving both the \NLOacc accuracy of the contributions in the individual jet
bins and the logarithmic accuracy of the parton shower.  Result for four-lepton
final states obtained with this formalism are presented for the first time
here.

Virtual corrections are computed with \OpenLoops~\cite{Cascioli:2011va}, an
automated generator of \NLOacc QCD amplitudes for SM processes,
which uses the \Collier library for the numerically stable evaluation of
tensor integrals~\cite{Denner:2002ii,Denner:2005nn} and scalar
integrals~\cite{Denner:2010tr}.  Thanks to a fully flexible interface of
\Sherpa with \OpenLoops, the entire generation chain -- from process
definition to collider observables -- is fully automated and can be steered
through \Sherpa run cards.

The results presented below refer to $\Pp\Pp\to\flfs+X$ at a centre-of-mass 
energy of $8\UTeV$ and are based on a \Sherpa2.0 pre-release version\footnote{
This pre-release version corresponds to
SVN revision 21340 (25 Mar 2013) and the main difference with respect
to the final \Sherpa2.0 release version is the tuning of parton shower,
hadronization and multiple parton interactions to experimental data.
}. 
The multijet merging is performed with a merging parameter
of $Q_{\mathrm{cut}}=20\UGeV$, and also LO matrix elements for
$\Pp\Pp\to\flfs+2$ jets are included in the merged sample.
Gluon-gluon induced contributions resulting from 
squared loop amplitudes are discarded.  All matrix elements are evaluated in 
the complex mass scheme and include all interference and off-shell effects.  
Masses and widths of the gauge bosons have been adjusted to yield the correct 
branching ratios into leptons at NLO accuracy and are given by
\begin{equation}
  \begin{split}
    \MW \;=\;& 80.399\,\mathrm{GeV}\;,\;\;\;
    \Gamma_{\PW} \;=\; 2.0997\,\mathrm{GeV}\;,\\
    \MZ \;=\;& 91.1876\,\mathrm{GeV}\;,\;\;\;
    \Gamma_{\PZ} \;=\; 2.5097\,\mathrm{GeV}\;,\\
  \end{split}
\end{equation}
while the electroweak mixing angle is a complex number given by the ratio of 
the complex $\PW$ and $\PZ$ masses, and
\begin{equation}
  \GF\;=\;1.16637\cdot 10^{-5}\;\mathrm{GeV}^{-2}\;\;\mathrm{and}\;\;\;
  1/\alpha_{\mathrm{QED}}\;=\;132.348905\,,
\end{equation}
in the $\GF$-scheme.
The five-flavor CT10 NLO~\cite{Lai:2010vv} parton distributions with the 
respective running strong coupling $\alphas$ has been employed
throughout.  Contributions with external $\Pb$-quarks are not included to 
trivially avoid any overlap with $\PAQt\PQt$ production.    As a 
default renormalization ($\muR$), factorization ($\muF$) and resummation
($\mu_Q$) scale we adopt the total invariant mass of the $\ell\ell'\nu\nu'$
final state, $\mu_0=m_{\ell\ell'\nu\nu'}$.  In the NLO parton-level predictions 
all $\alphas$ factors are evaluated at the same scale $\muR=\muF=\mu_0$,
while $\mu_Q$ is irrelevant.  In \MEPSatNLO predictions the scale $\mu_0$ is 
used only in tree and loop contributions to the $\Pp\Pp\to \flfs$ core process,
after clustering of hard jet emission.  For $\alphas$ factors associated with 
additional jet emissions a CKKW scale choice is applied.  In fact, in contrast 
to the original proposal for such a choice for \NLOacc merging presented 
in~\Bref{Hoeche:2012yf}, the nodal $k_{\mathrm T}$-scales for \NLOacc parts of the 
simulation are implemented through 
\begin{equation}
  \alphas(k_{\mathrm T})\,=\,\alphas(\mu_0)\,
  \left[1-\frac{\alphas(\mu_0)}{2\pi}\,\beta_0\,
    \log\frac{k_{\mathrm T}^2}{\mu_0^2}\right]\,, 
\end{equation}
because preliminary studies suggest that this choice indeed further minimizes 
the effect of $Q_{\mathrm{cut}}$ variations.

There are various theoretical uncertainties entering the simulation of 
hadronic observables:
\begin{itemize}
\item renormalization and factorization scale uncertainties are assessed by 
  an independent variation of $\muR$ and $\muF$ by factors of two
  excluding in both directions only the two opposite combinations.  Note that 
  the renormalization scale is varied in all $\alphas$ terms that arise in 
  matrix elements or from the shower;
\item resummation scale uncertainties are evaluated by varying the starting
  scale of the parton shower $\mu_Q$ by a factor of $\sqrt{2}$ in both 
  directions;
\item PDF -- and, related to them -- $\alphas(\MZ)$ uncertainties, which 
  are both ignored here;
\item non-perturbative uncertainties, which broadly fall into two categories:
  those related to the hadronization, which could be treated by retuning the 
  hadronization to LEP data and allowing/enforcing a deviation of for example 
  $5\%$ or one charged particle in the mean charged particle multiplicity at 
  the $\PZ$-pole, and those related to the underlying event, which could be
  evaluated by allowing the respective tunes to typical data to yield a
  $10\%$ variation in the plateau region of the mean transverse momentum 
  flow in the transverse regions of jet events.  Both have been ignored here
  and will be presented in a forthcoming study.
\end{itemize}

Quantifying the resummation scale uncertainty is crucial for a realistic
study of theory uncertainties in jet distributions and other observables
sensitive to details of the QCD radiation pattern.  As such, they certainly 
impact also on the cross sections in different jet bins.  They can be assessed 
through $\mu_Q$ variations in the \MEPSatNLO approach, which provide some 
sensitivity to subleading -- but potentially large -- Sudakov logarithms 
resulting from jet vetoes.  In contrast, the usual $\muR$ and $\muF$ 
variations alone are not sensitive to Sudakov logarithms or the scales entering
them and are thus not sufficient for a reliable assessment of theory
errors in jet bins.  The consistent implementation of all these effects in
ongoing, but preliminary studies suggest that the $\mu_Q$ variation is
fairly suppressed compared to the standard $\muR$ and $\muF$ variation. 

\subsubsection{Experimental setups and cuts}
The results presented here correspond to the cuts of the two relevant
analyses by the \ATLASexp~\cite{ATLAS-CONF-2013-030} and 
\CMSexp~\cite{CMS-PAS-HIG-13-003} collaborations for $\PH\to\PW\PW^*\to\flfs$ 
in the exclusive 0- and 1-jet bins.  The various cuts are listed in 
\Tref{Tab:NLOMC_WWcuts}\footnote{
  Definition of some kinematical quantities in \Tref{Tab:NLOMC_WWcuts}:
  \newline
  $^\dagger$: The quantity $E\!\!\!/_{\mathrm T}^{\mathrm{(proj)}}$ is given by 
  \begin{equation*}
    E\!\!\!/_{\mathrm T}^{\mathrm{(proj)}} = 
    E\!\!\!/_{\mathrm T}\cdot\sin\left(\min\{\Delta\phi_{\mathrm{near}},\,\pi/2\}\right)\,,
  \end{equation*}
  where $\Delta\phi_{\mathrm{near}}$ denotes the angle between the missing
  transverse momentum $E\!\!\!/_{\mathrm T}$ and the nearest lepton in the transverse
  plane.  \newline
  $^\ddagger$: \ATLASexp and \CMSexp have different definitions for $m_{\mathrm T}$, namely
  \begin{equation*}
    m_{\mathrm T}^2 = \left\{\begin{array}{lcl}
    \left(\sqrt{p_{\perp,\ell\ell'}^2+m_{\ell\ell'}^2}+E\!\!\!/_{\mathrm T}\right)^2-
    \left|p_{\perp,\ell\ell'}+E\!\!\!/_{\mathrm T}
    \vphantom{\sqrt{p_{\perp,\ell\ell'}^2}}\right|^2 &
    \mathrm{for} & \mathrm{ATLAS}\\[2mm]
    2 |p_{\perp,\ell\ell'}|\,|E\!\!\!/_{\mathrm T}|\,(1-\cos\Delta\phi_{\ell\ell',\,E\!\!\!/_{\mathrm T}}) &
    \mathrm{for} & \mathrm{CMS}.
    \end{array}\right.
  \end{equation*}
}. 
Lepton isolation is implemented at the particle level to be close to
the experimental definitions of both ATLAS and CMS. The scalar sum of
the transverse momenta of all visible particles within a $R=0.3$ cone
around the lepton candidate is not allowed to exceed $15\%$ of the
lepton $\pT$.
After a preselection ({\bf S1}), additional cuts are applied that define 
a signal ({\bf S2s}) and a control ({\bf S2c}) region. The latter is
exploited to normalize background simulations to data in the experimental 
analyses in each jet bin.  In the \ATLASexp analysis, different cuts are 
applied in the 0- and 1-jet bins. 

\begin{table}
  \begin{center}
    \caption{Jet definitions and selection cuts in the 
      \ATLASexp~\cite{ATLAS-CONF-2013-030} and 
      \CMSexp~\cite{CMS-PAS-HIG-13-003} analyses of  
      $\PH\to\PW\PW^*\to \flfs$ at $8\UTeV$. Partons are recombined 
into jets using the anti-$k_\rT$ algorithm \cite{Cacciari:2008gp}.  The cuts refer to various levels and 
      regions, namely event preselection ({\bf S1} cuts), the signal region 
      ({\bf S1} and {\bf S2s} cuts) and the control region 
      ({\bf S1} and {\bf S2c} cuts).  All cuts have been implemented in 
      form of a {\sc Rivet}~\cite{Buckley:2010ar} analysis.}
    \label{Tab:NLOMC_WWcuts}
    \begin{tabular}{llll}
      \hline
      {{ anti-$k_\mathrm{T}$ jets}} && \multicolumn{1}{p{5cm}}{\ATLASexp} & 
      \multicolumn{1}{p{5cm}}{\CMSexp} \\\hline 
      $R$ &  $=$    &  0.4          & 0.5\\
      $p_{\perp, j}(|\eta_{j}|)$ &$ >$  & $25\UGeV$ \hfill($|\eta_j|<2.4$) 
                                     & $30\UGeV$ \hfill($|\eta_j|<4.7$)\\ 
                                    && $30\UGeV$ \hfill($2.4<|\eta_j|<4.5$) &
      \\[2mm]\hline 
      {{\bf S1}} && \multicolumn{1}{p{5cm}}{\ATLASexp} & 
      \multicolumn{1}{p{5cm}}{\CMSexp} \\\hline 
      $p_{\perp,\{\ell_1,\,\ell_2\}}$ &$>$  & $25, 15\UGeV$   & $20, 10 \UGeV$\\
      $|\eta_{\{\Pe,\,\PGm\}}|$ &$ <$       & 2.47, 2.5    & 2.5, 2.4\\
      $|\eta_{\Pe}|$           & $\notin$    & $[1.37,1.57]$   & \\
      $p_{\perp,\ell\ell'}$ &$ >$         & see {{\bf S2s}, {\bf S2c}} 
                                                     &  $30 \UGeV$\\
      $m_{\ell\ell'}$ &$ >$              & $10\UGeV$       & $12\UGeV$\\
      $E\!\!\!\!/_{\mathrm T}^{\mathrm{(proj)}}$ &$ >$ & $25\UGeV$      & $20\UGeV$ $^\dagger$
      \\[2mm]\hline 
      {\bf S2s} &  & \multicolumn{1}{p{5cm}}{\ATLASexp} & 
      \multicolumn{1}{p{5cm}}{\CMSexp} \\\hline
      $\Delta\phi_{\ell\ell',\,E\!\!\!/_{\mathrm T}}$&$>$ & $\pi/2$ \hfill(0 jets only) & \\ 
      $p_{\perp,\ell\ell'}$ &$ >$         & $30\UGeV$ \hfill(0 jets only) & \\
      $\Delta\phi_{\ell\ell'}$ &$ <$    &  1.8 rad       &  \\
      $m_{\ell\ell'}$ &$ <$             &  $50\UGeV$        & $200\UGeV$\\
      $m_{\mathrm T}$ &$ \in$                   &                & $[60\UGeV,\,
                                                           280\UGeV]$
\;$^\ddagger$\\[2mm]\hline 
      {{\bf S2c}} &  & \multicolumn{1}{p{5cm}}{\ATLASexp} & 
      \multicolumn{1}{p{5cm}}{\CMSexp} \\\hline 
      $\Delta\phi_{\ell\ell',\,E\!\!\!/_{\mathrm T}}$&$>$ & $\pi/2$ \hfill(0 jets only) & \\ 
      $p_{\perp,\ell\ell'}$ &$ >$         & $30\UGeV$ \hfill(0 jets only) & \\
      $m_{\ell\ell'}$ &            & $\in [50,100]\UGeV$ \hfill(0 jets only)     
      & $>100\UGeV$\\
      &             & $>80\UGeV$ \hfill(1 jet only)   & \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\subsubsection{Results}
Predictions and theoretical errors\footnote{
  Scale variations in \MCatNLO predictions are not considered.} 
for exclusive jet cross sections in the signal and control regions, as well 
as their ratios, are displayed in \refTs{Tab:NLOMC_WWATLAS} 
and~\ref{Tab:NLOMC_WWCMS} for the \ATLASexp and \CMSexp analyses, respectively.
Comparing \NLOacc, \MCatNLO and \MEPSatNLO results at the same central
scale we observe that deviations between \NLOacc and \MEPSatNLO predictions
amount to only $0.5\%$ and $1{-}3\%$ in the 0-jets and 1-jet bins, respectively. The 
differences between \MCatNLO and \MEPSatNLO results are larger and 
reach %5{-}8\%$ ($20{-}22\%$) in the 0(1)-jets bins.
The sizable deficit of the \MCatNLO simulation in the 1-jet bin is not surprising 
given the limited (LO) accuracy of this approximation for exclusive 1-jet 
final states.

Differences between the \NLOacc, \MCatNLO and \MEPSatNLO approximations are
fairly similar in the various analyses and kinematic regions, and the
agreement between the various approximations is at the $0.5{-}3\%$ level in the
$\sigsr/\sigcr$ ratios.  The interpretation of this result as theoretical
uncertainty in the extrapolation from control to signal regions requires a
careful analysis of shape uncertainties, where also the (un)correlation of
scale choices in the various predictions should be considered.  These subtle
issues are beyond the scope of the present study.  We thus refrain from
assigning theoretical uncertainties to the individual signal-to-control
ratios.  In contrast, scale variations of the absolute \MEPSatNLO cross
sections in the various regions and jet bins can be regarded as a realistic
estimate of perturbative theory errors.
Adding $(\muR,\muF)$ and $\mu_Q$ variations in quadrature\footnote{
Variations of $(\muR,\muF)$ and
$\mu_Q$ can be regarded as uncorrelated and thus added in quadrature.
Another natural way of combining these two sources of 
uncertainty is to consider simultaneous variations of $(\muR,\muF,\mu_Q)$,
excluding rescalings in opposite directions as usual.
The variations resulting from this alternative approach are likely to be
even smaller than those obtained by adding  QCD and resummation scale
uncertainties in quadrature.}
we find a combined \MEPSatNLO uncertainty of $3{-}4\%$ in both jet bins.
%The dominant contribution arises from renormalization and 
%factorization scale variations, which give similarly large uncertainties as 
%parton-level \NLOacc calculations.
The dominant contribution arises from renormalization and factorization
scale variations, which turn out to be fairly consistent but slightly different
from the scale variations of the parton-level \NLOacc calculation.
The observed (minor) differences can be
attributed to the variation of extra $\alphas$ terms originating from the
shower, the CKKW scale choice in the \MEPSatNLO method, and the merging of
different jet multiplicities, which opens gluon-initiated channels also
in the 0-jets bin.  Moreover, \MEPSatNLO uncertainties might be slightly
overestimated due to statistical fluctuations at 1\% level.  

Resummation scale variations of \MEPSatNLO cross sections turn out to be
rather small.
%
This suggests that higher-order
subleading Sudakov logarithms, which are beyond the accuracy of the shower, 
are quite suppressed. The rather good agreement between
\NLOacc and \MEPSatNLO predictions indicates that also leading-logarithmic resummation
effects have a quantitatively small impact on the considered observables. 
\begin{table}
  \begin{center}
    \caption{Exclusive 0-jet and 1-jet bin cross sections in the signal (S2s) 
      and control (S2c) regions for the \protect\ATLASexp analysis at 
      $8\UTeV$.  The production of $\flfs+0,1$ jets is described with 
      \protect\NLOacc, \protect\MCatNLO (inclusive), and \protect\MEPSatNLO 
      accuracy as described in the text.  Results at \protect\NLOacc for 
      the $N$-jet bin correspond to $\flfs+N$ jets production.  Variations 
      of the $(\muR, \muF)$ and $\mu_Q$ scales are labeled 
      as $\delqcd$ and $\delres$, respectively.  
      Statistical errors are given in parenthesis.}
    \label{Tab:NLOMC_WWATLAS}
% ATLAS
    \begin{tabular}{lccc}
      \hline
      0-jets bin   &               \NLOacc $\pm\delqcd$       
      & \MCatNLO    & \MEPSatNLO $\pm\delqcd$ $\pm\delres$ \\[1mm]\hline
      $\sigsr$ [fb]      &  $35.08(9)\;^{+2.0\%}_{-1.9\%}$ 
      & $33.33(8)$  & $35.21(15)\;^{+1.8\%}_{-3.3\%}$$\;^{+1.7\%}_{-0.6\%}$
      \\[1mm] 
      $\sigcr$ [fb]      &  $57.05(9)\;^{+2.1\%}_{-2.0\%}$ 
      & $53.76(9)$  & $56.76(17)\;^{+2.3\%}_{-3.6\%}$$\;^{+1.9\%}_{-0.3\%}$
      \\[1mm]
      $\sigsr/\sigcr$  &  $0.615$                         
      & $0.620$   & $0.620$ 
      \\[1mm]\hline 
      1-jet bin  &               \NLOacc $\pm\delqcd$         
      & \MCatNLO    & \MEPSatNLO $\pm\delqcd$ $\pm\delres$ \\[1mm]\hline
      $\sigsr$ [fb]      &  $9.43(3)\;^{+1.8\%}_{-4.7\%}$ 
      & $7.43(4)$  & $9.23(9)\;^{+3.5\%}_{-1.9\%}$$\;^{+0.9\%}_{-0\%}$
      \\[1mm]
      $\sigcr$ [fb]      &  $29.14(6)\;^{+1.6\%}_{-4.7\%}$ 
      & $22.59(7)$  & $28.80(21)\;^{+3.1\%}_{-2.5\%}$$\;^{+1.4\%}_{-1.7\%}$
      \\[1mm] 
      $\sigsr/\sigcr$  &  $0.324$                         
      & $0.329$   & $0.320$ \\[1mm]\hline
    \end{tabular}
  \end{center}
\end{table}


\begin{table}
  \begin{center}
    \caption{Exclusive 0-jet and 1-jet bin cross sections in the signal (S2s) 
      and control (S2c) regions for the \protect\CMSexp analysis at $8\UTeV$, 
      with the same structure and conventions as in \refT{Tab:NLOMC_WWATLAS}.}
    \label{Tab:NLOMC_WWCMS}
% CMS   
    \begin{tabular}{lccc}
      \hline
      0-jets bin   &  \NLOacc $\pm\delqcd$                        
      & \MCatNLO    & \MEPSatNLO $\pm\delqcd$ $\pm\delres$ \\[1mm]\hline
      $\sigsr$ [fb]      &  $159.34(18)\;^{+1.8\%}_{-1.7\%}$ 
      & $150.6(2)$  & $160.3(3)\;^{+2.6\%}_{-3.8\%}$$\;^{+1.4\%}_{-0.5\%}$\\[1mm]
      $\sigcr$ [fb]      &  $60.08(15)\;^{+1.5\%}_{-1.4\%}$ 
      & $56.60(11)$  & $60.31(22)\;^{+3.6\%}_{-3.5\%}$$\;^{+0.7\%}_{-0.2\%}$
      \\[1mm]
      $\sigsr/\sigcr$  &  $2.65$                         
      & $2.66$   & $2.66$ \\[1mm]\hline 
      1-jet bin  &  \NLOacc $\pm\delqcd$                        
      & \MCatNLO    & \MEPSatNLO $\pm\delqcd$ $\pm\delres$ \\[1mm]\hline
      $\sigsr$ [fb]      &  $45.01(7)\;^{+1.3\%}_{-2.6\%}$ 
      & $34.75(9)$  & $44.88(23)\;^{+3.0\%}_{-2.7\%}$$\;^{+0.1\%}_{-0.3\%}$
      \\[1mm]
      $\sigcr$ [fb]      &  $22.09(5)\;^{+1.2\%}_{-3.2\%}$ 
      & $17.41(7)$  & $22.30(18)\;^{+3.0\%}_{-2.7\%}$$\;^{+0.5\%}_{-0.4\%}$
      \\[1mm]
      $\sigsr/\sigcr$  &  $2.04$                         
      & $2.00$   & $2.01$ \\[1mm]\hline
    \end{tabular}
  \end{center}
\end{table}

In \refFs{fig:flj_atlas_c_mT}--\ref{fig:flj_atlas_s_mll} distributions for 
the transverse mass $m_\rT$ and the dilepton mass $m_{\ell\ell'}$ are displayed 
for the \ATLASexp analysis at $8\UTeV$.  Similar plots for the \CMSexp 
analysis are shown in \refFs{fig:flj_cms_c_mT}--\ref{fig:flj_cms_s_mll}.  
These observables provide high sensitivity to the Higgs signal.  They are 
depicted separately in the exclusive 0- and 1-jet bins in the signal 
({\bf S1} and {\bf S2s} cuts) and control ({\bf S1} and {\bf S2c} cuts) regions.
In the lower frames, the various predictions are normalized to \MEPSatNLO 
results at the central scale.  Scale variations are given only for the 
\MEPSatNLO case.  Specifically, the individual 
$(\muR,\muF)$ and $\mu_Q$ variations and their 
combination in quadrature are displayed as three separate 
color-additive bands.

Comparing \NLOacc, \MCatNLO and \MEPSatNLO distributions, agreement on the 
few-percent level in the 0-jet is found, while in the 1-jet bin 
discrepancies between \MCatNLO and \MEPSatNLO on the 20\% level appear.  
This is consistent with the results in 
\refT{Tab:NLOMC_WWATLAS} and \refT{Tab:NLOMC_WWCMS}.  In the case of both
$m_\rT$ and $m_{\ell\ell'}$ distributions, also some shape distortions emerge.
However, as could be anticipated they are fairly moderate in the 0-jet bins
and reach up to about 20\% in the hard region of the 1-jet bins.  
In the tails of some distributions scale-variation bands are 
distorted by statistical fluctuations.
These features are qualitatively similar in the \ATLASexp 
and \CMSexp setups.  

% \ATLASexp plots
\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2c_0j_mT_atlas.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2c_1j_mT_atlas.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Control region of the \protect\ATLASexp analysis at $8\UTeV$:
  transverse-mass distribution in the 0-jets (left) and 1-jet (right) bins.
  \protect\MEPSatNLO (black solid), inclusive \protect\MCatNLO (red dashed), and 
  \protect\NLOacc (blue dashed) predictions at the central scale 
  $\muR=\muF=\mu_Q=m_{\ell\ell'\PGn\PGn'}$.  In the lower panel, showing
  the deviations and uncertainties, results are normalized to the central
  \protect\MEPSatNLO predictions.  The factor-2 variations of $\muR$ and 
  $\muF$ (red band), and factor-$\sqrt{2}$ variations of $\mu_Q$ (blue band),
  are combined in quadrature (yellow band).
  Scale variation bands are color additive, \ie  yellow+blue=green, 
yellow+red=orange, and yellow+red+blue=brown.
} 
\label{fig:flj_atlas_c_mT}%\refF{fig:flj_atlas_c_mT}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_0j_mT_atlas.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_1j_mT_atlas.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Signal region of the \ATLASexp analysis at $8\UTeV$:
  transverse-mass distribution in the 0-jets (left) and 1-jet (right) bins.
  Same approximations and conventions as in \refF{fig:flj_atlas_c_mT}.
}
\label{fig:flj_atlas_s_mT}%\refF{fig:flj_atlas_s_mT}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_0j_mll_atlas.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_1j_mll_atlas.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Signal region of the \protect\ATLASexp analysis at $8\UTeV$: dilepton 
  invariant-mass distribution in the 0-jets (left) and 1-jet (right) bins.
  Same approximations and conventions as in \refF{fig:flj_atlas_c_mT}.}
\label{fig:flj_atlas_s_mll}%\refF{fig:flj_atlas_s_mll}
\end{figure}

% \CMSexp plots
\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2c_0j_mT_cms.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2c_1j_mT_cms.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Control region of the \protect\CMSexp analysis at $8\UTeV$:
  transverse-mass distribution in the 0-jets (left) and 1-jet (right) bins.
  Same approximations and conventions as in \refF{fig:flj_atlas_c_mT}.}
\label{fig:flj_cms_c_mT}%\refF{fig:flj_cms_c_mT}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_0j_mT_cms.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_1j_mT_cms.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Signal region of the \protect\CMSexp analysis at $8\UTeV$:
  transverse-mass distribution in the 0-jets (left) and 1-jet (right) bins.
  Same approximations and conventions as in \refF{fig:flj_atlas_c_mT}.}
\label{fig:flj_cms_s_mT}%\refF{fig:flj_cms_s_mT}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_0j_mll_cms.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_NLOMC/S2s_1j_mll_cms.eps}
\end{center}
\vspace*{-0.3cm}
\caption{Signal region of the \protect\CMSexp analysis at $8\UTeV$:
  dilepton invariant-mass distribution in the 0-jets (left) and 1-jet (right) 
  bins.  Same approximations and conventions as in \refF{fig:flj_atlas_c_mT}.}
\label{fig:flj_cms_s_mll}%\refF{fig:flj_cms_s_mll}
\end{figure}

\subsubsection{Conclusion and outlook}
The findings of this contribution can be summarized as follows:
\begin{itemize}
\item Analyzing the leptonic observables $m_\rT$ and $m_{\ell\ell'}$
  only moderate shape distortions emerge between \MCatNLO and \MEPSatNLO
  predictions, which reach up to about 20\% only in the hard tails of the leptonic
  system.  

\item The picture changes when discussing the cross sections in the different
  jet bins, where sizable differences of up to about 20\% emerge between
  \MCatNLO and \MEPSatNLO predictions.  They are well understood and reflect the fact 
  that the 1-jet contribution in \MEPSatNLO is evaluated at logarithmically 
  improved \NLOacc accuracy, while in \MCatNLO the fixed order accuracy is 
  LO only.  
  It can be expected that this trend will also
  continue, and probably become more pronounced, when studying the
  2-jet bin, relevant for the weak boson fusion channel.  This is left for
  further investigations. 

\item Comparing \MEPSatNLO and \NLOacc predictions one finds only small deviations
    at $0.5\%$ and $3\%$ level in the 0- and 1-jet bin cross sections, respectively.

\item The combined theoretical uncertainties, as estimated 
  through renormalization-, factorization- and resummation-scale variations 
  in the \MEPSatNLO approach, amount to $3{-}4\%$, both in the 0- and 1-jets bins. 
  Resummation scale variations turn out to be rather small, 
  suggesting that subleading logarithms beyond the shower accuracy
  are not important. The few-percent level agreement between \NLOacc and \MEPSatNLO 
  cross sections in both jet bins indicates that also leading-log effects beyond
  \NLOacc are rather small.


\item Although the effect of hadronization and the underlying event
  have not been investigated here, it is clear that the former will not
  lead to uncertainties that are comparable in size to the ones already 
  present.  This is not quite so clear for the underlying event, which will
  influence the observable cross sections in two ways. First of all, a
  varying hadronic activity will lead to varying acceptance of isolated
  leptons.  At the same time, variations in the underlying event 
  activity or hardness may also lead to differences in the jet multiplicity
  distribution.  The quantification of such effects will be left to further
  studies.
\end{itemize}
