\section{BSM Higgs benchmarks in light of the discovery of a  126~\UGeV\ boson \footnote{%
    S.~Bolognesi, S.~Diglio, C.~Grojean, M.~Kadastik, H.E.~Logan, M.~Muhlleitner K.~Peters (eds.)}}
\label{sec:BSM}

\newcommand{\Ckappa}{\kappa}

This document provides a first proposition of a framework in which the
continuing LHC Higgs searches at masses other than $125\UGeV$ can be interpreted. 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}

After the discovery of a new Higgs-like boson with SM-like
properties by the LHC experiments ATLAS and CMS
\cite{Aad:2012tfa,Chatrchyan:2012ufa, Chatrchyan:2013lba} the next step is to determine whether this
Higgs-like particle is fully responsible for the generation of the
masses of the other SM particles. This means in other words, that it fully
unitarizes the high-energy scattering amplitudes for $\PVBL \PVBL \to \PVBL
\PVBL$ ($\PVB = \PW$ or $\PZ$) and $\PVBL \PVBL \to \Pf \bar{\Pf}$. 

If the Higgs-like particle at $125\UGeV$ is not fully responsible for the
unitarization of the scattering amplitudes, then additional new
physics must be present to play this role.  Here we propose two benchmarks
in which a second scalar particle completes the unitarization of
the scattering amplitudes.  The allowed values of the couplings of this
second particle are therefore constrained by the observed production
and decay rates of the $125\UGeV$ Higgs-like state. 

We try to match these benchmarks to the coupling extraction
parameterizations of the Light Mass Higgs Subgroup \cite{LHCHiggsCrossSectionWorkingGroup:2012nn}.  For each benchmark we give a model-independent parameterization as well as a particular realization in terms of a specific model.  In some cases, the specific model fixes the values of some of the free parameters.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Relations imposed by unitarity \label{sec:unitarity1}}

The couplings to gauge bosons and fermions of the $125\UGeV$ Higgs-like
state, denoted by $\Pho$ in the following, and the ones of the second
scalar particle, denoted by $\Pht$, are subject to constraints imposed
by unitarity. We introduce the scaling factors $\Ckappa_i$ and $\Ckappa^\prime_i$ ($i=\Pf,\PW,\PZ$)
for the couplings of $\Pho$ and $\Pht$ to the fermions and gauge bosons. The
coupling factors without prime apply for the couplings of the $125\UGeV$
Higgs-like state $\Pho$ and the one with prime for the second state
$\Pht$. The coupling factors are defined relative to the corresponding
couplings of a SM Higgs boson, as in the coupling-extraction
document \cite{LHCHiggsCrossSectionWorkingGroup:2012nn}. 
 
The requirement of unitarization of longitudinal gauge boson
scattering $\PVBL\PVBL \to \PVBL\PVBL$ ($\PVB=\PW,\PZ$) leads to the following sum
rule\footnote{This expression assumes that $\Ckappa_{\PW} = \Ckappa_{\PZ}$ as imposed by custodial symmetry.}:
\begin{equation}
  \Ckappa_{\PVB}^2 + \Ckappa_{\PVB}^{\prime 2} = 1.
\label{eq:cond1}
\end{equation}

The unitarization of longitudinal gauge boson scattering into a
fermion pair, $\PVBL \PVBL \to \Pf\bar{\Pf}$, requires that
\begin{equation}
  \Ckappa_{\PVB} \Ckappa_{\Pf} + \Ckappa_{\PVB}^{\prime} \Ckappa_{\Pf}^{\prime} = 1.
\label{eq:cond2}
\end{equation}
This equation holds separately for each fermion species.  Here
$\Ckappa_{\Pf}$ needs not be the same for different kinds of
fermions. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Proposed Benchmarks}

In the following we propose two benchmark scenarios. For each of them we
give a model-independent parameterization as well as a
particular realization in terms of a specific model.  In some cases,
the specific model fixes the values of some of the free
parameters. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Benchmark 1: One common scaling factor}

In this benchmark scenario a common scaling
factor is chosen both for the gauge and the fermion couplings of the field
$\Pho$ relative to the corresponding SM couplings. 
 
\noindent
{\it Model-independent parameterization:}  
For the $125\UGeV$ state $\Pho$ we then have
\begin{equation}
  \Ckappa \equiv \Ckappa_{\PVB} = \Ckappa_{\Pf}.
\label{eq:ccouplg1}
\end{equation}
This is equivalent to the overall signal strength scaling $\mu =
\Ckappa^2$. 

The corresponding coupling of the second state $\Pht$ is then
\begin{equation}
  \Ckappa^{\prime} \equiv \Ckappa_{\PVB}^{\prime} = \Ckappa_{\Pf}^{\prime} =  \sqrt{1 - \Ckappa^2}.
\label{eq:cprimecouplg1}
\end{equation}
While $\Ckappa^{\prime}$ can formally be of either sign, we choose the plus
sign with no loss of generality because only the relative signs of the
couplings of $\Pht$ are physically meaningful. 

The only other parameter affecting the rates for production and decay
of $\Pht$ is the branching ratio for possible decays into ``new'' final
states, BR$_{\mathrm{new}}$.  (For example, this new branching ratio can be due
to the decays $\Pht \to \Pho \Pho$ for $M_{\Pht} \geq 2 M_{\Pho} \simeq
250$~\UGeV). 

The relevant observables in the search for $\Pht$ are the $\Pht$ production cross
section $\sigma^{\prime}$, the $\Pht$ total width $\Gamma^{\prime}_{tot}$, and the
branching ratio BR$^{\prime}$ for $\Pht$ into the observable final state of interest.
From the expression for the total width $\Gamma^{\prime}$ of $\Pht$,
\begin{eqnarray}
	\Gamma^\prime = \Ckappa^{\prime 2} \Gamma_{\SM} + \Gamma_{\mathrm{new}},
\end{eqnarray}
it follows that these observables are given in terms of $\Ckappa^{\prime}$ and BR$_{\mathrm{new}}$ according to 
\begin{eqnarray}
	\sigma^{\prime} &=& \Ckappa^{\prime 2} \sigma_{\SM}, \nonumber \\
	\Gamma^{\prime} &=& \frac{\Ckappa^{\prime 2}}{(1 - {\rm
            BR}_{\mathrm{new}})} \Gamma_{\SM}, \nonumber \\
            {\rm BR}^{\prime} &=& (1 - {\rm BR}_{\mathrm{new}}) {\rm BR}_{\SM},
\end{eqnarray}
where $\sigma_{\SM}$, $\Gamma_{\SM}$, and BR$_{\SM}$ are the 
cross section, total width, and branching ratio into the final state of interest as predicted for the SM 
Higgs when its mass is equal to $M_{\Pht}$.  In the narrow width approximation,
the signal strength $\mu^{\prime}$ for $\Pht$ can be obtained using
\begin{equation}
	\mu^{\prime} = \frac{\sigma^{\prime} \times {\rm BR}^{\prime}}
	{\sigma_{\SM} \times {\rm BR}_{\SM}} 
	= \Ckappa^{\prime 2} (1 - {\rm BR}_{\mathrm{new}}).
\end{equation}
%The relevant observables in the search for $\Pht$ are the $\Pht$ signal strength
%$\mu^{\prime}$ (defined relative to that expected for a SM Higgs boson
%of the same mass) and the $\Pht$ total width
%$\Gamma^{\prime}_{\rm tot}$. From
%\begin{eqnarray}
%\Gamma^\prime_{\rm tot} = C^{\prime 2} \Gamma_{\SM} + \Gamma_{\mathrm{new}}
%\end{eqnarray}
%it follows that they are given in terms of $C^{\prime}$ and BR$_{\mathrm{new}}$ according to 
%\begin{eqnarray}
%  \mu^{\prime} &=& C^{\prime 2} (1 - {\mathrm BR}_{\mathrm{new}})
%  \\
%  \Gamma^{\prime}_{\rm tot} &=& \frac{C^{\prime 2}}{(1 - {\rm
%            BR}_{\mathrm{new}})} \Gamma_{\SM} \; . 
%\end{eqnarray}
In \refF{fig:bm1} we show the signal strength $\mu^\prime$ and the
total width $\Gamma^\prime$ in units of the total width of the SM
Higgs boson in the plane $(\Ckappa^{\prime 2}, {\rm BR}_{\mathrm{new}})$. 

\begin{figure}
\begin{center}
\includegraphics[angle=0,width=0.7\textwidth]{YRHXS3_BSM/bm1plot-logo_v2.eps}
\caption{The $\Pht$ signal strength $\mu^{\prime}$ and the $\Pht$ total
  width $\Gamma^\prime$ in units of the SM Higgs total width
  $\Gamma_{\SM}$ for Benchmark~1 (one common scaling factor) in the
  $(\Ckappa^{\prime 2}, {\rm BR}_{\mathrm{new}})$ plane.}
\label{fig:bm1}
\end{center}
\end{figure}

{\it Constraints from existing data:}  ATLAS and CMS have measured
$\mu \equiv \sigma/\sigma_{\SM}$ for the $125\UGeV$ boson.  The
results are (as of March 2013)~\cite{ATLAS-CONF-2013-034,CMS-PAS-HIG-13-005}
\begin{eqnarray}
	{\rm ATLAS}: && \mu = 1.30 \pm 0.20 \nonumber \\
	{\rm CMS}: && \mu = 0.88 \pm 0.21 \;.
\end{eqnarray}
Taking the uncertainty to be Gaussian, these
correspond to a $2\sigma$ lower bound on $\mu$ and hence an upper
bound of $\Ckappa^{\prime 2}$ of 
\begin{eqnarray}
	{\rm ATLAS}: && \mu > 0.90 \quad \rightarrow \quad \Ckappa^{\prime 2} < 0.10 \nonumber \\
	{\rm CMS}: && \mu > 0.46 \quad \rightarrow \quad \Ckappa^{\prime 2} < 0.54 \;.
\end{eqnarray}
Here we have assumed that the branching ratio of the $125\UGeV$ Higgs $\Pho$ into 
non-SM final states is zero. 

\noindent
{\it Specific model:} This parameterization is realized for the SM
Higgs boson mixed with an electroweak singlet.  In this case,
BR$_{\mathrm{new}}$ arises from decays of $\Pht \to \Pho \Pho$ for $M_{\Pht} > 2
M_{\Pho} \simeq 250$~\UGeV.  The Lagrangian and Feynman rules, are given
in \refS{sec:modelintro}. 

%\noindent
%{\it Constraints from existing data:}  ATLAS and CMS have measured
%$\mu \equiv \sigma/\sigma_{\SM}$ for the $125\UGeV$ boson.  The
%results are (as of July 2012)~\cite{Aad:2012tfa,Chatrchyan:2012ufa, Chatrchyan:2013lba}
%\begin{eqnarray}
%  {\rm ATLAS}: && \mu = 1.4 \pm 0.3 \nonumber \\
%  {\rm CMS}: && \mu = 0.87 \pm 0.23 \;.
%\end{eqnarray}
%Taking the uncertainty to be Gaussian, these
%correspond to a $2\sigma$ lower bound on $\mu$ and hence an upper
%bound of $C^{\prime 2}$ of 
%\begin{eqnarray}
%  {\rm ATLAS}: && \mu > 0.8 \rightarrow C^{\prime 2} < 0.2 \nonumber \\
%  {\rm CMS}: && \mu > 0.41\rightarrow C^{\prime 2} < 0.59.
%\end{eqnarray}
%Here we have assumed that the branching ratio into new final states
%${\mathrm BR}_{\mathrm{new}}$ is zero.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Specific benchmark model 1: Standard Model plus a Real Singlet Field \label{sec:modelintro}}
The simplest extension of the SM Higgs sector is given by the addition
of a singlet field which is neutral under the SM gauge
groups~\cite{Hill:1987ea, Veltman:1989vw, Binoth:1996au,
  Schabinger:2005ei, Patt:2006fw}. 
This singlet field also acquires a non-vanishing
vacuum expectation value. Such models have been discussed in
numerous publications
\cite{Bowen:2007ia,Barger:2007im, Barger:2008jx,Bhattacharyya:2007pb,Dawson:2009yx,Bock:2010nz,
  Fox:2011qc,Englert:2011yb,Englert:2011us,Batell:2011pz,Englert:2011aa, Gupta:2011gd, Dolan:2012ac, Bertolini:2012gu, Batell:2012mj, Pruna:2013bma}
and we shall give details in the following.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The model}

The most general gauge-invariant potential
can be written as \cite{Schabinger:2005ei,Bowen:2007ia}
\begin{equation}
  V = \lambda \left( \Phi^{\dagger} \Phi - \frac{v^2}{2} \right)^2 
  + \frac{1}{2} M^2 s^2
  + \lambda_1 s^4 
  + \lambda_2 s^2 \left( \Phi^{\dagger} \Phi - \frac{v^2}{2} \right)
  + \mu_1 s^3 
  + \mu_2 s \left( \Phi^{\dagger} \Phi - \frac{v^2}{2} \right),
  \label{eq:potential}
\end{equation}
where $s$ is the real singlet scalar and in the unitary gauge the SM
Higgs doublet can be written as 
\begin{equation}
  \Phi = \left( \begin{array}{c} 0 \\ (\phi +
            v)/\sqrt{2} \end{array} \right)
\label{eq:phifield}
\end{equation}
with $v \simeq 246\UGeV$.  We have already used the freedom to shift the 
value of $s$ so that $s$ does not get a vacuum expectation value.
As a result, $M^2$ must be  chosen positive in Eq.~\refE{eq:potential}. 

To prevent the potential from being unbounded from below, the quartic couplings
must satisfy the conditions:
\begin{equation}
  \lambda > 0, \qquad \lambda_1 > 0, \qquad \lambda_2 > -2 \sqrt{\lambda \lambda_1}.
\end{equation}
The trilinear couplings $\mu_1$ and $\mu_2$ can have either sign.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Mass eigenstates}

%After replacing Eq.~\refE{eq:phifield} in the potential of Eq.
%\refE{eq:potential}, the terms quadratic in the fields (that give
%rise to the mass matrix) are 
%\begin{equation}
%  V_2 = \lambda v^2 \phi^2 + \frac{1}{2} M^2 s^2 + \mu_2 v \, \phi s.
%\end{equation}
After replacing Eq.~(\ref{eq:phifield}) for $\Phi$ in the potential
Eq.~(\ref{eq:potential}), we obtain
\begin{equation}
V    =     \frac{\lambda}{4} \phi^4 + \lambda v^2 \phi^2 + \lambda v \phi^3 + \frac{1}{2}M^2 s^2 + \lambda_1 s^4 
               + \frac{\lambda_2}{2} \phi^2 s^2 +  \lambda_2 v \phi s^2 + \mu_1 s^3 + \frac{\mu_2}{2} \phi^2 s
               + \mu_2 v \phi s \;.
\end{equation}
The terms quadratic in the fields (that give
rise to the mass matrix) are 
\begin{equation}
	V_2 = \lambda v^2 \phi^2 + \frac{1}{2} M^2 s^2 + \mu_2 v \, \phi s.
\end{equation}
In particular, the mixing between $\phi$ and the singlet
field $s$ is controlled by the coupling $\mu_2$.  
The mass eigenvalues are then given by
\begin{equation}
  M^2_{\Pho, \Pht} = \lambda v^2 + \frac{1}{2} M^2 
  \mp \sqrt{ \left( \lambda v^2 - \frac{1}{2} M^2 \right)^2 +
          \mu_2^2 v^2 } \, ,
\end{equation}
where we have defined the mass eigenstates $\Pho,\Pht$ as
\begin{eqnarray}
  \Pho &=& \phi \cos\theta - s \sin\theta \nonumber \\
  \Pht &=& \phi \sin\theta + s \cos\theta,
\end{eqnarray}
with the mixing angle $\theta$ which can be written as
\begin{equation}
  \tan 2 \theta = \frac{-\mu_2 v}{\lambda v^2 - \frac{1}{2} M^2}
        \; .
\end{equation}

In order to find the domain of $\theta$ we can rewrite the masses as follows:
\begin{equation}
M^2_{\Pho, \Pht} = \left( \lambda v^2 + \frac{1}{2} M^2 \right)
  \mp \left( \frac{1}{2} M^2 - \lambda v^2\right) \sec 2\theta 
\end{equation}
If we require $\Pho$ to be the lighter mass eigenstate and choose $M^2
>  2 \lambda v^2$, then $\sec 2 \theta > 0$, and hence $\theta \in (-
\frac{\pi}{4}, \frac{\pi}{4})$. 

Note that in the notation of Eq.~\refE{eq:ccouplg1} and
\refE{eq:cprimecouplg1} 
we have in particular
\begin{eqnarray}
\Ckappa &\equiv& \Ckappa_{\PVB} = \Ckappa_{\Pf} = \cos \theta \\
\Ckappa^\prime &\equiv& \Ckappa_{\PVB}^\prime = \Ckappa_{\Pf}^\prime = \sin\theta \;.
\end{eqnarray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The trilinear and quartic interactions}

Here, we give the trilinear and quartic interactions among the mass
eigenstates $\Pho$ and $\Pht$. The related Feynman rules are necessary
for the determination of the Higgs-to-Higgs decays, namely the decay
of the heavier state $\Pht$ into two lighter bosons $\Pho \Pho$. 

%Once we substitute Eq.~\refE{eq:phifield} for $\Phi$ in the potential we get
%\begin{equation}
%V    =     \frac{\lambda}{4} \phi^4 + \lambda v^2 \phi^2 + \lambda v \phi^3 + \frac{1}{2}M^2 s^2 + \lambda_1 s^4 
%               + \frac{\lambda_2}{2} \phi^2 s^2 +  \lambda_2 v \phi s^2 + \mu_1 s^3 + \frac{\mu_2}{2} \phi^2 s
%               + \mu_2 v \phi s \;.
%\end{equation}
The relevant terms for the cubic interactions are 
\begin{equation}
  V_3 =  \lambda v \phi^3 +  \lambda_2 v \phi s^2  +  \mu_1
        s^3 +  \frac{\mu_2}{2} \phi^2 s \; .
\end{equation}
Using the shorthands $s_\theta \equiv \sin \theta$ and $c_\theta
\equiv \cos \theta$ and rewriting the trilinear terms of the potential
in terms of the mass eigenstates, we find the following couplings.

After replacing $\phi$ and $s$ by the mass eigenstates $\Pho$ and
$\Pht$, the triple-$\Pho$ coupling comes from the potential term
\begin{equation}
  V_{111} = \Pho^3 \left[  \lambda v c_\theta^3     +  \lambda_2  v  c_\theta s_\theta^2   
  - \mu_1 s_\theta^3  - \frac{\mu_2}{2} c_\theta^2 s_\theta   \right],
\end{equation}
which yields a Feynman rule
\begin{equation}
		\label{eq:111}
  \Pho \Pho \Pho:\ -6 i \left( \lambda v c_\theta^3   + \lambda_2 v c_\theta s_\theta^2   
  - \mu_1  s_\theta^3  - \frac{\mu_2}{2} c_\theta^2 s_\theta   \right)
  	\equiv - i L_{111}.
\end{equation}

The $\Pht \Pho \Pho$ coupling, which controls the $\Pht \to \Pho \Pho$ decay (if kinematically allowed), comes from the potential term
\begin{equation}
  V_{211} = \Pht \Pho^2 \left[       3 \lambda v s_{\theta} c_{\theta}      
   - \lambda_2  v  s_\theta  (  3 c_\theta^2  -1  )  
    + 3 \mu_1 c_\theta s_\theta^2 + \frac{\mu_2}{2} c_\theta (3 c_\theta^2 -2 )  \right],
\end{equation}
and yields the Feynman rule
\begin{equation}
	\label{eq:211}
  	\Pht \Pho \Pho  : \ -2 i \left(   3 \lambda v s_\theta c_\theta       
   -  \lambda_2  v s_\theta (3 c_\theta^2   - 1) 
  + 3 \mu_1  c_\theta s_\theta^2  + \frac{\mu_2}{2} c_\theta (3 c_\theta^2  - 2)   \right).
  		\equiv - i L_{211}.
\end{equation}

For the other cubic interactions we have the potential terms and
related Feynman rules:
\begin{eqnarray}
  V_{122} &=&   \Pho \Pht^2  \left[  -3 \lambda v c_\theta s_\theta^2   + \lambda_2  v c_\theta (1 - 3 s_\theta^2)  -\frac{\mu_2}{2} (2 - 3 s_\theta^2) \right] ,      \nonumber \\
  \Pho \Pht \Pht &:&    -2 i \left( -3 \lambda v c_\theta s_\theta^2   + \lambda_2  v  c_\theta (1 - 3 s_\theta^2)  - \frac{\mu_2}{2} s_\theta (2 - 3 s_\theta^2)  \right); \nonumber \\
  V_{222}  &=&  \Pht^3 \left[\lambda v s_\theta^3  +\lambda_2 v s_\theta c_\theta^2 + \mu_1 c_\theta^3 -\frac{\mu_2}{2} s_\theta^2 c_\theta  \right] ,     \nonumber \\
 	\Pht \Pht \Pht &:& -6 i \left(  \lambda  v  s_\theta^3  +
          \lambda_2 v s_\theta c_\theta^2  + \mu_1 c_\theta^3   -
          \frac{\mu_2}{2} s_\theta^2 c_\theta \right) \; .
\end{eqnarray}

For the quartic interactions, the relevant terms in the Lagrangian are,
\begin{equation}
  V_4 = \frac{\lambda}{4} \phi^4   +  \lambda_1  s^4  +
        \frac{\lambda_2}{2}  \phi^2 s^2 \;.
\end{equation}
After inserting the mass eigenstates in terms of the mixing angle
$\theta$, we find for the quartic potential terms and the
corresponding Feynman rules:
\begin{eqnarray}
  V_{1111} &=&  \Pho^4 \left[   \frac{\lambda}{4} c_\theta^4  + \lambda_1 s_\theta^4 + \frac{\lambda_2}{2} c_\theta^2 s_\theta^2   \right],  \nonumber \\
  \Pho \Pho \Pho \Pho &:&    -24 i \left(  \frac{\lambda}{4} c_\theta^4  + \lambda_1  s_\theta^4  + \frac{\lambda_2}{2} c_\theta^2 s_\theta^2    \right);    \nonumber \\
  V_{2222} &=&   \Pht^4 \left[   \frac{\lambda}{4} s_\theta^4  + \lambda_1  c_\theta^4  + \frac{\lambda_2}{2}  s_\theta^2  c_\theta^2   \right], \nonumber \\
  \Pht \Pht \Pht \Pht &:&   -24 i \left(  \frac{\lambda}{4} s_\theta^4   + \lambda_1 c_\theta^4  + \frac{\lambda_2}{2}  s_\theta^2   \right);   \nonumber \\
  V_{1112} &=&   \Pho^3 \Pht \left[   \lambda_1 s_\theta c_\theta^3  - 4 \lambda_1 c_\theta s_\theta^3  - \frac{\lambda_2}{4} \sin 4 \theta    \right], \nonumber \\
  \Pho \Pho \Pho \Pht &:&     -6 i \left(  \lambda s_\theta c_\theta^3  - 4 \lambda_1 c_\theta s_\theta^3  - \frac{\lambda_2}{4} \sin 4 \theta \right);    \nonumber \\
  V_{1122}    &= &  \Pho^2  \Pht^2  \left[   \frac{3}{2} \lambda s_\theta^2 c_\theta^2  + 6 \lambda_1 s_\theta^2 c_\theta^2  +
   \frac{\lambda_2}{2} (c_\theta^4 + s_\theta^4   - 4 c_\theta^2  s_\theta^2)     \right] ,  \nonumber  \\
 \Pho \Pho \Pht \Pht  &:&      -4 i \left(  \frac{3}{2} \lambda  s_\theta^2 c_\theta^2  + 6 \lambda_1 s_\theta^2 c_\theta^2  + 
  \lambda_2 (c_\theta^4  + s_\theta^4 - 4 c_\theta^2 s_\theta^2)  \right)  ;    \nonumber \\
  V_{1222}    & =&   \Pho \Pht^3   \left[   \lambda c_\theta s_\theta^3  - 4 \lambda_1 s_\theta c_\theta^3  +\frac{\lambda_2}{4} \sin 4 \theta  \right]  ,  \nonumber   \\
  \Pho \Pht \Pht \Pht  &: &  -6 i \left(  \lambda  c_\theta  s_\theta^3   - 4 \lambda_1  s_\theta c_\theta^3   +\frac{\lambda_2}{4} \sin 4 \theta \right).
\end{eqnarray}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Counting of free parameters}

The most general scalar potential in Eq.~(\ref{eq:potential}) contains six parameters, $\lambda$, $M^2$, $\lambda_1$, $\lambda_2$, $\mu_1$, and $\mu_2$.  The masses of $\Pho$ and $\Pht$ and the mixing angle $\theta$ are determined by the three parameters $\lambda$, $M^2$, and $\mu_2$.  The physically most interesting trilinear scalar couplings, $\Pho\Pho\Pho$ (the triple-Higgs coupling) and $\Pht \Pho \Pho$ (which controls $\Pht \to \Pho \Pho$ decays, if kinematically accessible) depend in addition on $\mu_1$ and $\lambda_2$.  There is enough parameter freedom to choose these two trilinear couplings independently.  The remaining parameter $\lambda_1$ appears only in quartic scalar interactions.

Two useful bases in which the model can be specified are
\begin{equation}
	M_{\Pho}, M_{\Pht}, \cos\theta, \mu_1, \lambda_2, \lambda_1
\end{equation}
and [see Eqs.~(\ref{eq:111}) and~(\ref{eq:211})]
\begin{equation}
	M_{\Pho}, M_{\Pht}, \cos\theta, L_{111}, L_{211}, \lambda_1.
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Benchmark 2: Scaling of vector boson and fermion couplings}

In the second benchmark scenario, the couplings of the $125\UGeV$ Higgs
boson to the gauge bosons and to the fermions are scaled by two
different coupling factors. 

\noindent
{\it Model-independent parameterization:}  For the $125\UGeV$ state
$\Pho$, the production rates in the various channels of observation can
be fit to two free parameters, 
\begin{eqnarray}
  \Ckappa_{\PVB} &\equiv& \Ckappa_{\PW} = \Ckappa_{\PZ} \;, \nonumber \\
  \Ckappa_{\PCF} &\equiv& \Ckappa_{\PQt} = \Ckappa_{\PQb} = \Ckappa_{\PGt} \; .
\end{eqnarray}
We assume that the $\Pho$ couplings to light fermions are scaled by the same factor $\Ckappa_{\PCF}$.
The couplings of $\Pho$ to $\Pg\Pg$ and $\PGg\PGg$ are also modified by the appropriate scaling factors applied to the fermion and $\PW$ boson loops.  

We assume that there are no new 
colored particles that contribute to the loop-induced $\Pho \Pg\Pg$ or $\Pht \Pg\Pg$ couplings.  In the 
specific model that we discuss below, there is a charged scalar that can contribute to the 
loop-induced $\Pho\PGg\PGg, \Pho\PGg \PZ$ and $\Pht\PGg\PGg, \Pht \PGg \PZ$ couplings; for this benchmark we assume that its contributions to these loop-induced couplings are negligible.


The corresponding couplings of the second state $\Pht$, as enforced by unitarity, are
\begin{eqnarray}
  \Ckappa_{\PVB}^{\prime} &=& \sqrt{1 - \Ckappa_{\PVB}^2}, \nonumber \\
  \Ckappa_{\PCF}^{\prime} &=& \frac{1 - \Ckappa_{\PVB} \Ckappa_{\PCF}}{\sqrt{1 - \Ckappa_{\PVB}^2}}.
\end{eqnarray}
We have chosen the phase of $\Pht$ such that $\Ckappa_{\PVB}^{\prime}$ is
positive.  $\Ckappa_{\PCF}^{\prime}$ can be positive or negative.  
As in the previous benchmark, there can be additional decays of $\Pht$ with a branching ratio BR$_{\mathrm{new}}$ (for example, $\Pht \to \Pho \Pho$ if kinematically allowed).

\noindent

{\it Constraints from existing data:}  The allowed ranges of
$\Ckappa_{\PVB}^{\prime}$ and $\Ckappa_{\PCF}^{\prime}$ are constrained by
fits of $\Ckappa_{\PVB}$ and $\Ckappa_{\PCF}$ from measurements of the 125 GeV
Higgs $\Pho$, as shown in Fig.~5a of \Bref{ATLAS-CONF-2013-034} and Fig.~13a
of \Bref{CMS-PAS-HIG-12-045} (in these figures $\Ckappa_{\PVB} \equiv
\kappa_{\PVB}$ and $\Ckappa_{\PCF} \equiv \kappa_{\PCF}$).  A theorist-made
translation of ATLAS, CMS, and Tevatron Higgs data available in July 2012 (see
\Bref{Espinosa:2012im} for the details of the data used in this analysis) into
constraints on $\Ckappa_{\PVB}^{\prime}$ and $\Ckappa_{\PCF}^{\prime}$ is
shown in \refF{fig:bm2}.  

\begin{figure}
\begin{center}
\includegraphics[width=0.7\textwidth]{YRHXS3_BSM/2HDM-typeI_v2.eps}
\caption{Best-fit points (black dots), 1$\sigma$ (green) and 2$\sigma$ (white)
  allowed regions in 
  the $(\Ckappa_{\PVB}^{\prime}$, $\Ckappa_{\PCF}^{\prime})$ plane for Benchmark 2 (equivalent
  to the Type-I two-Higgs-doublet model), based on data available in
  July 2012 (see \Bref{Espinosa:2012im}).}
\label{fig:bm2}
\end{center}
\end{figure}

\noindent
{\it Specific model:} 
This parameterization is realized for the SM Higgs boson mixed with a
second scalar electroweak doublet that carries a vacuum expectation
value but does not couple to fermions; i.e., the Type-I Two Higgs
Doublet Model (2HDM). This model will be specified in the following
\refS{sec:2hdm}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Specific benchmark model 2: Type-I
  Two-Higgs-Doublet-Model \label{sec:2hdm}}
The simplest extensions of the SM Higgs sector are given by adding
scalar doublet and singlet fields. Thus in two-Higgs-doublet models
\cite{Lee:1973iz,Gunion:2002zf, Branco:2011iw} the Higgs sector consists of two Higgs doublet fields. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The scalar Higgs potential}
In the following we assume CP conservation. Furthermore, we take care
to avoid tree-level flavor changing neutral currents. The latter is achieved by
imposing a discrete symmetry ${\mathbb Z}_2$ under which one of the doublet fields
changes sign, while all the fermion fields remain unchanged.  Denoting by $\Phi_1$ and
$\Phi_2$ two hypercharge-one weak doublet fields, for the Type-I 2HDM we have in
particular:
\begin{eqnarray}
\underline{\mbox{Type-I 2HDM: }} \qquad 
\Phi_1 \to -\Phi_1 \quad \mbox{ and } \quad \Phi_2 \to
\Phi_2 \;. \label{eq:type1}
\end{eqnarray} 
This ensures that the fermions couple only to $\Phi_2$.

The most general scalar Higgs potential is then given by
\begin{eqnarray}
V &=& m_{11}^2 \Phi_1^\dagger \Phi_1 + m_{22}^2 \Phi_2^\dagger \Phi_2
- [m_{12}^2 \Phi_1^\dagger \Phi_2 + {\mbox h.c.}] + \frac{1}{2}
\lambda_1 (\Phi_1^\dagger \Phi_1)^2 + \frac{1}{2} \lambda_2
(\Phi_2^\dagger \Phi_2) \nonumber \\
&& + \lambda_3 (\Phi_1^\dagger \Phi_1) (\Phi_2^\dagger \Phi_2) +
\lambda_4 (\Phi_1^\dagger \Phi_2) (\Phi_2^\dagger \Phi_1) + \left\{
  \frac{1}{2} \lambda_5 (\Phi_1^\dagger \Phi_2)^2 + {\mbox h.c.}
\right\} \;.
\end{eqnarray}
This potential softly violates (by dimension-two terms) the imposed
discrete ${\mathbb Z}_2$ symmetry. The parameters $m_{12}^2$ and $\lambda_5$ are taken
real to ensure CP conservation. Requiring that the minimum of the
potential is given by a CP-conserving vacuum which does not break the
electromagnetic symmetry $U(1)_{\mathrm em}$, we have for the vacuum
expectation values of the neutral components $\Phi_i^0$ ($i=1,2$) of the two Higgs
doublets, 
\begin{eqnarray}
\langle \Phi_i^0 \rangle = \frac{v_i}{\sqrt{2}} \quad \mbox{with}
\quad \tan\beta \equiv \frac{v_2}{v_1} \quad \mbox{and} \quad v^2 =
v_1^2 + v_2^2 = (246 \mbox{ \UGeV})^2 \;.
\end{eqnarray}
By imposing the minimum conditions of the potential the parameters
$m_{11}^2$ and $m_{22}^2$ can be eliminated in favor of $v^2$ and
$\tan\beta$, so that we are left with six free parameters, $m_{12}^2$
and $\lambda_j$ ($j=1,...,5$). Rotation of the interaction states to
the mass eigenstates results in four physical Higgs bosons, two
neutral CP-even ones, denoted by $\Ph$ for the lighter and by $\PH$ for
the heavier one, one neutral CP-odd state $\PA$ and a charged Higgs
boson $\PH^{\pm}$. The masses of these four particles, together with 
the neutral CP-even Higgs mixing angle
$\alpha$ introduced to diagonalize the neutral CP-even Higgs squared-mass
matrix, can be expressed in terms of these six parameters, so
that one free parameter is left over.
For further discussion on the
scalar potential, its symmetries and bounds on the parameters we
refer to the literature \cite{Gunion:2002zf, Branco:2011iw}. Here we content ourselves
to give the Higgs couplings to gauge bosons and fermions. 

The couplings of the neutral CP-even Higgs bosons $\Ph,\PH$ to the gauge
bosons $\PVB$ ($\PVB=\PW^\pm, \PZ$) normalized to the corresponding SM coupling are given by
\begin{eqnarray}
g_{\Ph\PVB\PVB} = \sin (\beta-\alpha) \qquad \mbox{and} \qquad g_{\PH\PVB\PVB} = \cos
(\beta - \alpha) \; . \label{eq:gaugecouplgs}
\end{eqnarray}
The pseudoscalar $\PA$ does not couple to gauge bosons. With the
notation of \refS{sec:unitarity1} we identify
\begin{eqnarray}
\Ckappa_{\PVB} \equiv g_{\Ph\PVB\PVB} \qquad \mbox{and} \qquad \Ckappa^\prime_{\PVB} \equiv g_{\PH\PVB\PVB}
\end{eqnarray}
The couplings
Eq.~\refE{eq:gaugecouplgs} fulfill the constraint Eq.~\refE{eq:cond1}
imposed by the requirement of unitarity of the longitudinal gauge
boson scattering. Furthermore, we recover the SM limit for $h$ in case
$\sin (\beta-\alpha) =1$. Note, that there can also be scenarios where
$H$ corresponds to the $125\UGeV$ SM-like Higgs boson, in which case $\cos
(\beta-\alpha)$ is close to $1$. 

As stated above, 2HDMs suffer from possible tree-level
flavor-changing neutral currents (FCNC). According to the
Paschos--Glashow--Weinberg theorem \cite{Glashow:1976nt, Paschos:1976ay}, FCNC are absent if
all fermions with the same quantum numbers couple to the same Higgs
multiplet. In the 2HDMs this can be achieved by imposing discrete or
continuous symmetries. According to the symmetries imposed there are
different types of 2HDM, where we discuss here the type-I model,
defined in Eq.~\refE{eq:type1}. In this case the Yukawa Lagrangian is
given in the mass basis by~\cite{Aoki:2009ha}
\begin{eqnarray}
{\mathcal L}_{\rm Yukawa} &=& - \sum_{\Pf=\PQu,\PQd,\Pl} \frac{m_{\Pf}}{v} \left( \xi_h^{\Pf}
  \bar{\Pf} \Pf \Ph + \xi_{\PH}^{\Pf} \bar{\Pf} \Pf \PH - i\xi_{\PA}^{\Pf} \bar{\Pf} 
\gamma_5 \Pf \PA
\right) \nonumber \\
&& - \left\{ \frac{\sqrt{2} V_{\PQu\PQd}}{v} \bar{u} (m_{\PQu} \xi_A^{\PQu} P_L + m_{\PQd}
\xi_{\PA}^d P_R) d \, \PH^+ + \frac{\sqrt{2} m_{\Pl} \xi_{\PA}^{\Pl}}{v} \bar{\nu}_L {\Pl}_R
H^+ + \mbox{ h.c. } \right\} \;,
\end{eqnarray}
where $\PQu,\PQd,\Pl$ stand generically for up-type quarks, down-type quarks, and charged
leptons of all three generations, respectively, $P_{L,R}$ are the projection operators
of the left- and right-handed fermions,  and $V_{\PQu\PQd}$ denotes the appropriate element
of the CKM matrix. We have
\begin{eqnarray}
\underline{\mbox{Type-1 2HDM:}} \quad && \xi_h^u = \xi_h^d = \xi_h^l \equiv
\xi_h = \frac{\cos \alpha}{\sin \beta} \label{eq:fermioncouplgs1}\\
&& \xi_H^u = \xi_H^d = \xi_H^l \equiv \xi_H = \frac{\sin \alpha}{\sin \beta} \;.
\label{eq:fermioncouplgs2}
\end{eqnarray}
In our notation of \refS{sec:unitarity1} we have
\begin{eqnarray}
\Ckappa_{\Pf} \equiv \xi_h \qquad \mbox{and} \qquad \Ckappa^\prime_{\Pf} \equiv \xi_H \;.
\end{eqnarray}
It is easy to verify that the Yukawa couplings
Eq.~\refE{eq:fermioncouplgs1}, Eq.~\refE{eq:fermioncouplgs2} and the
couplings to gauge bosons 
Eq.~\refE{eq:gaugecouplgs} fulfill the unitarity conditions Eq.~\refE{eq:cond2}.

It is instructive to look the decoupling limit, {\it i.e.}, when $m_{\PH} \gg m_{\Ph}$ which is obtained for $\alpha \to \beta -\pi/2$:
\begin{eqnarray}
& \displaystyle \Ckappa_{\PVB}  = 1 +  {\mathcal O}\!\left(  \frac{\MZ^4}{\MH^4} \right), \\
& \displaystyle \Ckappa_{\Pf} = 1+ 2\, \frac{\MZ^2}{\MH^2}\cos^2\!\beta \cos 2 \beta +
  {\mathcal O}\!\left(  \frac{\MZ^4}{\MH^4} \right).
\end{eqnarray}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tools}
Here we give a short collection of tools which can be exploited in the
frameworks of the benchmark models 1 and 2. 

\noindent {\it Production:} As in the proposed benchmark models only
the Higgs couplings to the gauge bosons and fermions are modified, the
SM production cross sections can be taken over including higher order
QCD corrections. They only have to be multiplied by the appropriate
scaling factor. Since in both models the fermion modification factors for
each of the two Higgs bosons are universal, this is also  particularly easy for gluon
fusion, as not distinction between top and bottom
loops needs to be made. As for electroweak (EW) corrections, however, they
cannot be taken over from SM calculations.EW corrections in the BSM models can be substantially different from the SM
EW corrections.  As long as no dedicated analysis has been
performed, no statements about the possible size of these corrections can be made. Hence,
we have in benchmark model 1 and 2 for the QCD corrected production
cross sections of $\Pho$, $\Pht$ through gluon fusion ($\Pg\Pg$), vector boson
fusion (VBF), Higgs-strahlung (VH) and associated production with
heavy quarks (QQH),
\begin{eqnarray}
\sigma^h_{\Pg\Pg} = {\bar \Ckappa}_{\Pf}^2\, \sigma^{\SM,\QCD}_{\Pg\Pg} \; , \;
\sigma^h_{\mathrm{VBF}} = {\bar \Ckappa}_{\PVB}^2\, \sigma^{\SM,\QCD}_{\mathrm{VBF}} \; , \;
\sigma^h_{\mathrm{VH}} = {\bar \Ckappa}_{\PVB}^2\, \sigma^{\SM,\QCD}_{\mathrm{VH}} \; , \;
\sigma^h_{\mathrm{QQH}} = {\bar \Ckappa}_{\Pf}^2\, \sigma^{\SM,\QCD}_{\mathrm{QQH}} \;,
\end{eqnarray}
where
\begin{eqnarray}
{\bar \Ckappa}_{\Pf} = \Ckappa_{\Pf} \;(\Ckappa^\prime_{\Pf}) \quad \mbox{for} \quad h=\Pho \;(\Pht) \qquad
\mbox{and} \qquad
{\bar \Ckappa}_{\PVB} = \Ckappa_{\PVB} \;(\Ckappa^\prime_{\PVB}) \quad \mbox{for} \quad h=\Pho \;(\Pht) \;.
\end{eqnarray}
For the programs which allow for the calculation of the production
cross sections at higher order QCD we refer the reader to
YR1. 

The program \textsc{SUSHI} \cite{Harlander:2012pb, sushi} has implemented
the calculation of neutral Higgs bosons $\Ph,\PH,\PA$ within the 2HDM
through gluon fusion and bottom quark annihilation. This program can be applied up to
NNLO QCD if the Higgs mass stays below twice the top quark
mass (this is because the NNLO QCD corrections rely upon an approximation which is valid for
$M_{\Ph} < 2 m_{\PQt}$). The NLO QCD corrections are exact for all Higgs boson
masses. Electroweak corrections have to be turned off for consistency
reasons, since the electroweak corrections depend on the details of the BSM model
(including couplings between scalars that are highly model-dependent) and have not been implemented.

The program \textsc{gg2VV} \cite{Kauer:2012hd,gg2VV} is a parton-level integrator and event 
generator for all $\Pg\Pg\ (\to \PH)\to \PV\PV \to 4$ leptons processes 
$(\PV\!=\!\PW,\PZ/\PGg^\ast)$, which can be used to study Higgs-continuum 
interference and off-shell effects.
It can be used to calculate predictions for BSM scenarios with
a SM-like Higgs boson with rescaled $\PH\Pg\Pg$, $\PH\PW\PW$ and 
$\PH\PZ\PZ$ couplings.  Full BSM implementations in
\textsc{gg2VV} are not public yet. The program has, however, been used already for
calculations and checks in models in which all Higgs
couplings are modified by a common scaling factor. 

\noindent {\it Decays:} For the calculation of the decay branching
ratios in benchmark model 1 the program \textsc{HDECAY} \cite{hdecay} can
be used. It has implemented the possibility to turn on modification
factors for the SM Higgs couplings to fermions and gauge bosons. This
way the branching ratios for $\Pho$ and $\Pht$ can be calculated
separately by specifying in the input file the appropriate scaling
factors. The program includes both QCD and EW corrections computed
for the SM Higgs, which can only be applied in the vicinity of the SM. 
The new program \textsc{eHDECAY}~\cite{Contino:2013kra, ehdecay} has been adapted from 
\textsc{HDECAY} to implement the
calculation of Higgs branching ratios in the framework of effective
Lagrangians.  \textsc{eHDECAY} provides the possibility to turn off the SM EW
corrections, which should be done in the benchmark models described here.  By specifying in the input file the appropriate scaling factors and setting the flag for EW corrections to zero, the branching ratios for
$\Pho$, $\Pht$ can be separately calculated 
including the QCD corrections and without the EW corrections. However, neither
program has implemented the decays into new states as {\it e.g.}
the decay $\Pht \to \Pho \Pho$. The authors of these programs plan to add this 
decay in the future. 

For the calculation of the branching ratios in the 2HDM there is a
dedicated tool, \textsc{2HDMC}~\cite{Eriksson:2009ws, 2hdmc}. This program calculates all two-body
decay widths and branching ratios at leading order (including FCNC) within
different parameterizations which can be specified in the input
file. Leading QCD corrections to the decays are included. The program
also includes singly off-shell decays into scalar-plus-vector-boson and
two-vector-boson final states. 
Furthermore, theoretical constraints
on the 2HDM parameters from perturbativity, unitarity and stability
of the scalar potential are included, as are the constraints from EW
precision data (via the oblique parameters). Model constraints from
Higgs searches and flavor physics can be accessed using external
codes. The code provides output in a form similar to the SUSY Les
Houches accord~\cite{Skands:2003cj, Allanach:2008qq, Mahmoudi:2010iz}, 
which can also be used for Monte Carlo event
generation with a supplied model file for \textsc{MadGraph/MadEvent}.



