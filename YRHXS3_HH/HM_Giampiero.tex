%\documentclass[10pt]{article}
%---
%\usepackage{heppennames2}
%\usepackage{lhchiggs}
%\usepackage{cernunits}
%\usepackage{hepparticles}
%---
\hyphenation{ma-ni-pu-la-tions}
\allowdisplaybreaks
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% basic data for the eprint:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\textwidth=6.5in  \textheight=8.7in
%\leftmargin=-0.8in   \topmargin=-0.20in
%\hoffset=-.85in

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--
%- Local macros
%--


%--

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%\begin{document}
%--
\subsubsection{CPS - scheme \label{HMcpss}}
%--
The general formalism for describing unstable particles in QFT was developed long ago,
see \Brefs{Veltman:1963th,Jacob:1961zz,Valent:1974bd,Lukierski:1978ke,Bollini:1993yp}.
For an implementation of the formalism in gauge theories we refer to the work of
\Brefs{Grassi:2001bz,Grassi:2000dz,Kniehl:1998vy,Kniehl:1998fn}, for complex
poles in general to \Brefs{Stuart:1991xk,Argyres:1995ym,Beenakker:1996kn}.

We can summarize by saying that unstable particles are described by irreducible, 
non-unitary representations of the Poincare group, corresponding to the complex 
eigenvalues of the four-momentum  $p^{\mu}$ satisfying the condition 
$p^2= - \mu^2 + i\,\mu\gamma$. 

A complete implementation of the Higgs-boson complex pole (within the SM) can be found in the
work of \Brefs{Actis:2008uh,Passarino:2010qk,Goria:2011wa}.

In this Section we summarize the status of theoretical uncertainties (THU) associated with the 
Higgs boson lineshape. The recent observation of a new massive neutral boson by ATLAS and CMS 
opens a new era where characterization of this new object is of central importance.
The search for the coupling structure of the light Higgs-like particle, as well as for new
heavy states, will continue. The huge uncertainty used so far for the heavy Higgs 
searches~\cite{Dittmaier:2011ti} ($1.5\,(M_{\PH}/\UTeV)^3$ uncertainty on the cross-section) 
was supposed to cover both the effect of the incorrect treatment of the lineshape and the missing 
interference. However, this uncertainty forced ATLAS and CMS to stop the search at $600$ \UGeV, 
where the uncertainty is $30\%$.

In the following we will review recent improvements on estimating the THU. We do not discuss 
uncertainties coming from QCD scale variations\footnote{For a recent discussion see 
https://indico.cern.ch/conferenceDisplay.py?confId=251810} and from 
PDF$\,+\alphas$~\cite{Dittmaier:2011ti}.
%--

Until recently, the Higgs boson invariant mass distribution (Higgs-boson-lineshape) has
received little attention.
In the work of \Brefs{Passarino:2010qk,Goria:2011wa} we have made an attempt to resolve 
the problem by comparing different theoretical inputs to the off-shellness of the Higgs boson. 
There is no question at all that the zero-width approximation should be avoided, especially in 
the high-mass region where the on-shell width becomes of the same order as the on-shell mass, 
or higher. We have shown evidence that only the Dyson-resummed propagator should be used, 
leading to the introduction of the $\PH$ complex pole, a gauge-invariant property of the 
$S\,$-matrix. It is convenient to describe the Complex-Pole scheme (CPS) as follows: the signal 
cross-section for the process $i j \to \PF$ can be written as
%--
\bq
\sigma_{i j \to \PH \to \PF}(s) = \frac{1}{\pi}\,
\sigma_{i j \to \PH}\,\frac{s^2}{\bmid s - s_{\PH}\bmid^2}\,
\frac{\Gamma^{\tot}_{\PH}(\sqrt{s})}{\sqrt{s}}\,\hbox{BR}\bigl( \PH \to \PF\bigr),
\quad
\Gamma^{\tot}_{\PH}(\sqrt{s}) = \sum_{\PF}\,\Gamma_{\PH \to \PF}(\sqrt{s}).
\label{QFT}
\eq
%--
where $s$ is the Higgs virtuality, $s_{\PH}$ is the Higgs complex pole and we have introduced 
a sum over all final states.

Note that the complex pole describing an unstable particle is conventionally parametrized as
$s_i = \mu^2_i - i\,\mu_i\,\gamma_i$.
It would we desirable to include two- and three-loop contributions in $\gh$ and for some
of these contributions only on-shell results have been computed so far. 
Therefore, it is very useful to give a rough estimate of the missing orders. Following the
authors of \Bref{Ghinculov:1996py} (as explained in Sect.~7 of \Bref{Goria:2011wa}) 
we can estimate that the leading uncertainty in $\gh$ is roughly given by
%--
\bq
\delta_{\PH} = 0.350119\,\frac{G_{\ssF}\,\muhs}{2 \sqrt{2} \pi^2}.
\eq
%--
Uncertainty estimates in $\gh$ range from $2.3\%$ at $400$ \UGeV to $9.4\%$ at $750$ \UGeV. 
In general, we do not see very large variations up to $1$ \UTeV with a breakdown of the 
perturbative expansion around $1.74$ \UTeV.
Therefore, using $\gh\,(1 \pm \delta_{\PH})$ we can give a rough but reasonable estimate of 
the remaining uncertainty on the lineshape.
To summarize our estimate of the theoretical uncertainty associated to the signal: 
the remaining uncertainty on the production cross-section is typically well reproduced 
by $(\delta_{\PH} +1)[\%]$, $\sigma_{\mathrm{max}}$ (the peak cross-section) changes approximately 
with the naive expectation, $2\,\delta_{\PH}[\%]$.

The factor $\Gamma^{\tot}_{\PH}(\sqrt{s})$ in \eqn{QFT} deserves a separate discussion.
It represents the ``on-shell'' decay of an Higgs boson of mass $\sqrt{s}$ and we have to quantify 
the corresponding uncertainty.
The staring point is $\Gamma^{\tot}$ computed by \textsc{Prophecy4f}~\cite{Prophecy4f}
which includes two-loop leading corrections in $G_{\ssF} M^2_{\PH}$, where $M_{\PH}$ is now 
the on-shell mass. Next we consider the on-shell width in the Higgs-Goldstone model,
discussed in~\cite{Ghinculov:1996py,Frink:1996sv}. We have
%--
\bq
\frac{\Gamma_{\PH}}{\sqrt{s}}\bmid_{\ssHG} = \sum_{n=1}^3\,a_n\,\lambda^n = X_{\ssHG},
\qquad \lambda = \frac{G_{\ssF} s}{2 \sqrt{2} \pi^2}.
\label{HGG}
\eq
%--
Let $\Gamma_{\mathrm p}= X_{\mathrm{p}}\,\sqrt{s}$ the width computed by \textsc{Prophecy4f}, 
we redefine the total width as
%--
\bq
\frac{\Gamma_{\tot}(\sqrt{s})}{\sqrt{s}} = \bigl( X_{\mathrm{p}} - X_{\ssHG} \bigr) + X_{\ssHG} =
\sum_{n=0}^3\,a_n\,\lambda^n,
\eq
%--
where now $a_0 = X_{\mathrm{p}} - X_{\ssHG}$. As long as $\lambda$ is not too large we can define
a $p\% < 80\%$ credible interval (see the work of \Bref{Cacciari:2011ze} for details) as 
(following from $a_{2,3} < a_1$)
%--
\bq
\Gamma_{\tot}(\sqrt{s}) = \Gamma_{\mathrm p}(\sqrt{s}) \pm \Delta\Gamma, \qquad
\Delta\Gamma = \frac{5}{4}\,\max\{\mid a_0\mid,a_1\}\,p\%\,\lambda^4\,\sqrt{s}.
\eq
%--
The CPS has been recently implemented within the \textsc{POWHEG-BOX} Monte Carlo 
generator~\cite{Alioli:2010xd}. 
%--
\subsubsection{Interference signal - background \label{HMbis}}
%--
In the current experimental analysis there are additional sources of uncertainty, e.g.\ background 
and Higgs interference 
effects~\cite{ATLAS:2012ac,pippo-atlas,Chatrchyan:2012ty,pluto-atlas,Chatrchyan:2012ft}. 
As a matter of fact, this interference is partly available and should not be included 
as a theoretical uncertainty; for a discussion and results we refer to 
\Brefs{Campbell:2011cu,Kauer:2012ma,Binoth:2006mf}.

Here we will examine the channel $\Pg \Pg \to \PZ \PZ$ and discuss the associated THU.
The background (continuum $\Pg \Pg \to \PZ \PZ$) and the interference are only known at leading 
order (LO, one-loop)~\cite{Glover:1988rg}. Here we face two problems, a missing NLO calculation of 
the background (two-loop) and the NLO and NNLO signal at the amplitude level, without which 
there is no way to improve upon the present LO calculation\footnote{There is, however, a recent 
and promising attempt to go beyond leading-order in \Bref{Bonvini:2013jha}.}.

A potential worry, already addressed in \Bref{Campbell:2011cu}, is: should we simply use the 
full LO calculation or should we try to effectively include the large (factor two) $K\,$-factor
to have effective NNLO observables? There are different opinions since interference effects 
may be as large or larger than NNLO corrections to the signal. Therefore, it is important to 
quantify both effects. Let us consider any distribution $D$, i.e.\
%--
\bq
D = \frac{d\sigma}{d x} \quad x = M_{\PZ\PZ} \quad \mbox{or} \quad x = p_{\perp}^{\PZ} \quad
\mbox{etc.}
\eq
%-- 
where $M_{\PZ\PZ}$ is the invariant mass of the $\PZ\PZ\,$-pair and $p_{\perp}^{\PZ}$ is the
transverse momentum. We introduce the following options, see \Bref{Passarino:2012ri}
($S,B$ and $I$ are shorthands for signal, background and interference):
%--
\begin{itemize}
\item {\bf{additive}} where one computes
%--
\bq
\diste{\NNLO} = \dist{\NNLO}(S) + \dist{\LO}(I) + \dist{\LO}(B)
\label{Aopt}
\eq
%--
\item {\bf{multiplicative}} where one computes
%--
\bq
\diste{\NNLO} = K_{\ssD}\,\bigl[ \dist{\LO}(S) + \dist{\LO}(I) \bigr] 
+ \dist{\LO}(B),
\qquad
K_{\ssD} = \frac{\dist{\NNLO}(S)}{\dist{\LO}(S)},
\label{Mopt}
\eq
%--
where $K_{\ssD}$ is the differential $K\,$-factor for the distribution. Note that
$K_{\ssD}$ accounts for both QCD and EW higher order effects in the production
and in the decay. 
%--
\item {\bf{intermediate}}
%--
It is convenient to define
\bq
K_{\ssD} = K_{\ssD}^{\Pg\Pg} + K_{\ssD}^{\mathrm{rest}},
\qquad
K_{\ssD}^{\Pg\Pg} = \frac{\dist{\NNLO}\bigl( \Pg\Pg \to \PH(\Pg) \to \PZ\PZ(\Pg)\bigr)}
{\dist{\LO}\bigl( \Pg\Pg \to \PH \to \PZ\PZ\bigr)}~,
\eq
%--
\bq
\diste{\NNLO} = K_{\ssD}\,\dist{\LO}(S) + 
\bigl( K_{\ssD}^{\Pg\Pg}\bigr)^{1/2}\,\dist{\LO}(I) + \dist{\LO}(B)~.
\label{Iopt}
\eq
%--
\end{itemize}
%--
Our recipe for estimating the theoretical uncertainty in the effective NNLO distribution is
as follows: the intermediate option gives the {\em central value}, while the band between the 
multiplicative and the additive options gives the uncertainty.
Note that the difference between the intermediate option and the median of the band is 
always small if not far away from the peak where, in any case, all options become questionable. 

For an inclusive quantity the effect of the interference, with or without the NNLO
$K\,$-factor for the signal, is almost negligible. For distributions this is radically different; 
referring to the $\PZ\PZ$ invariant mass distribution we can say that, close to 
$M_{\PZ\PZ} = \muh$, the uncertainty is small but becomes large in the rest of the search window 
$[\muh - \gh\,,\,\muh + \gh]$. 
The effect of the LO interference, w.r.t. LO $S + B$, reaches a maximum  before the peak 
(e.g.\ ${+}16\%$ at $\muh=700$ \UGeV) while our estimate of the scaled interference (always w.r.t. 
LO $S + B$) is $86^{+7}_{-3}\,\%$ in the same region, showing that NNLO signal effects 
are not negligible\footnote{Complete set of results, including results for the THU
discussed in \Sref{HMcpss}, and a code for computing the SM Higgs complex pole can be found 
at~\cite{CPHTO}.}.
%--

\noindent
{\it EW corrections to $\Pg \Pg \to \PH$  and $\PH \to \PV\PV$}\\[.2em]
%--
The NLO EW corrections to gluon fusion have been computed in 
Refs.~\cite{Actis:2008ts,Actis:2008ug}. The original results have been produced up to a Higgs 
invariant mass of $1$ \UTeV. If one is interested in the lineshape corresponding to a Higgs mass 
of $600$ \UGeV - $1$ \UTeV\  there will be some non-negligible fraction of events with invariant 
mass up to $2$ \UTeV. In this case extrapolation will give wrong results; for this reason we 
have provided additional values for the NLO EW correction factor to the inclusice cross-section: 
$\delta_{\EW} = + 19.37\%(+ 34.53\%,\, + 53.90\%)$ 
for $\muh = 1.5$ \UTeV($2$ \UTeV,\,$2.5$ \UTeV)\footnote{A complete grid up to $2.5$ \UTeV\
(see \Brefs{Passarino:2007fp,Actis:2008ug,Actis:2008ts}) and a program for a cubic interpolating 
spline incorporating the grid can be found at~\cite{EWgrid}.}.
Also $\Gamma^{\tot}_{\PH}$ of \eqn{QFT} needs some attention. The best results
available are from \Bref{Dittmaier:2011ti} where, however, tables stop at
$1$ \UTeV. If one wants to go above this value extrapolation should be avoided and it is better 
to include few additional points in the grid, e.g.\ we have included 
$\Gamma^{\tot}_{\PH} = 3.38(15.8)$ \UTeV\  for $\muh= 1.5(2)$ \UTeV. Note that, at $2$ \UTeV, one 
has $\Gamma( \PH \to \PZ\PZ) = 5.25$ \UTeV\  and $\Gamma( \PH \to \PW\PW) = 10.52$ \UTeV.
Finally, mention should be made of the very recent estimate of the N${}^3$LO QCD
corrections, see \Bref{Ball:2013bra}.
%--
\clearpage
%--
%\bibliographystyle{atlasnote}
%\bibliography{HM_Giampiero}{}

%===
%\end{document}



