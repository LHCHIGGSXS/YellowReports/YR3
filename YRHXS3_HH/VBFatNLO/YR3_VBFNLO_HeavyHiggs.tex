
%\usepackage{amsmath,amssymb,graphicx,longtable}

\providecommand{\HDECAY}{{\sc HDECAY}}
\providecommand{\HIGLU}{{\sc HIGLU}}
\providecommand{\Prophecy}{{\sc Prophecy4f}}
\providecommand{\CPsuperH}{{\sc CPsuperH}}
\providecommand{\FeynHiggs}{{\sc FeynHiggs}}

\providecommand{\lsim}
{\;\raisebox{-.3em}{$\stackrel{\displaystyle <}{\sim}$}\;}
\providecommand{\gsim}
{\;\raisebox{-.3em}{$\stackrel{\displaystyle >}{\sim}$}\;}
\providecommand{\orderx}[1]{\ensuremath{{\cal O}(#1)}}

\providecommand{\eqn}[1]{Eq.\,(\ref{#1})}
\providecommand{\eqns}[2]{Eqs.\,(\ref{#1}) -- (\ref{#2})}
\providecommand{\refF}[1]{Figure~\ref{#1}}
\providecommand{\refFs}[2]{Figures~\ref{#1} -- \ref{#2}}
\providecommand{\refT}[1]{Table~\ref{#1}}
\providecommand{\refTs}[2]{Tables~\ref{#1} -- \ref{#2}}
\providecommand{\refS}[1]{Section~\ref{#1}}
\providecommand{\refSs}[2]{Sections~\ref{#1} -- \ref{#2}}
\providecommand{\refC}[1]{Chapter~\ref{#1}}
\providecommand{\refCs}[2]{Chapters~\ref{#1} -- \ref{#2}}
\providecommand{\refA}[1]{Appendix~\ref{#1}}
\providecommand{\refAs}[2]{Appendices~\ref{#1} -- \ref{#2}}

\providecommand{\zehomi}[1]{$\cdot 10^{-#1}$}
\providecommand{\zehoze}{}
\providecommand{\zehopl}[1]{$\cdot 10^{#1}$}
%\providecommand{\zehomi}[1]{E-0#1}
%\providecommand{\zehoze}{E+00}
%\providecommand{\zehopl}[1]{E+0#1}


\subsubsection{Signal definition}

Also in the VBF Higgs production mode, interference effects between the
Higgs signal process and the continuum background become increasingly
important when going to higher Higgs masses. NLO QCD corrections to the
full process $\Pp\Pp \rightarrow \PVB\PVB jj$ have been calculated in
\Bref{Jager:2006zc,Jager:2006cp} and are available in {\sc
VBFNLO}~\cite{Arnold:2012xn,Arnold:2011wj,Arnold:2008rz} including
decays of the vector bosons. We will
concentrate on the $\Pp\Pp \rightarrow \PWp \PWm jj$ process, which
can be calculated with both fully leptonic ($\Pl^{+} \PGnl \Pl^{-}
\PAGn_{\Pl}$) as well as semileptonic ($\Pl^{+} \PGnl jj$, $\Pl^{-}
\PAGn_{\Pl} jj$) decays of the $\PW$ bosons.

Both Higgs and continuum diagrams are necessary to ensure the correct
high-energy behavior. For large invariant masses of the vector-boson
pair $\sqrt{s}$, the continuum diagrams ${\mathcal M}_{\mathrm B}$ are proportional to
$-\frac{s}{v^2}$, where $v$ denotes the Higgs vacuum expectation value,
and would lead to unitarity violation at about 1 TeV. They are canceled
by corresponding diagrams with s-, t- and u-channel exchange of a Higgs
boson ${\mathcal M}_H \propto \frac{s}{v^2}$ in the leading term.
This raises the question of how to best define the signal, or
correspondingly the background contribution which is subsequently
subtracted from the full process. Naive definitions like only taking the
signal plus interference contributions into account or, equivalently,
subtracting the contribution coming from background diagrams only will
violate unitarity at large center-of-mass energies of the diboson
system.

\begin{figure}
%\includegraphics[width=0.48\textwidth,angle=270]{YRHXS3_HH/VBFatNLO/mHvar.eps}
\centering
\includegraphics[width=0.48\textwidth]{YRHXS3_HH/VBFatNLO/mHvar.eps}
\caption{Invariant $\PW\PW$ mass distribution for the process
$\Pp\Pp\rightarrow \PW\PW jj$ for different Higgs boson masses, intended
either as signal ($600$ and $800\UGeV$) or as background definition with a
light Higgs boson ($100$ and $126\UGeV$).}
\label{fig:vbfnlorew1}
\end{figure}
The invariant mass distribution of the $\PW\PW$ diboson system is shown
in \Fref{fig:vbfnlorew1} for various choices of the Higgs boson
mass. The distributions have been generated with \textsc{VBFNLO} using
standard vector-boson-fusion cuts on the two tagging jets ($m_{jj}>600$
GeV, $|\eta_{j1}-\eta_{j2}|>4$, $\eta_{j1} \times \eta_{j2} < 0$) in
addition to general detector acceptance cuts ($p_{T,j} > 20\UGeV$,
$\eta_j<4.5$, $p_{T,\ell} > 10\UGeV$, $\eta_\ell<2.5$).
We show two examples for heavy Higgs bosons of $600$ and $800\UGeV$ mass, as
well as for two light Higgs masses of $126$ and $100\UGeV$. For the latter,
the Higgs peak is outside of the shown range. We see that the two
light-mass curves agree very well, with some small remaining mass
dependence in the region around the $\PW\PW$ production threshold.
Therefore, we can use these to define the background
\begin{equation}
\sigma_{\mathrm B} = \int d\Phi |{\mathcal M}_{\mathrm B} + {\mathcal M}_{\Ph}(m_{\Ph})|^2 \ ,
\end{equation}
where $m_{\Ph}$ denotes the mass of the light Higgs boson used for
subtraction and $d\Phi$ denotes the phase-space integration. This then
leads to our definition of the signal+interference contribution
\begin{equation}
\sigma_{S+I} = 
\int d\Phi |{\mathcal M}_{\mathrm B} + {\mathcal M}_H(\MH)|^2 - \sigma_{\mathrm B} 
\label{eq:sireweight}
\end{equation}
with the heavy Higgs mass $\MH$.
This approach respects the correct high-energy behavior.  


\subsubsection{Event reweighting}

A full simulation of LHC processes, involving NLO events plus parton
shower and detector simulation with cuts on the final-state particles,
is a time-consuming task. If events in a related scenario with similar
kinematic features have already been fully simulated, one can re-use
them and apply a reweighting procedure to the events at parton-level. In
\textsc{VBFNLO} this is included as an add-on named \textsc{REPOLO} --
REweighting POwheg events at Leading Order -- and is available on
request by the \textsc{VBFNLO} authors.

In this subsection we will discuss the reweighting of VBF production of a
heavy Higgs boson. The starting point are unweighted events of the
signal process only for a Higgs boson of 800 GeV, generated with
POWHEG~\cite{Nason:2009ai}. 
Using the \textsc{VBFNLO} framework, we then create a new event file
where each signal event is reweighted by a factor
\begin{equation} 
\frac{|{\mathcal M}_{\mathrm B} + {\mathcal M}_H(\MH)|^2 - |{\mathcal M}_{\mathrm B} + {\mathcal
M}_{\Ph}(m_{\Ph})|^2}{|{\mathcal M}_H(\MH)|^2}
\label{eq:repoloreweight}
\end{equation}
according to Eq.~\ref{eq:sireweight}. Events are reweighted for heavy
Higgs bosons without decay. For the resulting signal+interference
contribution, this corresponds to dividing over the Higgs branching into
$\PW\PW$. Internally, a decay into $\PW^+\PW^- \rightarrow \Pl^{+}
\PGnl \Pl^{-} \PAGn_{\Pl}$ is simulated for all matrix elements which
appear in the reweighting.

The result of this procedure is shown in \Fref{fig:vbfnlorew2} where
900000 POWHEG events have been reweighted. For the background
subtraction a Higgs boson with mass 100 GeV has been used.
\begin{figure}
\centering
%\includegraphics[width=0.48\textwidth,angle=270]{YRHXS3_HH/VBFatNLO/mHrew.eps}
\includegraphics[width=0.48\textwidth]{YRHXS3_HH/VBFatNLO/mHrew.eps}
\caption{Invariant $\PW\PW$ mass distribution for the process
$\Pp\Pp\rightarrow \PW\PW jj$. The distribution is shown for the signal-only
process generated with POWHEG, as well as reweighted by REPOLO to
include signal-background interference according to
Eq.~\ref{eq:repoloreweight}. The branching ratio $\PH\rightarrow \PW\PW$ has
been divided out for both curves.}
\label{fig:vbfnlorew2}
\end{figure}
One observes the significant effect of the interference term, which
enhances the rate for smaller invariant masses, while at the same time
decreasing the rate for larger ones. This corresponds to the
constructive and destructive interference, respectively, as expected
from theory. 
The reweighting procedure has its limits in phase-space regions, where
the reweighted event weight is significantly larger than the original
one. In the shown example this happens for invariant masses below about
400 GeV. The differential signal-only cross section gets very small,
while the interference term still yields a relevant contribution.
In practice, a possible remedy would be to generate additional events in
the region where the process to be reweighted is known to be small. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\end{document}
