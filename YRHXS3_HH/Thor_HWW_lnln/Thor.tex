For the $\PW\PW \rightarrow \Pl\PGnl \Pl\PGnl$ channel (with $l=e,\,\mu$), the effect of the interference between 
$\Pg\Pg \rightarrow \PH \rightarrow \PW\PW$ and continuum $\Pg\Pg \rightarrow \PW\PW$ production becomes increasingly important 
as the mass of the resonance increases. 

\Fref{fig:HWW_SMWW_int} shows the LO Feynman diagrams for the two processes.

\begin{figure}[tbp]
  \centering
  \includegraphics[width=0.70\textwidth]{YRHXS3_HH/Lashkar_interference_ATLAS_WWlvlv/figures/HWW_SMWW_interference.eps}
  %\end{center}
  \vspace*{-2mm}
  \caption{Diagrams for the $\Pg\Pg \rightarrow \PH \rightarrow \PW\PW \rightarrow \Pl\PGnl \Pl\PGnl$ process (left) and the continuum $\Pg\Pg \rightarrow \PW\PW \rightarrow \Pl\PGnl \Pl\PGnl$ process (right).}
  \label{fig:HWW_SMWW_int}
\end{figure}


In ATLAS, a study of the effect of the interference on key kinematic variables has been performed using the
\textsc{MCFM} \cite{MCFMweb} Monte Carlo program. The study uses \textsc{MCFM 6.2}, the processes being generated
at a centre-of-mass (CM) energy of $8\UTeV$, with no cuts on the generation. Events 
MCFM processes 121 and 122 are respectively used to generate 
distributions for the pure $\PH \rightarrow \PW\PW \rightarrow \Pl\PGnl \Pl\PGnl$ process and the process including the
effect of the interference. Both processes are calculated to leading order accuracy in QCD. This requires a reweighting procedure to account for higher-order corrections. This procedure has been described in section \ref{HMcpss}.
Distributions are generated for six different resonance masses $\MH: 400, 500, 600, 700, 800$ and $900\UGeV$. The renormalization and factorization scales are set to the mass of the resonance being generated.

The object selection criteria used in this study are similar to those used in the ATLAS $\PH \rightarrow \PW\PW \rightarrow \Pl\PGnl \Pl\PGnl$
analysis \cite{Aad:2012uub}. The leptons are required to be within $|\eta| < 2.5$. The leading lepton must have $\pT > 25\UGeV$,
and the sub-leading lepton $\pT > 15\UGeV$. The missing transverse momentum is required to be larger than $45\UGeV$. No requirement is made on the number of jets in the final state nor on the types of the two leptons.

The interference effect affects the shape of the distributions used in the analysis.
The effects of such reweighting are studied for four crucial kinematic variables:

\begin{itemize}
  \item the dilepton invariant mass $M_{\ell\ell}$
  \item the transverse momentum of the dilepton system $\pT^{\ell\ell}$
  \item the dilepton azimuthal opening angle $\Delta\phi_{\ell\ell}$
  \item the transverse mass $M_\mathrm{T}$
  
\end{itemize}  


The first three variables are important because the analysis uses them to reject background and define a signal region.
The fourth variable, $M_\mathrm{T}$, is the final discriminant~\cite{Aad:2012uub}.\\

Figures \ref{fig:mll_reweight_cut_1} - \ref{fig:Mt_reweight_cut_1} show distributions of the variables for pure $\PH \rightarrow \PW\PW$, for
the process including the interference and for the pure signal process reweighted according to the lineshape distribution (i.e. the invariant mass of the two Ws). For each variable, distributions are shown for three of the six resonance masses corresponding to 400, 600 and 900 GeV. The ratio between the signal distributions including the interference and the reweighted distributions is also plotted. The cross-section $\sigma_{\PH_i}$ including the interference effect is not strictly physical, and can be negative (see Eq. 4.1 in \cite{Campbell:2011cu}), as seen in many of the distributions. From these plots it is clear that the interference has a large impact on both the cross-section and the shape of the distributions, particularly for higher Higgs masses. The plots also show that reweighting by $M_{\PW\PW}$ correctly reproduces the effect of interference on the shapes of other distributions.


\begin{figure}[htb!]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_Mll_reweighting_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_Mll_reweighting_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_Mll_reweighting_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the dilepton invariant mass $M_{\ell\ell}$ showing the effect of the interference and the result of the reweighting procedure after cuts, corresponding to a Higgs mass of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:mll_reweight_cut_1}
\end{figure} 	


\begin{figure}[htb!]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_ptll_reweighting_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_ptll_reweighting_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_ptll_reweighting_cut_YR3.eps}

  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the transverse momentum of the dilepton system $\pT^{\ell\ell}$ showing the effect of the interference and the result of the reweighting procedure after cuts, corresponding to a Higgs mass of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:ptll_reweight_cut_1}
\end{figure} 	

\begin{figure}[htb!]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_DeltaPhi_ll_reweighting_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_DeltaPhi_ll_reweighting_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_DeltaPhi_ll_reweighting_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the dilepton azimuthal opening angle $\Delta\phi_{\ell\ell}$ showing the effect of the interference and the result of the reweighting procedure after cuts, corresponding to a Higgs mass of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:DeltaPhi_ll_reweight_cut_1}
\end{figure} 	


\begin{figure}[htb!]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_Mt_reweighting_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_Mt_reweighting_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_Mt_reweighting_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the transverse mass $M_\mathrm{T}$ showing the effect of the interference and the result of the reweighting procedure after cuts, corresponding to a Higgs mass of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:Mt_reweight_cut_1}
\end{figure} 	



The same distributions have been obtained before applying any cuts and it has been proven that the shape of the distributions is not affected by the cuts. The difference between the efficiency calculated on samples which include the interference at generation level and on signal reweighted samples, has been used to evaluate the systematic uncertainty associated to the reweighting procedure as reported in table \ref{tab:eff}.

\begin{table}
\caption{Efficiency of the cuts (fraction of cross section remaining) for the samples generated with the interference effect included and the reweighted samples.}
\label{tab:eff}
\centering
\begin{tabular}{lcccccc}
% top line of headings - label masses
\hline
\multicolumn{1}{l}{} & \multicolumn{2}{c}{$\MH$ = 400 GeV} & \multicolumn{2}{c}{$\MH$ = 600 GeV} & \multicolumn{2}{c}{$\MH$ = 900 GeV} \\
% 2nd line of headings - S+I and Reweighted labels
\multicolumn{1}{l}{} & \multicolumn{1}{c}{S+I} & \multicolumn{1}{c}{Reweighted} & \multicolumn{1}{c}{S+I} & \multicolumn{1}{c}{Reweighted} & \multicolumn{1}{c}{S+I} & \multicolumn{1}{c}{Reweighted} \\ \hline 
%first chunk of table contents
\multicolumn{1}{l}{After Preselection Cuts} & \multicolumn{1}{l}{0.844945} & \multicolumn{1}{l}{0.846989} & \multicolumn{1}{l}{0.876930} & \multicolumn{1}{l}{0.879919} & \multicolumn{1}{l}{0.876166} & \multicolumn{1}{l}{0.881239} \\ 
\multicolumn{1}{l}{After Kinematic Cuts} & \multicolumn{1}{l}{0.624508} & \multicolumn{1}{l}{0.619817} & \multicolumn{1}{l}{0.640973} & \multicolumn{1}{l}{0.630267} & \multicolumn{1}{l}{0.661190} & \multicolumn{1}{l}{0.634992}\\ \hline
\end{tabular}

%%\end{center}
\end{table}

From this it is clear that the overall effect of the discrepancies between the reweighting and the process including the
effect of the interference distributions is a few \%. The exact magnitude of the uncertainty depends upon which cuts are applied, and which mass point is being examined - here, it ranges from $1\%$ for the $\MH = 400\UGeV$ case to 5\% for the $\MH = 900\UGeV$ case. 

Having looked at the effect of reweighting at LO, there remains the issue of reweight for higher order effects. This study reweights to NNLO, using the 
schemes suggested in \cite{Passarino:2012ri}, and the correspondent K factors \cite{CPHTO} as discussed in section \ref{HMbis}: 

\begin{itemize}
\item Additive: K\,S + I
\item Multiplicative: K\,S + K\,I
\item Intermediate: K\,S + sqrt($K_{\Pg\Pg}$)\,I
\end{itemize}

Figures \ref{fig:mll_NNLO_cut_1} - \ref{fig:Mt_NNLO_cut_1} show these three schemes -- together with the LO Signal for comparison -- for each of the 4 distributions shown earlier, after the aforementioned cuts have been applied. The Additive (in yellow) and the Multiplicative (in magenta) schemes form bounds for the theory uncertainty in the scaling to NNLO, while the Intermediate scheme (in black) is the nominal NNLO Signal plus Interference distribution. 


\begin{figure}[tbp]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_Mll_NNLO_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_Mll_NNLO_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_Mll_NNLO_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the dilepton invariant mass $m_{\ell\ell}$ showing the three schemes for scaling to NNLO, with the LO signal distribution for comparison, at Higgs masses of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:mll_NNLO_cut_1}
\end{figure} 	


\begin{figure}[tbp]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_ptll_NNLO_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_ptll_NNLO_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_ptll_NNLO_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the transverse momentum of the dilepton system $\pT^{\ell\ell}$ showing the three schemes for scaling to NNLO, with the LO signal distribution for comparison, at Higgs masses of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:ptll_NNLO_cut_1}
\end{figure} 	


\begin{figure}[tbp]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_DeltaPhi_ll_NNLO_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_DeltaPhi_ll_NNLO_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_DeltaPhi_ll_NNLO_cut_YR3.eps}
 
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the dilepton azimuthal opening angle $\Delta\phi_{\ell\ell}$ showing the three schemes for scaling to NNLO, with the LO signal distribution for comparison, at Higgs masses of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:DeltaPhi_ll_NNLO_cut_1}
\end{figure} 	



\begin{figure}[tbp]
  \centering
  
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_400/HWW_400_Mt_NNLO_cut_YR3.eps}
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_600/HWW_600_Mt_NNLO_cut_YR3.eps} 
  \includegraphics[width=0.32\textwidth]{YRHXS3_HH/Thor_HWW_lnln/Plots_YR3_900/HWW_900_Mt_NNLO_cut_YR3.eps}
 
  
  %\end{center}
  \vspace*{-2mm}
  \caption{Distributions of the transverse mass $M_\mathrm{T}$ showing the three schemes for scaling to NNLO, with the LO signal distribution for comparison, at Higgs masses of $\MH = 400\UGeV$ (left), $600\UGeV$ (middle), $900\UGeV$ (right).}
  \label{fig:Mt_NNLO_cut_1}
\end{figure} 	



From these plots, it is evident that the difference between the Intermediate distributions and the Additive or Multiplicative ranges from a negligible amount at $\MH = 400\UGeV$, to about $\pm$ $25\%$ at $\MH = 900\UGeV$. This is a very conservative estimate of the uncertainty in scaling to NNLO, and gives a very large uncertainty for high $\MH$. The different schemes for scaling to NNLO do not seem to affect the shape of the distributions significantly. \\

In conclusion, event-by-event reweighting by the $M_{\PW\PW}$ distribution reproduces the effect of interference fairly closely, though it does introduce some uncertainty, of order $1\%$ for $\MH = 400 \UGeV$ and $5\%$ for $\MH = 900\UGeV$. A much larger uncertainty is introduced by the process of reweighting to NNLO, rising to $\pm$ $25\%$ for $\MH = 900\UGeV$, the highest mass point studied.

