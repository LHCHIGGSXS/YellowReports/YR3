Fraction of cross-section remaining after cuts: 
                        S+I             Reweighted
Preselection cuts       0.876166        0.881239
Kinematic cuts          0.661190 	0.634992
                        Additive        Multiplicative  Intermediate
Preselection cuts       0.895012        0.89085 	0.895178
Kinematic cuts          0.638197        0.63485 	0.636239

