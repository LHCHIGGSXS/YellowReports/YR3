%\section{Section name ()
%  \footnote{%
%    F.~Convenor, S.~Convenor, \ldots (eds.);
%    Stefano Frixione, Antoine Lauyers, Fabio Maltoni } }
%\label{sec:SectionName}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

At LO the cross section for $\Pg\Pg \to \PVB\PVB$ can be written as

\begin{equation}
\sigma^{\LO}_{\mathrm{S+i+B}} = |{\mathcal M}_{\mathrm S}|^2+2\, {\rm Re}( {\mathcal M}_{\mathrm S} {\mathcal M}_{\mathrm B}^*) + |{\mathcal M}_{\mathrm B}|^2\,,
\end{equation}
where the signal $S$ features a triangle loop and the Higgs propagation in the $s$-channel while the background $B$ proceeds via $\Pg\Pg\to \PVB\PVB$ boxes.
At high partonic energy $\hat s$, there is a cancellation between the $\Pp\Pp \to \PH \to \PVB\PVB$ diagram and the $S$-wave massive contribution of the 
non-resonant ones~\cite{Glover:1988rg}, each growing as $m_{\PQq}^2/m_{\PVB}^2 \log^2 \hat s/m_{\PQq}^2$ where $\PQq$ is the heavy quark running in the loop.
This contribution is proportional to the axial part of the vector boson coupling to the quarks and it is basically due to the non-conservation of the
axial current for massive quarks.  Such a cancellation ensures the unitarity of the standard model up to arbitrary large scales. 
Note that  the width scheme has also an impact on the unitarity of the theory. 

In absence of a complete NLO computation for the full process $\Pp\Pp \to \PVB\PVB$ a scheme for combining the signal $\Pp\Pp \to \PH \to \PVB\PVB$ at NLO (or NNLO), the background at LO and the interference between them  is needed. Different methods have been proposed and discussed, see, .e.g., ~\cite{Passarino:2012ri},  which make different approximations and can broadly divided in multiplicative or additive. In essence the main difference stems from how the signal-background interference is treated: in the additive scheme it is taken at the LO, i.e., it corresponds exactly to the accuracy to which is presently known, while in the multiplicative scheme a guess for the NLO (or NNLO) interference is made, which is obtained by multiplying the LO results by a $K$-factor, overall or differential based on a pivot distribution. The former scheme is the only possible one for a MC generator, yet it violates unitarity at NLO  in $\alphas$, while the latter is better suited for analytical calculations, does not violate unitarity, yet it is just a guess for an unknown quantity. 

In our approach, we therefore employ an additive scheme,

\begin{equation}
\sigma^{\mathrm{MC}}(pp \to  \PVB\PVB) = \sigma^{\NLO}_{\mathrm S}+ \sigma^{\LO}_{\mathrm{i+B}} = \sigma^{\NLO}_{\mathrm S} +2\, {\rm Re}( {\mathcal M}_{\mathrm S} {\mathcal M}_{\mathrm B}^*) + |{\mathcal M}_{\mathrm B}|^2 \,.
\label{eq: additive-scheme}
\end{equation}

Events corresponding to the first term are generated with \textsc{MC@NLO} v.4.09~\cite{Frixione:2002ik}, which features real (one-loop) and virtual (two-loop) matrix elements with  the exact $m_{\PQb}$ and $m_{\PQt}$ dependence (taken from \Bref{Aglietti:2006tp,Bonciani:2007ex}).
Such calculation is for on-shell Higgs production. Introducing the Higgs propagation and decay, and therefore the Higgs width, needs special care especially for a heavy Higgs in order to maintain gauge invariance and unitarity, as discussed at length in the literature and this report. In \textsc{MC@NLO} given a final state in terms of leptons,  this is achieved by the replacement:
\begin{equation}
\delta(\hat s - \MH^2) \rightarrow \frac{1}{\pi} \frac{\sqrt{\hat s} \cdot \Gamma(H(\hat s) \to {\rm final\, state})}{(\hat s - \MH^2)^2 + \MH^2 \Gamma_{\PH}^2}\,,
\end{equation}
where the partial width into the final state is calculated at corresponding virtuality and $\Gamma_{\PH}$ is the total Higgs width. 
Such a replacement exactly corresponds to a complex-pole scheme for the Higgs if $\Gamma_{\PH}$ is calculated accordingly~\cite{Goria:2011wa}.

The second and third terms in Eq.~(\ref{eq: additive-scheme}) are generated via dedicated implementations, whose loop matrix elements are automatically  obtained via \textsc{MadLoop}~\cite{Hirschi:2011pa} and checked, when possible, with MCFM~\cite{Campbell:2011cu}. The corresponding codes are publicly available at the \textsc{aMC@NLO} web page ({\tt http://amcatnlo.cern.ch}). In our calculation all six flavor run in the background boxes for $\Pg\Pg\to \PVB\PVB$ and quark mass effects are accounted for exactly.

Samples corresponding to different leptonic final states, $\ell^+ \PGn_\ell \ell'^- \bar \PGn_{\ell'} (\PWp \PWm)$, $\ell^+ \ell^-  \PGn_{\ell'}  \bar \PGn_{\ell'}  (\PZ\PZ)$ and $\ell^+ \ell^-  \ell'^+ \ell'^-  (\PZ/\PGg^* Z/\PGg^*)$ can be obtained. In \Fref{fig:ww.zz.aa} the invariant mass of the four leptons in  the   $\Pep \Pe  \PGn_{\PGm}  \bar \PGn_{\PGm}$ and  $\Pep \Pe  \PGmp \PGm $ final states for two different Higgs masses are shown. Only minimal acceptance cuts have been applied. The blue (low lying) curve show the full LO result. the black (dotted) curve the NLO signal plus the background, while the red curve is the combined result corresponding to Eq.~(\ref{eq: additive-scheme}). The first important result is that NLO
effects for the signal are very important around the Higgs mass, as expected, and cannot be neglected. The interference gives an enhancement
of the cross section before the peak and a depletion (corresponding to the cancellation of the bad high-energy behavior) at large invariant masses. Such effects, while certainly visible already for a Higgs of $\MH=500{-}600\UGeV$, give a small contributions overall as the high-energy tail  is strongly suppressed by the gluon PDF's.  



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{YRHXS3_HH/MCatNLO/zz.eps}
\includegraphics[width=0.7\textwidth]{YRHXS3_HH/MCatNLO/zz2.eps}
\caption{Invariant mass distributions for $\Pp\Pp\to 4 \ell$ in different channels as obtained from \textsc{MC@NLO} for the signal $S$ and \textsc{MadLoop} for the background and interference $B+i$. Basic acceptance cuts applied. }
\label{fig:ww.zz.aa}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

