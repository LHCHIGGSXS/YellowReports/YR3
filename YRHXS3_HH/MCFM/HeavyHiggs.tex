
\allowdisplaybreaks


%\section{Overview}

This section provides an overview of the implementation of Higgs boson production and background processes in \textsc{MCFM}. Further details can be found in \Brefs{Campbell:2011cu , Campbell:2011bn}. \textsc{MCFM} contains NLO predictions for a variety of processes. For studies of heavy Higgs bosons the most relevant signal processes are Higgs production through gluon fusion and vector-boson-fusion, with subsequent decays to massive vector bosons. The SM background production of the direct diboson final state themselves. In addition to the standard NLO predictions for the diboson processes, \textsc{MCFM} includes the contributions from gluon induced initial states. Although formally $\mathcal{O}(\alphas^2)$ (and hence NNLO) the large gluon flux at LHC operating energies enhances these pieces beyond the naive NNLO power counting. The current status (as of \textsc{MCFM} v6.4) of these processes is as follows. For the $\Pg\Pg\rightarrow \PW\PW$ process all three families of quarks are included in the loops, with the exact dependence on the top mass $m_{\PQt}$ kept and all other quarks considered massless. For the $\PZ\PZ$ process only the first five massless flavors are implemented. 

One of the most interesting phenomenological features of a heavy Higgs boson is its large width and the corresponding impact of the interference term with the SM production of $\PVB\PVB$ pairs. Since the interference pattern is often not accounted for in event generator simulations and higher order corrections, we focus on its impact on phenomenology in this report. 

%\section{Interference effects}

In order to study the interference between the SM production of $\PW$ pairs and Higgs decay to $\PW\PW$ we introduce the following definitions, 
\begin{eqnarray}
{\sigma_{\mathrm B}} &\longrightarrow& \left|{\mathcal A}_{\mathrm{box}}\right|^2 \;, \qquad {\mathcal A}_{\mathrm{box}} = 2{\mathcal A}_{\mathrm{massless}}+{\mathcal A}_{\mathrm{massive}} \;, \nonumber \\
{\sigma_{\PH}} &\longrightarrow& \left|{\mathcal A}_{\mathrm{Higgs}}\right|^2 \nonumber \;, \\
{\sigma_i} &\longrightarrow& 2{\mathrm{Re}} \left({\mathcal A}_{{\mathrm{Higgs}}} {\mathcal A}_{\mathrm{box}}^\star \right) \;, \nonumber \\
{\sigma_{\PH,i}} &=& \sigma_{\PH} + \sigma_i \;.
\label{eq:Hidescription}
\end{eqnarray}
Here $\sigma_{\mathrm B}$ represents the background production of $\PW\PW$
pairs, (proceeding through the ${\mathcal A}_{\mathrm{box}}$ amplitude which is
made up of massless and massive fermion loops) and $\sigma_{\PH}$ represents signal squared cross section associated with the LO production of a Higgs boson through gluon fusion, with a subsequent decay to $\PW\PW$. Note that this LO processes proceeds through a top quark loop. Finally $\sigma_i$ represents the interference between the signal and background amplitudes.  

The interference pattern contains two terms, which are extracted by removing the Higgs propagator from the amplitude (with the new stripped amplitude referred to as $\tilde{A}_{\mathrm{Higgs}}$).  The imaginary part of the Higgs propagator couples to the imaginary part of the product, $\tilde{A}_{\mathrm{Higgs}}{\mathcal A}_{\mathrm{box}}^* $. Hence this piece is proportional to the Higgs width. The real part of the Higgs propagator couples to the real part of  $\tilde{A}_{\mathrm{Higgs}}{\mathcal A}_{\mathrm{box}}^* $. The resulting interference cross section is thus of the form, 
\begin{equation}
\delta\sigma_i = \frac{(\hat s - \MH^2)}{ (\hat s - \MH^2)^2 + \MH^2 \Gamma_{\PH}^2} \, \Re \left( 2 \widetilde{\mathcal A}_{\mathrm{Higgs}} {\mathcal A}_{\mathrm{box}}^*  \right)
         + \frac{\MH \Gamma_{\PH}}{ (\hat s - \MH^2)^2 + \MH^2 \Gamma_{\PH}^2} \, \Im  \left( 2 \widetilde{\mathcal A}_{\mathrm{Higgs}} {\mathcal A}_{\mathrm{box}}^*  \right) \;,     
\label{eq:intfdecomp}
\end{equation}
The first piece of this expression is an odd function in $\hat s$ about $\MH$ and therefore, for well resolved final states (such as $\PZ\PZ^*$ and $\PGg\PGg$) in which the Higgs signal can be localized in $\hat s$, the interference effects from this piece approximately cancel over the integration of the $\hat s $ bin. In these cases the interference phenomenology is dominated by the width effects (which for a heavy Higgs will be sizeable). For Higgs decays which are not fully reconstructed (such as $\PW\PW$) both terms contribute. Since the Higgs boson is a unitarizing particle the interference is destructive at large $\hat s$. Thus, since the function is odd the interference in the corresponding region $\hat s <  \MH$ is constructive. Since the background cross section is typically larger in this region the net effect of the interference is constructive for most Higgs masses. 

\begin{figure}[htb] 
\centering
\includegraphics[width=8cm]{YRHXS3_HH/MCFM/heavy_higgs_incxs.eps}
\caption{Inclusive $\PH\rightarrow \PW\PW\rightarrow \ell\PGn\ell'\PGn'$ cross section at the LHC8. The standard signal squared $\sigma_{\PH}$ is shown in blue, whilst the signal squared plus interference cross sections are shown in red. The lower panel shows the ratio of these two cross sections.} 
\label{fig:higgs_xs}
\end{figure}
\Fref{fig:higgs_xs} presents the net contribution of the interference for the inclusive $\PH\rightarrow \PW\PW\rightarrow \ell\PGn\ell'\PGn'$ cross section at LHC operating energies ($8\UTeV$), focusing on the heavy Higgs region. The renormalization and factorization scales have been set equal to the Higgs mass. It is clear that the interference becomes the dominant part of the Higgs production cross section as the Higgs mass grows. This is primarily since the Higgs cross section is a rapidly falling function in $\MH$, hence the term linear in the signal amplitude (the interference) dominates over the quadratic piece (the signal squared). 
\begin{figure}[htb]
\centering
\includegraphics[width=8cm]{YRHXS3_HH/MCFM/MT800.eps}
\caption{The transverse mass distribution for a Higgs boson with mass $800\UGeV$ at the $8\UTeV$ LHC. The upper panel shows the prediction for the differential distribution with and without including the interference terms, the lower panel provides the ratio of these predictions.} 
\label{fig:mt}
\end{figure}

It is also interesting to consider the impact of the interference pieces on
more differential quantities. One such relevant quantity is the transverse
mass of the missing transverse energy plus leptons final
state. The \textsc{MCFM} prediction for the transverse mass spectrum for a
Higgs mass of $800\UGeV$ is shown in \Fref{fig:mt}. The pattern predicted by 
\eqn{eq:intfdecomp} is manifest. The interference is large and constructive in the region ($\hat s  < \MH \implies m_{\mathrm T} < \MH)$, whilst in the large $\hat s$ region the interference is destructive. The enhancement in the low $m_{\mathrm T}$ region is significant and can be around an order of magnitude compared to the naive signal prediction. Therefore inclusion of the interference effects are vital in order to simulate the phenomenological impact of the heavy Higgs.  

Since it is clear that the contributions from the interference effects are important, it is interesting to look at possible mechanisms for modifying the Higgs propagator in order to simulate the impact of the interference. If such a modification can mimic the interference terms then use of this propagator in higher order codes (for example the NNLO code of ref.~\cite{Anastasiou:2011pi}) can enhance these predictions. \textsc{MCFM} provides an excellent testing environment for these techniques, since it is possible to modify the Higgs propagator as desired and compare it to the full signal plus interference prediction at LO. One such modification is the improved $s-$channel approximation (ISA) due to Seymour~\cite{Seymour:1995np}. In this setup one replaces the fixed-width propagator with the following, 
\begin{eqnarray}
\frac{is}{s-\MH^2+i\Gamma_{\PH}\MH} \rightarrow \frac{i\MH^2}{s-\MH^2+i\Gamma_{\PH}(\MH) \frac{s}{\MH}}. 
\end{eqnarray}
The dependence on $\hat{s}$ in the width piece modifies the corresponding Higgs boson lineshape. We illustrate these differences in \Fref{fig:isa}. In order to ensure the invariant mass distribution is positive definite we also include the prediction for $\Pg\Pg\rightarrow \PW\PW$, $\sigma_{\mathrm B}$. It is clear that by modifying the propagator we have significantly altered the resulting lineshape of the Higgs. The ISA has captured the form of the interference prediction, namely enhancing the differential cross section in the region $\hat{s} < \MH$ and decreasing it in the high mass tail. Whilst the ISA is an improvement over the fixed width approximation it fails to capture the true impact of the destructive interference and overestimates the rate at small $m_{4\ell}$. 

\begin{figure} [htb]
\centering
%\includegraphics[width=12cm]{YRHXS3_HH/\textsc{MCFM}/m4l_int_isa.ps}
\includegraphics[width=8cm]{YRHXS3_HH/MCFM/m4l_int_isa.eps}
\caption{Comparison of the improved $s$-channel approximation and the full LO signal plus interference prediction for the invariant mass of the four lepton final state. For reference the naive signal squared prediction is also plotted. The background production of $\Pg\Pg\rightarrow \PW\PW$ is also included.} 
\label{fig:isa}
\end{figure}

\noindent
{\it Summary}\\[.2em]
%
\textsc{MCFM} provides a range of predictions relevant for searches for a heavy Higgs boson. The most relevant being the signal production cross sections and many of the irreducible background processes. Since the width of a heavy Higgs boson is very large interference effects between signal and background dominate the lineshape of the Higgs. \textsc{MCFM} includes a full treatment of this interference at LO for the $\PW\PW$ decay modes, allowing the user to test systematic uncertainties in other codes which may not model these interactions.

\clearpage
%\end{document}




