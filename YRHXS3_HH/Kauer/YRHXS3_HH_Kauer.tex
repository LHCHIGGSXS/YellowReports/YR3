\DeclareRobustCommand{\PV}{\HepParticle{V}{}{}\Xspace} % V=W,Z boson

\providecommand{\sla}[1]{\ifmmode%
  \setbox0=\hbox{$#1$}%
  \setbox1=\hbox to\wd0{\hss$/$\hss}\else%
  \setbox0=\hbox{#1}%
  \setbox1=\hbox to\wd0{\hss/\hss}\fi%
  #1\hskip-\wd0\box1 }


%\section{Heavy Higgs \footnote{%
%    F.~Convenor, S.~Convenor, \ldots (eds.);
%    \ldots, N.~Kauer} }

%\label{sec:HH}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% gg2VV ----------------------------------------------------------------------

In this section, the impact of Higgs-continuum interference on $\PH\to\PZ\PZ$ 
searches at the LHC for a heavy SM Higgs boson is studied with \textsc{gg2VV} 
\cite{gg2VV}.  \textsc{gg2VV} is a parton-level integrator and event generator 
for all $\Pg\Pg\ (\to \PH)\to \PV\PV \to 4$ leptons processes $(\PV\!=\!\PW,\PZ/\PGg^\ast)$
\cite{Binoth:2005ua,Binoth:2006mf,Binoth:2008pr,Kauer:2012hd}.
It can be used to calculate integrated and differential cross sections with 
scale and PDF uncertainties and to produce unweighted events in LHEF format.  
\textsc{gg2VV} takes into account the complete, fully off-shell $\Pg\Pg\to 4$ 
leptons loop-induced LO matrix element with full spin correlations.  Finite top 
and bottom quark mass effects are included.  Amplitude evaluation is facilitated 
by {\sc FeynArts}/{\sc FormCalc}~\cite{Hahn:2000kx,Hahn:1998yk}.
MC integration is facilitated by {\sc Dvegas}~\cite{Dvegas}, which was 
developed in the context of \Brefs{Kauer:2001sp,Kauer:2002sn}.

% input parameters/settings ---------------------------------------------------

The following input parameters and settings have been used to calculate the
results presented in \refSs{sec:HH_ggVV_2l2l} and \ref{sec:HH_ggVV_WWZZ}.
They also apply to the light Higgs results presented in \refSs{sec:ggF_ggVV_WWZZ}
and \ref{sec:ggF_ggVV_peakshift}.
The input-parameter set of \Bref{Dittmaier:2011ti}, App.\ A,   
is used with NLO $\Gamma_{\PW,\PZ}$ and $G_\mu$ scheme. 
Top and bottom quark mass effects are taken into account, while
lepton masses are neglected.
The renormalization and factorization scales are set 
to $\MH/2$.  In \refS{sec:ggF_ggVV_WWZZ} (\refS{sec:HH_ggVV_2l2l}) 
[\refSs{sec:ggF_ggVV_peakshift} and \ref{sec:HH_ggVV_WWZZ}], the 
PDF set MSTW2008 NNLO (MSTW2008 LO) [CT10 NNLO] with 3(1)[3]-loop running 
for $\alphas(\mu^2)$ and 
$\alphas(\MZ^2)=0.11707$ $(0.13939)$ $[0.1180]$ is used.
The complex-pole scheme \cite{Goria:2011wa} with $\GH=29.16$ $(103.9)$ 
$[416.1]\UGeV$ for $\MH=400$ $(600)$ $[1000]\UGeV$ is used for the Higgs 
resonance.  The fixed-width prescription is used for $\PW$ and $\PZ$ propagators.
The CKM matrix is set to the unit matrix, which causes a 
negligible error \cite{Kauer:2012ma}.
No flavor summation is carried out for charged leptons ($\Pl$) or neutrinos.  
A $p_{\mathrm{T}\PV} > 1\UGeV$ cut is applied to prevent that numerical 
instabilities spoil the amplitude evaluation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsubsection{{\boldmath Signal-background interference in $\Pg\Pg\ (\to \PH)\to \PZ\PZ\to \Pl\PAl\Pl'\PAl'$}}
\label{sec:HH_ggVV_2l2l}

To illustrate signal-background interference effects for a heavy SM Higgs 
boson, integrated cross sections and differential distributions for 
$\Pg\Pg\ (\to \PH)\to \PZ\PZ\to \Pl\PAl\Pl'\PAl'$ in $\Pp\Pp$ collisions 
at $7\UTeV$ for $\MH=400\UGeV$ are presented in \refT{tab:HH_ggVV_2l2l_1} and 
\refFs{fig:HH_ggVV_2l2l_1} and \ref{fig:HH_ggVV_2l2l_2}, respectively.  
Results are given for Higgs signal, $\Pg\Pg$ continuum background 
and the sum with (without) interference.   
To quantify the signal-background interference effect, the
$S$+$B$-inspired measure $R_1$ and $S/\sqrt{B}$-inspired measure $R_2$
defined in \eqn{eqn:ggF_ggVV_WWZZ_1} are used.  When standard cuts 
($p_{\mathrm{T}\Pl} > 20\UGeV$,  $|\eta_{\Pl}| < 2.5$, 
$76\UGeV< M_{\Pl\PAl}, M_{\Pl'\PAl'} < 106\UGeV$) are applied, interference
effects of about $2\%$ are obtained at $7\UTeV$, 
which increase to $3$--$5\%$ when the 
collision energy is increased to $14\UTeV$ (see \refT{tab:HH_ggVV_2l2l_2}).
As shown in \refF{fig:HH_ggVV_2l2l_1}, the Higgs-continuum interference
is negative (positive) for $M_{\PZ\PZ}$ larger (smaller) than $\MH$.
A compensation between negative and positive interference will typically 
occur for integrated cross sections.  Applied selection cuts 
will in general reduce this cancellation, as seen in 
\refF{fig:HH_ggVV_2l2l_2} for the charged-lepton azimuthal opening angle
$\Delta\phi_{\Pl\PAl}$, and should be taken into account to get a reliable
estimate for the interference effect.  Since the Higgs invariant mass can be
reconstructed, it is suggestive to apply a 
$|M_{\PZ\PZ} - \MH| < \GH$ cut in addition to the standard 
cuts to reduce the background (Higgs search cuts).
Such a cut further improves the cancellation of positive and negative
interference, due to the change of sign at $\MH$.  As seen in 
\refTs{tab:HH_ggVV_2l2l_1} and \ref{tab:HH_ggVV_2l2l_2}, the interference 
measures $R_{1,2}-1$ are reduced to the $1\%$ level, when Higgs search
cuts are applied.

\begin{table}
\caption{
Cross sections in $\UfbZ$ for $\Pg\Pg\ (\to \PH)\to \PZ\PZ\to 
\Pl\PAl\Pl'\PAl'$ in $\Pp\Pp$ collisions at $7\UTeV$ for $\MH=400\UGeV$.  
Results are given for signal ($|\PH|^2$), $\Pg\Pg$ continuum background 
($|\mathrm{cont}|^2$) and signal+background+interference 
($|\mathrm{H}$+$\mathrm{cont}|^2$).  
$R_{1,2}$ as defined in 
\eqn{eqn:ggF_ggVV_WWZZ_1} are also displayed.  
Standard cuts: $p_{\mathrm{T}\Pl} > 20\UGeV$,  $|\eta_{\Pl}| < 2.5$, 
$76\UGeV< M_{\Pl\PAl}, M_{\Pl'\PAl'} < 106\UGeV$.  Higgs search cuts: standard 
cuts and $|M_{\PZ\PZ} - \MH| < \GH$ with $\GH=29.16\UGeV$.
No flavor summation is carried out.
The integration error is given in brackets.
}
\label{tab:HH_ggVV_2l2l_1}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{selection cuts} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
standard cuts & $0.3654(4)$ & $0.3450(4)$ & $0.7012(8)$ & $0.987(2)$ & $0.975(3)$ \\
Higgs search cuts & $0.2729(3)$ & $0.01085(2)$ & $0.2867(3)$ & $1.010(2)$ & $1.011(2)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}
\caption{
As \refT{tab:HH_ggVV_2l2l_1}, but for $\sqrt{s} = 14\UTeV$.
}
\label{tab:HH_ggVV_2l2l_2}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{selection cuts} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
standard cuts & $1.893(3)$ & $1.417(2)$ & $3.205(5)$ & $0.969(2)$ & $0.945(3)$ \\
Higgs search cuts & $1.377(2)$ & $0.0531(1)$ & $1.445(2)$ & $1.011(2)$ & $1.011(3)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{figure}
\centering\includegraphics[width=0.48\textwidth]{YRHXS3_HH/Kauer/YRHXS3_HH/YRHXS3_HH_ggVV_fig_MZZ.eps}
\vspace*{0.cm}
\caption{
$M_{\PZ\PZ}$ distributions for $\Pg\Pg\ (\to \PH)\to \PZ\PZ\to 
\Pl\PAl\Pl'\PAl'$ in $\Pp\Pp$ collisions at $7\UTeV$ for $\MH=400\UGeV$.
Distributions for signal ($|\PH|^2$), $\Pg\Pg$ continuum background 
($|\mathrm{cont}|^2$), signal+background ($|\mathrm{H}|^2$+$|\mathrm{cont}|^2$), 
and signal+background+interference ($|\mathrm{H}$+$\mathrm{cont}|^2$)
are shown.  Standard cuts and other details as in \refT{tab:HH_ggVV_2l2l_1}.
}
\label{fig:HH_ggVV_2l2l_1}
\end{figure}

\begin{figure}
\centering\includegraphics[width=0.48\textwidth]{YRHXS3_HH/Kauer/YRHXS3_HH/YRHXS3_HH_ggVV_fig_DeltaPhi.eps}
\vspace*{0.cm}
\caption{
Azimuthal opening angle $\Delta\phi_{\Pl\PAl}$ distributions 
for $\Pg\Pg\ (\to \PH)\to \PZ\PZ\to \Pl\PAl\Pl'\PAl'$ in 
$\Pp\Pp$ collisions at $7\UTeV$ for $\MH=400\UGeV$.
Other details as in \refF{fig:HH_ggVV_2l2l_1}.
}
\label{fig:HH_ggVV_2l2l_2}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsubsection{{\boldmath $\PZ\PZ/\PW\PW$ interference in $\Pg\Pg\ (\to \PH)\to 
\Pl\PAl\PGnl\PAGnl$}}
\label{sec:HH_ggVV_WWZZ}

In \refS{sec:HH_ggVV_2l2l}, the key features of signal-background interference in 
$\PH\to\PZ\PZ$ searches were elucidated using the ``golden mode.''
In this section, $\PW\PW$ corrections are studied that occur when the 
same-flavor final state $\Pl\PAl\PGnl\PAGnl$ is produced.\footnote{%
See also \refS{sec:ggF_ggVV_WWZZ}.}  Results obtained with a minimal 
$M_{\Pl\PAl} > 4\UGeV$ cut are shown in \refTs{tab:HH_ggVV_WWZZ_1} and 
\ref{tab:HH_ggVV_WWZZ_2} for $\MH=600\UGeV$ and $\MH=1\UTeV$, respectively, 
at $\sqrt{s}=8\UTeV$.  Cross sections when only either the $\PW\PW$ or 
$\PZ\PZ$ intermediate state is included are also given.
As above, results for Higgs signal, $\Pg\Pg$ continuum background 
and the sum with interference as well as the interference measures $R_{1,2}$
are displayed.  Without search cuts, $S/B$ decreases significantly with 
increasing heavy Higgs mass.  Consequently, the interference measure 
$R_2-1$ increases substantially when going from $\MH=600\UGeV$ to $\MH=1\UTeV$,
namely from $\Ord(20\%)$ to $\Ord(2)$.  In addition to signal-background
interference, the interference between $\PW\PW$ and $\PZ\PZ$ contributions
is also of interest.  For the $\Pg\Pg$ continuum background, one finds
$\sigma(|\PW\PW+\PZ\PZ|^2)/\sigma(|\PW\PW|^2+|\PZ\PZ|^2)=0.935(5)$,
while for the signal this ratio agrees with $1$ at the sub-percent
level for $\MH=600\UGeV$ and $1\UTeV$.  
The $\Ord(5\%)$ $\PW\PW/\PZ\PZ$ interference for the $\Pg\Pg$ continuum 
background has to be compared to the negligible $\PW\PW/\PZ\PZ$ interference
found in \Bref{Melia:2011tj} for the quark-induced continuum background.

\begin{table}
\caption{
Cross sections in $\UfbZ$ for $\Pg\Pg\ (\to \PH)\to \PW\PW, \PZ\PZ\to 
\Pl\PAl\PGnl\PAGnl$ (same flavor) in $\Pp\Pp$ collisions at $8\UTeV$ for 
$\MH=600\UGeV$.  Cross sections when only either the $\PW\PW$ or 
$\PZ\PZ$ intermediate state is included are also given.
A minimal $M_{\Pl\PAl} > 4\UGeV$ cut is applied.
Other details as in \refT{tab:HH_ggVV_2l2l_1}.
}
\label{tab:HH_ggVV_WWZZ_1}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{$\PV\PV$} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
$\PW\PW$ & $1.44(1)$ & $12.29(3)$ & $14.10(5)$ & $1.027(4)$ & $1.26(4)$ \\
$\PZ\PZ$ & $0.261(2)$ & $1.590(5)$ & $1.896(6)$ & $1.024(4)$ & $1.17(3)$ \\
$\PW\PW, \PZ\PZ$ & $1.69(2)$ & $12.98(6)$ & $15.00(8)$ & $1.022(7)$ & $1.19(6)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}
\caption{
As \refT{tab:HH_ggVV_WWZZ_1}, but for $\MH=1\UTeV$.
}
\label{tab:HH_ggVV_WWZZ_2}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{$\PV\PV$} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
$\PW\PW$ & $0.0772(5)$ & $10.50(3)$ & $10.72(3)$ & $1.013(4)$ & $2.8(5)$ \\
$\PZ\PZ$ & $0.01426(9)$ & $1.353(4)$ & $1.387(4)$ & $1.015(4)$ & $2.4(4)$ \\
$\PW\PW, \PZ\PZ$ & $0.0914(6)$ & $11.02(6)$ & $11.30(8)$ & $1.017(9)$ & $3(1)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

% H --> ZZ search cuts -------------------------------------------------------

We now investigate the impact of $\PH\to\PZ\PZ$ search cuts on the 
signal-background interference.  To be specific, we consider the cuts
$|M_{\Pl\PAl}-\MZ| < 15\UGeV$, $\sla{E}_\mathrm{T} > 110\UGeV$, 
$M_\mathrm{T} > 325\UGeV$. The transverse mass is defined as
\beq
\label{eqn:HH_ggVV_WWZZ_1}
M_\mathrm{T}=\sqrt{ \left(M_{\mathrm{T},\Pl\PAl}+\sla{M}_{\mathrm{T}}\right)^2-({\bf{p}}_{\mathrm{T},\Pl\PAl}+{\sla{\bf{p}}}_\mathrm{T})^2 }\quad\mathrm{with}\quad \sla{M}_{\mathrm{T}}=\sqrt{\sla{p}_{\mathrm{T}}^2+M_{\Pl\PAl}^2}\,.
\eeq
Results for $\MH=600\UGeV$ and $1\UTeV$
are shown in \refTs{tab:HH_ggVV_WWZZ_3} and \ref{tab:HH_ggVV_WWZZ_4}, 
respectively.  Since the Higgs search cuts suppress the background while
retaining the signal, one can expect that with search cuts $R_2$ deviates 
less from $1$ than when only minimal cuts are applied. This is confirmed
by the shown results: For $\MH=600\UGeV$ ($1\UTeV$),
$R_2-1$ decreases from $\Ord(20\%)$ ($\Ord(2)$) to $\Ord(7\%)$ ($\Ord(1)$).

\begin{table}
\caption{
Cross sections in $\UfbZ$ for $\Pg\Pg\ (\to \PH)\to \PW\PW, \PZ\PZ\to 
\Pl\PAl\PGnl\PAGnl$ (same flavor) in $\Pp\Pp$ collisions at $8\UTeV$ for 
$\MH=600\UGeV$.  $\PH\to\PZ\PZ$ search cuts are applied: 
$|M_{\Pl\PAl}-\MZ| < 15\UGeV$, $\sla{E}_\mathrm{T} > 110\UGeV$, 
$M_\mathrm{T} > 325\UGeV$.  
$M_\mathrm{T}$ is defined in \eqn{eqn:HH_ggVV_WWZZ_1}.
Other details as in \refT{tab:HH_ggVV_WWZZ_1}.
}
\label{tab:HH_ggVV_WWZZ_3}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{$\PV\PV$} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
$\PZ\PZ$ & $0.2175(8)$ & $0.0834(2)$ & $0.3150(8)$ & $1.047(4)$ & $1.065(6)$ \\
$\PZ\PZ, \PW\PW$ & $0.2220(8)$ & $0.1020(2)$ & $0.3406(8)$ & $1.051(4)$ & $1.075(6)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}
\caption{
As \refT{tab:HH_ggVV_WWZZ_3}, but for $\MH=1\UTeV$.
}
\label{tab:HH_ggVV_WWZZ_4}%
\renewcommand{\arraystretch}{1.2}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lccccc}
\hline
\multicolumn{1}{c}{$\PV\PV$} & $|\PH|^2$ & $|\mathrm{cont}|^2$ & $|\mathrm{H}$+$\mathrm{cont}|^2$ & $R_1$ & $R_2$ \\
\hline
$\PZ\PZ$ & $0.01265(5)$ & $0.0687(2)$ & $0.0927(2)$ & $1.140(3)$ & $1.90(2)$ \\
$\PZ\PZ, \PW\PW$ & $0.01278(5)$ & $0.0846(3)$ & $0.1090(2)$ & $1.119(3)$ & $1.91(3)$ \\
\hline
\end{tabular}
\end{center}
\end{table}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% \subsubsubsection*{Acknowledgments}
% 
% Financial support from HEFCE, STFC and the IPPP Durham is gratefully 
% acknowledged by N.K.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% LocalWords:  sn ggVV WWZZ
