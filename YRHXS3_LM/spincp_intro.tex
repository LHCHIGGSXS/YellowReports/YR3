Since a clear signal for a resonance consistent with
the long sought Higgs boson
has been established \cite{Aad:2012tfa,Chatrchyan:2012ufa},
the next step is a detailed study of its properties.
In this Section we focus on the spin/CP properties of the new resonance, and review the strategies
to determine whether the Higgs-like boson is consistent with the spin zero particle predicted in the SM,
with $J^{CP}=0^{+}$, and the extent to which it could be a mixture of different CP eigenstates.
We recall that, as discussed in \refS{sec:LM_eft}, going beyond the interim framework for coupling studies
presented in \refS{sec:LM_ir:framework} will require the analysis of couplings and
spin/CP properties to be treated in a common framework. Nonetheless, to the purpose of presentation, we discuss here
the spin/CP properties in a separate section.

The observation of the new resonance in the decay modes
$\Hgaga$ \cite{ATLAS-CONF-2013-012,CMS-PAS-HIG-13-001},
$\HZZ$   \cite{ATLAS-CONF-2013-013,CMS-PAS-HIG-13-002} and
$\HWW$   \cite{ATLAS-CONF-2013-030,CMS-PAS-HIG-13-003} allows multiple independent tests of the spin/CP properties.
Thanks to the Landau-Yang theorem \cite{Landau:1948kw,Yang:1950rg},
the observation in the $\Hgaga$ mode already rules out the possibility that the new resonance
has spin 1, and, barring $C$ violating effects in the Higgs sector, fixes $C=+1$.

Having ruled out the $J=1$ possibility\footnote{Note that there are two caveats to this argument. The first is that the Landau-Yang theorem strictly applies to an on-shell resonance. This means that the $J=1$ hypothesis can be excluded only by making an additional small-width assumption. The second is that in principle the decay product could consist of 2 pairs of boosted photons each misinterpreted as a single photon.}, the case $J=2$ is of course the first one that should be tested.
This possibility turns out to be extremely challenging from a theoretical view point.
The naive coupling of a massive spin-2 field with a $U(1)$ gauge field leads to the Velo-Zwanziger problem \cite{Velo:1969bt,Velo:1970ur},
and the model develops modes that travel superluminally and other pathological features.
Detailed studies have shown that
such models have an intrinsic cut off of the order of the mass of the spin-2 resonance \cite{Porrati:2008gv}.
A consistent effective description could be obtained by interpreting the spin 2 particle as a Kaluza Klein (KK)
graviton. However, one should then explain why analogous KK excitations of the SM gauge bosons have never been observed.
Moreover, a recent study  \cite{Ellis:2012mj} has shown that a graviton-like massive spin 2 boson would have
too small couplings to $\PW\PW$ and $\PZ\PZ$ with respect to $\PGg\PGg$, and that in many models with a compactified extra dimension the massive spin 2 boson would have equal coupling to $\Pg\Pg$ and $\PGg\PGg$, thus leading to $\Gamma(\PH\to \Pg\Pg)\sim 8\Gamma(\Hgaga)$, which seems to be in contradiction to the observed data.


The strategies to determine the properties of a resonance through its decays to gauge bosons
date back to more than 50 years ago. Photon polarization in $\PGpz\to\PGg\PGg$ decay can be used to measure the pion parity \cite{Yang:1950rg} but it turns out to be easier to use the orientation of the decay planes in the $\PGpz\to\Pep\Pem\Pep\Pem$ \cite{Plano:1959zz} decay. Analogously, the $\PH\to \PZ\PZ\to 4\Pl$ decay mode, allowing full control on the event kinematics, is an excellent channel to study spin, parity and tensor structure of the coupling of the Higgs-like resonance.
As discussed in detail in \refS{sec:LM_cp_maggie}, the invariant mass
distribution of the off shell gauge boson in $\PH\to \PV\PV^*$ is
proportional to the velocity
$d\Gamma/dM_*\sim\beta\sim \sqrt{(\mH-m_{\PV})^2-M_*^2}$ (where $M_*$
denotes the invariant mass of the off-shell gauge boson) and therefore
features a characteristic steep behavior with $M_*$ just below the
kinematical threshold. This behavior is related to the spin-zero nature
of the SM Higgs boson and will rule out other spin assignments with the
exception of the $1^+$ and $2^-$ cases, which can be ruled out through
angular correlations. The pseudoscalar case $0^-$ can 
be instead discriminated against the SM $0^+$ by studying the distribution in the azimuthal angle $\phi$ between the two $Z$ decay planes \cite{Dell'Aquila:1985ve,Choi:2002jk}.
%%%%%
It should be noted, however, that in many models of physics beyond the
SM there is no lowest-order coupling between a pseudoscalar and a pair
of gauge bosons, so that the decay $\PA \to \PZ\PZ \to 4\Pl$ can be heavily suppressed compared to the decay $\PH \to \PZ\PZ \to 4\Pl$. For a state that is an admixture
between CP-even and CP-odd components the decay into $\PZ\PZ$ essentially
projects to the CP-even part in such a case, so that the angular
distributions would show a pure CP-even pattern although the state in
fact has a sizable CP-odd component. See section \refS{sec:LM_pseudo} for a more
detailed discussion of this issue.

More generally, instead of relying on specific kinematical variables, one can try to exploit the full
information on the event. The {\em matrix element method} uses the tree level amplitude to construct a likelihood to isolate a signal over a background, or to discriminate between two different signal hypothesis.
The construction of the matrix element can be carried out by using two different strategies: the {\em effective Lagrangian} (see \refS{sec:LM_eft} and \refS{sec:LM_mad}) and {\em anomalous couplings} (see \refS{sec:LM_jhu}: 'generic parameterizations') approaches. The former implies to write the most general effective Lagrangian compatible with Lorentz and gauge invariance. The latter implies to write the most general {\em amplitude} compatible with Lorentz and gauge invariance, but does not assume a hierarchy in the scales, and thus the couplings become momentum dependent form factors. The effective Lagrangian approach has the advantage that it can be extended beyond LO.
The anomalous coupling approach is restricted to LO but somewhat more general, since it is valid also in the case in which new light degrees of freedom are present and circulate in the loops. The effective Lagrangian approach is being pursued by the {\sc MadGraph} team (see \refS{sec:LM_mad}). The anomalous coupling approach has been used to perform studies on the spin/CP properties of the Higgs boson \cite{DeRujula:2010ys}, and its most widely used implementation is so called MELA approach\footnote{ATLAS uses also the BDT method.} \cite{Gao:2010qx}, described in \refS{sec:LM_jhu} (see also MEKD~\cite{Avery:2012um}).

Here we note that the matrix element method is {\em maximally} model dependent, since it allows to exclude various specific models one by one. An issue which is important to understand is the extent to which
the discrimination of a given spin and CP hypothesis depends on the
production model assumed. The results for the $\Hgaga$ channel
recently presented by the ATLAS
collaboration \cite{ATLAS-CONF-2013-029} show that the spin-2 hypothesis can be discriminated only by assuming a $\Pg\Pg$ fusion production mode.
This is somewhat related to the fact that the $\Hgaga$ channel offers essentially
only one angular variable, the polar angle $\theta^*$ of the photons
in the Higgs rest frame. The situation is different in the $\HWW$
decay mode, where a discrimination against the $2^+$ hypothesis is
possible \cite{ATLAS-CONF-2013-031}, although, thanks to spin correlations, the discriminating power is maximum if the Higgs is produced in the $\Pq\Pqb$ channel.
As discussed above, it is the $\PH\to \PZ\PZ\to 4\Pl$ channel that
offers the maximum amount of information. Here, the dependence on the
production model is present but the experimental
results \cite{ATLAS-CONF-2013-013} show that the discrimination power is
essentially independent on the production model. This is consistent with
what is shown in \refS{sec:LM_jhu}, and is due to the fact that, as observed in \Bref{DeRujula:2010ys}, the selection cuts {\em sculpt} the angular distributions making the dependence on the production model rather marginal.

Another channel that can be used to test the CP structure of the $\PH\PV\PV$ vertex is the $\PH\PV$ associated production
of the Higgs boson with a vector boson ($\PV=\PWpm,\PZ$).
In \Bref{Ellis:2012xd} it has been noted that the invariant mass distribution of the $\PV\PH$ system would be very different in the $0^+$, $0^-$ and $2^+$ hypotheses, thus providing a fast track indicator on Higgs spin and CP properties.
We point out that these differences in the invariant mass distribution, together with analogous differences in
other kinematical distributions \cite{Ellis:2013ywa,Djouadi:2013yb}, are related to the fact that such spin/CP assignments lead to interactions with the vector bosons that are mediated by higher dimensional operators.

The Higgs CP properties and the structure of the $\PH\PV\PV$ vertex can also be studied in vector boson fusion (VBF),
by looking at the azimuthal separation of the two tagging jets \cite{Plehn:2001nj}.
Recent studies on the determination of Higgs Spin/CP properties in VBF are presented in \Brefs{Frank:2012wh,Englert:2012xt,Djouadi:2013yb}. When more data will be available, the VBF channel will nicely complement the information obtained in the inclusive Higgs production modes.

The experimental analyses of the CP properties have so far mainly
focused on discriminating between the distinct hypotheses of a pure
CP-even and a pure CP-odd state. First studies towards dealing with an
admixture of CP-even and CP-odd components have recently been presented
by the CMS collaboration \cite{CMS-PAS-HIG-13-002}. As mentioned above, however, angular
distributions in $\PH \to \PZ\PZ$ and $\PH \to \PW\PW$ decays as well as invariant
mass distributions and azimuthal distributions in $\PV\PH$ and VBF production will have a limited
sensitivity for discriminating between a pure CP-even state and a mixed
state if the coupling of the CP-odd component to $\PV\PV$ is suppressed
compared to the $\PH\PV\PV$ coupling. The couplings of the Higgs boson to
fermions offer a more democratic test of its CP nature,
since in this case the CP even and odd components can have the same magnitude.
In this respect, if the $\PH\PQt\PAQt$ channel can be exploited sufficiently
well this would offer a good opportunity
to study Higgs CP properties \cite{Gunion:1996xu,Gunion:1998hm,Field:2002gt}.

The remainder of this Section is organized as follows. In \refS{sec:LM_pseudo} an overview is given
about the coupling of a pseudoscalar to gauge bosons in different models
of physics beyond the SM. In \refS{sec:LM_cp_maggie} the theoretical basis for spin
and parity studies at the LHC is reviewed. In \refS{sec:LM_jhu} the matrix
element approach based on the {\sf JHU} generator is briefly reviewed. In \refS{sec:LM_mad} the effective Lagrangian approach implemented by the {\sc MadGraph} group is presented, together with a comparison with {\sc JHU} results.

%We should not forget, however, that these studies all share a potential theoretical limitation.
%The data tell us that the Higgs-like resonance has substantial rates in vector boson pairs ($\PGg\PGg$, $\PZ\PZ$ and $\PW\PW$). This implies that this particle must have a significant CP even component, since the couplings of a pseudoscalar
%to vector boson pairs are loop induced. On the other hand, for the same reason, it will be difficult to rule out the existence of a small CP odd component. First studies in this direction have been recently presented by the CMS collaboration \cite{CMS13002}.
%The couplings of the Higgs boson to fermions offer a more democratic test of its CP nature, since in this case the CP even and odd components can have the same magnitude. In this respect, the $\PH\PQt\PAQt$ channel will offer a nice opportunity to study Higgs CP properties \cite{Gunion:1996xu,Gunion:1998hm,Field:2002gt}, once enough integrated luminosity will be accumulated. In \refS{sec:LM_pseudo} a discussion of possible BSM scenarios in which a pseudoscalar Higgs boson couples to gauge bosons is presented.

%The remainder of this section is organized as follows.
%In \refS{sec:LM_cp_maggie} the theoretical basis for spin and parity studies
%at the LHC is reviewed. In \refS{sec:LM_jhu} the matrix element approach based on the JHU generator is briefly discussed.
%In \refS{sec:LM_pseudo} a discussion of possible BSM scenarios in which a pseudoscalar Higgs boson couples to gauge bosons is presented.







