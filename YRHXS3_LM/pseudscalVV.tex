As discussed above, the present analyses of the CP properties of the new 
state are based in particular on the investigation of angular distributions of 
the decays $\PH \to \PZ\PZ \to 4\Pl$ and $\PH \to \PW\PW \to 4\Pl$, of
the azimuthal separation of the two tagging jets in VBF and of the
invariant mass distributions of the WH and ZH production processes. 
It should be noted that all the above processes involve the coupling 
of the new state to two gauge bosons, $\PH\PV\PV$, where $\PV = \PW, \PZ$ 
(this coupling also plays an important role for the processes 
$\PH \to \PGg\PGg$ and $\PH \to \PZ\PGg$ via the $\PW$~loop
contribution). 

The angular and kinematic distributions in these processes will only
provide sensitivity for a discrimination between CP-even and CP-odd
properties if a possible CP-odd component A of the new state couples
with sufficient strength to $\PW\PW$ and $\PZ\PZ$. If however the $\PA\PV\PV$ coupling is 
heavily suppressed compared to the coupling of the CP-even component, 
the difference between a pure CP-even state and a state that is a
mixture of CP-even and CP-odd components would merely manifest itself as
a modification of the total rate (which could at least in principle also
be caused by other effects).
The angular distributions in this case, on the
other hand, would show
no deviations from the expectations for a pure CP-even state, even if
the state had a sizable CP-odd component.

The extent to which the above analyses will be able to reveal effects of
a CP-odd component of the new state therefore crucially depends on 
the coupling strength AVV in comparison to the coupling of the CP-even
component to gauge bosons. In the following we will briefly discuss this
issue for different kinds of models of physics beyond the SM.

While the coupling of a CP-even scalar to VV is present in lowest order in
renormalizable gauge theories, no such coupling exists for a CP-odd
scalar. In general pseudoscalar states A couple to gauge bosons
%($V=\PW,\PZ,\PGg,\Pg$) 
via dimension 5 operators,
\begin{equation}
{\mathcal L}_{\mathrm{AVV}} = \frac{c_{\mathrm V}}{\Lambda}~A~V^{a\mu\nu} \tilde V^a_{\mu\nu}
\label{eq:pseudoscalar}
\end{equation}
with $\tilde V^a_{\mu\nu} =
\frac{1}{2}\epsilon_{\mu\nu\rho\sigma}V^{a\rho\sigma}$ denoting the dual
field strength tensor (the same structure also holds for couplings to
photons and gluons). In the effective operator given in
Eq.~(\ref{eq:pseudoscalar}) $c_{\mathrm V}$ denotes the coupling strength emerging
from the theory at the scale $\Lambda$.  These couplings can either
arise via loop effects in renormalizable weakly-interacting models as
e.g.~extensions of the Higgs sector or supersymmetric models, or they
can occur in non-perturbative models as e.g.~expanded versions of
technicolor~\cite{Weinberg:1975gm,Weinberg:1979bn,Susskind:1978ms} with
a typical cut-off scale $\Lambda$.

\subsubsection{ Weakly interacting models}
%            =========================

In weakly interacting models the pseudoscalar coupling to gauge bosons
is mediated by loop effects (dominantly top loops in many models) such
that the coupling $c_{\mathrm V}$ of Eq.~(\ref{eq:pseudoscalar}) is related to the
contributing Yukawa couplings of the underlying model and $\Lambda$ to
the masses of the fermions involved in the corresponding contribution to
the chiral anomaly.

This loop-induced coupling turns out to be heavily suppressed 
in several classes of BSM models. In particular, in the minimal
supersymmetric extension of the SM (MSSM) the production times decay
rates for purely pseudoscalar Higgs boson states are typically
suppressed by three orders of magnitude or more (see e.g.\ the analysis in
\cite{Bernreuther:2010uw} and the case of a CP-violating scenario
discussed in \cite{Figy:2010ct}). Within the MSSM, the detection of any
pseudoscalar component of a mixed scalar-pseudoscalar 
via the above analyses at the LHC therefore does not look feasible.
The same holds for the pseudo-axion states in Little Higgs models
\cite{ArkaniHamed:2001nc,ArkaniHamed:2002pa,ArkaniHamed:2002qy}.

The situation may be somewhat better in other extensions of the SM.
In \cite{Bernreuther:2010uw} a general type II 2HDM was investigated.
Based on a scan over the relevant parameters it was found 
that rates for pseudoscalar
production and decay can reach a detectable level at the LHC for small
values of $\tan\beta$ (i.e.\ $\tan\beta < 1$), 
where the pseudoscalar rates can get close to or even exceed 
the rates for a SM Higgs in some cases. In regions of
significant and observable rates at the LHC the pseudoscalar Higgs boson
is predominantly produced via gluon fusion $gg\to A$ which is enhanced
by the larger top quark contribution than for the SM Higgs boson. The
loop-induced decays into gauge bosons also involve the relatively large
coupling of the top quark to the pseudoscalar for
small values of $\tan\beta$. The main difference to the MSSM is that
small values of $\tan\beta$ are still allowed within the type II 2HDM,
while the MSSM is strongly constrained by the direct MSSM Higgs searches
so that such small values of $\tan\beta$ are excluded \cite{Schael:2006cr}.


\subsubsection{Strongly interacting models}
%           ===========================

Turning now to strongly interacting models involving non-perturbative
effects, effective couplings of pseudoscalar states to gauge bosons
naturally arise in technicolor models
\cite{Weinberg:1975gm,Weinberg:1979bn,Susskind:1978ms}, where the
pseudoscalar pseudo-Nambu-Goldstone bosons (PNGB) couple to the
respective chiral anomalies and the associated axial vector currents
\cite{Dimopoulos:1980yf,Ellis:1980hz,Chivukula:1995dt}. Moreover, these
types of couplings can be generated by instanton effects in the
framework of these models. The cut-off scale $\Lambda$ of the novel
underlying strong interactions is related to the corresponding
pseudoscalar decay constant $F_{\mathrm A}$ defined by the associated PNGB
couplings to the axial vector currents via PCAC,
\begin{equation}
\langle 0|j^\mu_5|A(p)\rangle = i F_{\mathrm A} p^\mu
\end{equation}
where $j^\mu_5$ denotes the axial vector current emerging from the novel
techni-fermions and $p^\mu$ the four-momentum of the pseudoscalar field
$A$. The coupling of the PNGB $A$ to gauge bosons can be derived from the
corresponding coupling to the divergence of the axial vector current,
\begin{equation}
{\mathcal L}_{\mathrm A} = \frac{A}{F_{\mathrm A}}\partial_\mu j^\mu_5
\end{equation}
Even for massless techni-fermions the divergence of the axial vector
current develops an anomalous contribution, the
Adler--Bell--Jackiw-anomaly (ABJ-anomaly) \cite{Adler:1969gk,Bell:1969ts},
\begin{equation}
\partial_\mu j^\mu_5 = -S_{\mathrm V}~\frac{g_{\mathrm V}^2}{16\pi^2}~V^{a\mu\nu} \tilde
V^a_{\mu\nu}
\end{equation}
where $S_{\mathrm V}$ is the associated anomaly coefficient of the
corresponding gauge group and $g_{\mathrm V}$ its gauge coupling to the
techni-fermions. Finally one obtains the effective PNGB couplings to
gauge bosons \cite{Dimopoulos:1980yf,Ellis:1980hz,Chivukula:1995dt}
\begin{equation}
{\mathcal L}_{\mathrm{AVV}} = -S_{\mathrm V}~\frac{g_{\mathrm V}^2}{16\pi^2 F_{\mathrm A}}~A~V^{a\mu\nu} \tilde
V^a_{\mu\nu}
\end{equation}
Depending on the size of the anomaly coefficient $S_{\mathrm V}$ and the PNGB
decay constant $F_{\mathrm A}$ these effective couplings can imply production and
decay processes of these pseudoscalar states at the LHC which can reach 
similar orders of magnitude as for SM Higgs bosons. For CP-non-conserving 
technicolor models in general large admixtures of
scalar and pseudoscalar components are possible which could lead to sizeable
deviations of e.g.~the angular distributions in final states with 4
charged leptons.

A rigorous analysis of PNGB production and decay at the LHC has been
performed in the framework of top-color assisted technicolor models
\cite{Hill:1994hp}.  These models introduce two separate strongly
interacting sectors in order to explain electroweak symmetry breaking
(EWSB) and the large top quark mass at the same time. Techni-fermion
condensates generate most of the EWSB but contribute only little to the
top mass. The latter is induced by the condensation of top-antitop pairs
which eventually generates a large Yukawa coupling of the techni-pion to
top quarks. The top condensate makes only a small contribution to EWSB.
Within this class of models pseudoscalar rates of the order of the SM
Higgs rates can be reached in a large part of the available parameter
space \cite{Bernreuther:2010uw}. In contrast to the top quark case the
much smaller Yukawa coupling of the pseudoscalar top-pion state to
bottom quarks is induced by extended technicolor as well as top-color
instanton effects. As before the pseudoscalar couplings to gauge bosons
are generated by the ABJ-anomaly. At the LHC a discrimination between
scalar and pseudoscalar components at the level of ${\mathcal O}(10\%)$
could help to put constraints on possible mixing between scalar and
pseudoscalar fields within these models.
