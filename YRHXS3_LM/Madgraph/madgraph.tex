In this section we introduce a complete framework, based on an effective field
theory description, that allows to perform characterization studies of a new
boson in all relevant channels in a consistent, systematic and accurate way.

In the following we present the implementation of an effective Lagrangian featuring bosons $X(J^P)$ with various assignments of spin/parity
 $J^P=0^+,0^-,1^+,1^-,2^+,2^-$ that can be used to test the nature of the recently-discovered
 new boson with mass around 125~GeV at the LHC~\cite{Aad:2012tfa, Chatrchyan:2012ufa}. The new
 states can couple to Standard Model particles via interactions of the
 minimal (and next-to-minimal) dimensions. The implementation of the
 Lagrangian is done in \textsc{FeynRules}~\cite{Christensen:2008py} and the
 corresponding model named \emph{Higgs Characterization model}. It extends and
 completes an earlier version used in \Bref{Englert:2012xt} and it is publicly
 accessible at~\cite{feynrules}. It is therefore
 available to all matrix element generators featuring an UFO
 interface~\cite{Degrande:2011ua}. For our study we have
 employed \textsc{MadGraph~5}~\cite{Alwall:2011uj}. Results at the NLO
 accuracy in QCD can be automatically (or semi-automatically) obtained
 via \textsc{aMC@NLO}~\cite{Frederix:2009yq,Hirschi:2011pa}.  

There are several advantages in having a first principle implementation
in terms of an effective Lagrangian which can be automatically
interfaced to a matrix element generator (and then to an event
generator). First and most important, all relevant production and decay
modes can be studied within the same model, from gluon-gluon fusion to
VBF as well as $\PVB\PH$ and $\PAQt\PQt$ associated
productions can be considered and the corresponding processes
automatically generated within minutes. Second, it is straightforward to
modify the model implementation to extend it further in case of need, by
adding further interactions, for example of higher-dimensions. Finally,
higher-order effects can be easily accounted for, by generating
multi-jet merged samples or computing NLO corrections with automatic
frameworks. 
All the detailed demonstration and analyses are currently in
progress~\cite{Artoisenet:2013jma}.\\

In the following we first write down the
effective Lagrangian explicitly and then show comparison plots with JHU results as
presented in \Bref{Bolognesi:2012mm} which are currently employed
by both ATLAS and CMS collaborations. 
We remind here that, even though we list several cases of interest in a
simple conversion table as a dictionary between JHU and our \emph{Higgs
Characterization model}~\cite{Artoisenet:2013jma}, in 
general the two approaches will be different.  Our implementation is
based on an effective theory approach valid up to the scale $\Lambda$,
while JHU describes the interaction between the new state and SM
particles  in terms of  anomalous couplings.   


\subsubsection{The effective Lagrangian}

\begin{table}[h]
\begin{center}
\caption{Model parameters.}
\label{tab:param}
\begin{tabular}{lll}
\hline
 parameter\hspace*{5mm} & default value\hspace*{5mm} & description \\
\hline
 $\Lambda$ [GeV] & $10^3$ & cutoff scale \\
 $c_{\alpha}(\equiv \cos\alpha$) & 1 & mixing between $0^+$ and
	 $0^-$ \\
 $\kappa_i$ & 1 or 0 & dimensionless coupling parameter \\
\hline
\end{tabular}
\end{center}
\end{table}


\subsubsubsection{Spin 0}

The spin-0 $\PX$ interaction Lagrangian with fermions and vector-bosons
are given by 
%
\begin{align}
 {\mathcal L}_0^{\Pf} = -\big[ c_{\alpha}\kappa_{\PH\Pf\Pf}g_{\PH\Pf\Pf}\, \bar\psi_{\Pf}\psi_{\Pf}
               +s_{\alpha}\kappa_{\PA\Pf\Pf}g_{\PA\Pf\Pf}\, \bar\psi_{\Pf} i\gamma_5\psi_{\Pf} \big] \PX_0,
\label{FRMG5HC:1}
\end{align}
%
and
%
\begin{align}
 {\mathcal L}_0^{\PVB} =\bigg[&
  c_{\alpha}\kappa_{\SM}\big[\frac{1}{2}g_{\PH\PZ\PZ}\, \PZ_\mu \PZ^\mu 
                                +g_{\PH\PW\PW}\, \PW^{+\mu} \PW^{-\mu}\big] \nonumber\\
  &-\frac{1}{4}\big[c_{\alpha}\kappa_{\PH\PGg\PGg}g_{\PH\PGg\PGg} \, \PA_{\mu\nu}\PA^{\mu\nu}
        +s_{\alpha}\kappa_{\PA\PGg\PGg}g_{\PA\PGg\PGg}\,\PA_{\mu\nu}\widetilde \PA^{\mu\nu}
 \big] \nonumber\\
  &-\frac{1}{2}\big[c_{\alpha}\kappa_{\PH\PZ\PGg}g_{\PH\PZ\PGg} \, \PZ_{\mu\nu}\PA^{\mu\nu}
        +s_{\alpha}\kappa_{\PA\PZ\PGg}g_{\PA\PZ\PGg}\,\PZ_{\mu\nu}\widetilde \PA^{\mu\nu} \big] \nonumber\\
  &-\frac{1}{4}\big[c_{\alpha}\kappa_{\PH\Pg\Pg}g_{\PH\Pg\Pg} \, G_{\mu\nu}^aG^{a,\mu\nu}
        +s_{\alpha}\kappa_{\PA\Pg\Pg}g_{\PA\Pg\Pg}\,G_{\mu\nu}^a\widetilde G^{a,\mu\nu} \big] \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda}\big[c_{\alpha}\kappa_{\PH\PZ\PZ} \, \PZ_{\mu\nu}\PZ^{\mu\nu}
        +s_{\alpha}\kappa_{\PA\PZ\PZ}\,\PZ_{\mu\nu}\widetilde \PZ^{\mu\nu} \big] \nonumber\\
  &-\frac{1}{2}\frac{1}{\Lambda}\big[c_{\alpha}\kappa_{\PH\PW\PW} \, \PW^+_{\mu\nu}\PW^{-\mu\nu}
        +s_{\alpha}\kappa_{\PA\PW\PW}\,\PW^+_{\mu\nu}\widetilde \PW^{-\mu\nu} \big]
 \bigg] \PX_0, 
\end{align}
%
where the (reduced) field strength tensors are
%
\begin{align}
 V_{\mu\nu} &=\partial_{\mu}V_{\nu}-\partial_{\nu}V_{\mu}\quad (V=\PA,\PZ,\PWpm), \\
 G_{\mu\nu}^a &=\partial_{\mu}G_{\nu}^a-\partial_{\nu}G_{\mu}^a
  +g_sf^{abc}G_{\mu}^bG_{\nu}^c,
\end{align}
%
and the dual tensor is
%
\begin{align}
 \widetilde V_{\mu\nu} =\frac{1}{2}\epsilon_{\mu\nu\rho\sigma}V^{\rho\sigma}.
\end{align}
%
The model parameters in the Lagrangian that are possible to be modified are
listed in \Tref{tab:param}. This parameterization allows to describe the mixing
between $CP$-even and $CP$-odd states and correspondingly to give an effective
description of a reasonably ample range of $CP$-violating scenarios, such as those arising 
in SUSY or in a generic 2HDM.

The dimensionful couplings are set so as to reproduce a SM Higgs in the case $c_\alpha=1$ and
a pseudo scalar in a 2HDM with $\tan \beta=1$ for the default values of
$\kappa_i$, e.g. $g_{\PH\Pf\Pf}=m_{\Pf}/v$ and $g_{\PH\PVB\PVB}=2m_{\PVB}^2/v$ as well as
$g_{\PH\Pg\Pg}=-\alpha_s/3\pi v$ in the heavy top loop limit.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsubsection{Spin 1}

The spin-1 $\PX$ interaction Lagrangian with fermions is
%
\begin{align}
 {\mathcal L}_1^{\Pf} = \sum_{f=\PQu,\PQd} 
      \bar\psi_{\Pf} \gamma_{\mu}(\kappa_{f_a}a_{\Pf} - \kappa_{\Pf_b}
           b_{\Pf}\gamma_5)\psi_{\Pf} \PX_{1}^{\mu},
\end{align}
%
where $\PQu$ and $\PQd$ denote the up-type and down-type quarks, respectively. 
The $a_{\Pf}$ and $b_{\Pf}$ are the SM couplings, i.e.
%
\begin{align} 
 a_{\PQu} &= \frac{g}{2\cw}\Big(\frac{1}{2}-\frac{4}{3}\sw^2\Big),\quad
 b_{\PQu} = \frac{g}{2\cw}\frac{1}{2}, \\
 a_{\PQd} &= \frac{g}{2\cw}\Big(-\frac{1}{2}+\frac{2}{3}\sw^2\Big),\quad
 b_{\PQd} = -\frac{g}{2\cw}\frac{1}{2}.
\end{align}
%
The $\PX\PW\PW$ interaction at the lowest dimension is in
general~\cite{Hagiwara:1986vm}  
%
\begin{align}
 {\mathcal L}_1^{\PW} =
    &+i\kappa_{\PVB_1}g_{\PW\PW\PZ} (\PWp_{\mu\nu} \PWm^{\mu} - \PWm_{\mu\nu} \PWp^{\mu})
 \PX_{1}^{\nu} \nonumber\\ 
  &+ i\kappa_{\PVB_2}g_{\PW\PW\PZ} \PWp_{\mu} \PWm_{\nu} \PX_{1}^{\mu\nu}   \nonumber\\
  &-\kappa_{\PVB_3} \PWp_{\mu} \PWm_{\nu}(\partial^\mu \PX_{1}^{\nu} +
 \partial^\nu \PX_{1}^{\mu})  \nonumber\\
  &+ i\kappa_{\PVB_4} \PWp_{\mu} \PWm_{\nu}\widetilde \PX_{1}^{\mu\nu}   \nonumber\\
  &- \kappa_{\PVB_5} \epsilon_{\mu\nu\rho\sigma}
  [\PWp^{\mu} ({\partial}^\rho \PWm^{\nu})-({\partial}^\rho \PWp^{\mu})\PWm^{\nu}] \PX_{1}^{\sigma},
\end{align}
%
where $g_{\PW\PW\PZ}=e\cot\theta_\rw$.
Similarly, the $\PX\PZ\PZ$ interaction is given by~\cite{Keung:2008ve}
%
\begin{align}
 {\mathcal L}_1^{\PZ} =
  &-\kappa_{\PVB_3} \PX_{1}^{\mu}({\partial}^{\nu} \PZ_{\mu})\PZ_{\nu} \\
  &-\kappa_{\PVB_5} \epsilon_{\mu\nu\rho\sigma}  \PX_{1}^{\mu}
  \PZ^{\nu} ({\partial}^\rho \PZ^{\sigma}).
\end{align}
%

For $\PX_1=1^-$ in parity-conserving scenarios:
%
\begin{align}
 \kappa_{f_a,\PVB_1,\PVB_2,\PVB_3}\ne 0.
\end{align}
%

For $\PX_1=1^+$ in parity-conserving scenarios:
%
\begin{align}
 \kappa_{f_b,V_4,V_5}\ne 0. 
\end{align}
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsubsection{Spin 2}

The spin-2 $\PX$ interaction Lagrangian starts from the dimension-five 
terms~\cite{Giudice:1998ck, Han:1998sg, Ellis:2012jv,Englert:2012xt}:
%
\begin{align}
 {\mathcal L}_2^{\Pf} = -\frac{1}{\Lambda}\sum_{f=\PQq,\ell} 
  \kappa_{f}\,T^f_{\mu\nu}\PX_2^{\mu\nu},
\end{align}
%
and
%
\begin{align}
 {\mathcal L}_2^{\PVB} = 
  &-\frac{1}{\Lambda} \kappa_{\PVB}\,T^V_{\mu\nu}\PX_2^{\mu\nu}  \nonumber\\
  &-\frac{1}{\Lambda} \kappa_{\PGg}\,T^{\PGg}_{\mu\nu}\PX_2^{\mu\nu}  \nonumber\\
  &-\frac{1}{\Lambda} \kappa_{\Pg}\,T^{\Pg}_{\mu\nu}\PX_2^{\mu\nu}, 
\end{align}
%
where $V=\PZ,\PWpm$ and 
$T^i_{\mu\nu}$ is the energy-momentum tensor of the SM fields; see 
e.g.~\cite{Hagiwara:2008jb} for the explicit forms.
The even higher dimensional terms~\cite{Bolognesi:2012mm}, dimension-seven, are
also implemented as 
%
\begin{align}
 {\mathcal L}_2^{V_{\mathrm{HD}}} = 
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\PVB_1}(\partial_{\nu}(\partial_{\mu}   (\PZ_{\rho\sigma}\PZ^{\rho\sigma}+2\PW^+_{\rho\sigma}\PW^{-\rho\sigma})))\PX_2^{\mu\nu}
 \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\PVB_2}(\partial_{\nu}(\partial_{\mu}   (\PZ_{\rho\sigma}\widetilde \PZ^{\rho\sigma}+2\PW^+_{\rho\sigma}\widetilde \PW^{-\rho
\sigma})))\PX_2^
{\mu\nu} \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\PGg_1}(\partial_{\nu}(\partial_{\
mu}
   A_{\rho\sigma}A^{\rho\sigma}))\PX_2^{\mu\nu} \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\PGg_2}(\partial_{\nu}(\partial_{\
mu}
   A_{\rho\sigma}\widetilde A^{\rho\sigma}))\PX_2^{\mu\nu} \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\Pg_1}(\partial_{\nu}(\partial_{\mu}
   G_{\rho\sigma}G^{\rho\sigma}))\PX_2^{\mu\nu} \nonumber\\
  &-\frac{1}{4}\frac{1}{\Lambda^3}\,\kappa_{\Pg_2}(\partial_{\nu}(\partial_{\mu}
   G_{\rho\sigma}\widetilde G^{\rho\sigma}))\PX_2^{\mu\nu},
\end{align}
%
where $V_{\mu\nu}=\partial_{\mu}V_{\nu}-\partial_{\nu}V_{\mu}$, etc, are the
reduced field strength tensor.


For $\PX_2=2^+$ in the RS-like graviton scenario:
%
\begin{align}
 \kappa_{f}=\kappa_{\PVB}=\kappa_{\PGg}=\kappa_{\Pg}\ne 0. 
\end{align}
%

For $\PX_2=2^+$ with the higher-dimensional operator in parity-conserving scenarios:
%
\begin{align}
 \kappa_{\PVB_1},\kappa_{\PGg_1},\kappa_{\Pg_1}\ne 0. 
\end{align}
%

For $\PX_2=2^-$ with the higher-dimensional operator in parity-conserving scenarios:
%
\begin{align}
 \kappa_{\PVB_2},\kappa_{\PGg_2},\kappa_{\Pg_2}\ne 0. 
\end{align}
%


\subsubsection{Comparison plots with JHU}

In this section we show comparison plots with JHU results in
\Bref{Bolognesi:2012mm} for $\Pp\Pp\to \PX\to VV^*\to 4\ell$. 

\subsubsubsection{Event generation}

50K events with $m_\PX=125\UGeV$ at the 8TeV-LHC were generated for each
spin state with the \textsc{MG5} (v1.5.9). Note that all the kinematical cuts for
leptons are removed.
%
%In practice we use the following \textsc{MG5} syntax for the $\PX\to \PZ\PZ$ an%alysis:
%%
%\begin{verbatim}
%import model HiggsCharac_v2.0
%generate p p > x0, x0 > mu- mu+ e- e+
%generate p p > x1, x1 > mu- mu+ e- e+
%generate p p > x2, x2 > mu- mu+ e- e+ / a QNP=1
%\end{verbatim}
%%
We note that we remove photons for diagram generation for spin-2, 
while the $\PX_0$-$\PZ$-$\PGg$ contribution for spin-0
 can be removed by setting $\kappa_{\PH\PZ\PGg}=\kappa_{\PA\PZ\PGg}=0$.
%Similarly for the $\PX\to \PW\PW$ analysis:
%%
%\begin{verbatim}
%import model HiggsCharac_v2.0
%generate p p > x0, x0 > mu- vm~ e+ ve
%generate p p > x1, x1 > mu- vm~ e+ ve
%generate p p > x2, x2 > mu- vm~ e+ ve
%\end{verbatim}
%%
We also note that the spin-2 case has seven diagrams, one double-$\PV$
resonant diagram and six single-$\PV$ diagrams including the four
point $\PX_2$-$\PV$-$\ell$-$\ell$ diagrams. Those single resonant
contributions can be removed by setting $\kappa_{\ell}=0$.


\subsubsubsection{Distributions}


The translation of the notation for the kinematical variables to JHU is
%
\begin{align}
 \phi_1\to\Phi_1,\quad \phi_1-\phi_2\to\Phi. 
\end{align}
%
Note also that the azimuthal angles are defined from 0 to $2\pi$ here,
while $-\pi$ to $\pi$ in the JHU paper. The parameter set for the JHU
comparison are listed in \Tref{tab:JHU}.

As shown in Figs.~\ref{fig:ZZ} and \ref{fig:WW}, all the distributions agree
with the JHU ones. Moreover, the lowest dimensional spin-2 is consistent
with the RS model in \textsc{MG5}.
The comparison for the higher dimensional terms for spin-2 is in progress
and will be reported elsewhere~\cite{Artoisenet:2013jma}.


\begin{table}
\begin{center}
\caption{Parameter set for the JHU comparison; see also Table I in the
 JHU implementation~\cite{Bolognesi:2012mm}.}
\label{tab:JHU}
\begin{tabular}{lc}
\hline
 JHU scenario\hspace*{5mm} & HC parameter choice\hspace*{5mm} \\
\hline
 $0^+_m$ & $\kappa_{\PH\Pg\Pg}\ne0,\ \kappa_{\SM}\ne0,\ c_{\alpha}=1$\\
 $0^+_h$ & $\kappa_{\PH\Pg\Pg}\ne0,\ \kappa_{HVV}\ne0,\ c_{\alpha}=1$\\
 $0^-$ & $\kappa_{\PA\Pg\Pg}\ne0,\ \kappa_{\PA\PVB\PVB}\ne0,\ c_{\alpha}=0$\\
 $1^+$ & $\kappa_{fu_a}=\kappa_{fu_b}=\kappa_{fd_a}=\kappa_{fd_b}\ne0,\ \kappa_{V_5}\ne0$\\
 $1^-$ & $\kappa_{fu_a}=\kappa_{fu_b}=\kappa_{fd_a}=\kappa_{fd_b}\ne0,\ \kappa_{V_3}\ne0$\\
 $2^+_m$ & $\kappa_{\Pg}\ne0,\ \kappa_{\PVB}\ne0$\\
 $2^+_h$ & $\kappa_{\Pg_1}\ne0,\ \kappa_{\PVB_1}\ne0$\\
 $2^-_h$ & $\kappa_{\Pg_2}\ne0,\ \kappa_{\PVB_2}\ne0$\\
\hline
\end{tabular}
\end{center}
\end{table}


\begin{figure}
\begin{center}
spin-0\hskip 2.5cm
spin-1\hskip 2.5cm
spin-2\\[-1.3cm] 
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_M1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_M1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_M1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_M2}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_M2}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_M2}\\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_cos}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_cos}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_cos} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_phi1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_phi1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_phi1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_cos1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_cos1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_cos1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_cos2}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_cos2}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_cos2} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0_dphi}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1_dphi}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2kl_dphi}\\
\end{center} 
\vskip -0.7cm
\caption{Distributions of the $\PX\to \PZ\PZ$ analysis; see also JHU Figs.~11
 and 12~\cite{Bolognesi:2012mm}.}
\label{fig:ZZ}
\end{figure}


\begin{figure}
\begin{center}
spin-0\hskip 2.5cm
spin-1\hskip 2.5cm
spin-2\\[-1.3cm] 
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0w_M1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1w_M1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2klw_M1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0w_cos}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1w_cos}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2klw_cos} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0w_phi1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1w_phi1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2klw_phi1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0w_cos1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1w_cos1}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2klw_cos1} \\[-1.7cm]
\hskip -3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin0w_dphi}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin1w_dphi}\hskip -0.3cm
 \includegraphics[width=0.24\textwidth]{YRHXS3_LM/Madgraph/HC_figs/spin2klw_dphi}\\
\end{center} 
\vskip -0.7cm
\caption{Distributions of the $\PX\to \PW\PW$ analysis; see also JHU Fig.~13~\cite{Bolognesi:2012mm}.}
\label{fig:WW}
\end{figure}






