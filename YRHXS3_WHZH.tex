\section{$\PW\PH$/$\PZ\PH$ production mode\footnote{%
    S.~Dittmaier, G.~Ferrera, A.~Rizzi, G.~Piacquadio (eds.);
    A.~Denner, M.~Grazzini, R.V.~Harlander, S.~Kallweit, A.~M\"uck, 
    F.~Tramontano and T.J.E.~Zirke.}}

\subsection{Theoretical developments}
\label{sec:YRHX3_WHZH_th}

In the previous two working group reports \cite{Dittmaier:2011ti} and \cite{Dittmaier:2012vm}
the state-of-the-art predictions for $\Pp\Pp\to\PW\PH/\PZ\PH$ were summarized and numerically
discussed for the total and differential cross sections, respectively, taking into account
all available higher-order corrections of the strong and electroweak interactions.
The remaining uncertainties in the cross-section predictions, originating from missing higher
orders and parametric errors in $\alphas$ and PDFs, were estimated to be smaller
than $5\%$ for integrated quantities, with somewhat larger uncertainties for differential
distributions. In the meantime the predictions have been refined and supplemented upon
including further higher-order corrections that are relevant at this level of accuracy as well.
In the following we, thus, update the cross-section predictions accordingly.

Current state-of-the-art predictions are based on the following ingredients:
\begin{itemize}
\item
QCD corrections are characterized by the similarity of $\PW\PH/\PZ\PH$ production to the
Drell--Yan process. While the NLO QCD corrections to these two process classes are
analytically identical, the NNLO QCD corrections to $\PW\PH/\PZ\PH$ production
also receive contributions that have no counterparts in Drell--Yan production.
The Drell--Yan-like contributions comprise the bulk of the QCD corrections of $\sim30\%$
and are completely known to NNLO both for 
integrated~\cite{Brein:2003wg} and 
fully differential~\cite{Ferrera:2011bk,Ferrera_ZH}
observables for the $\PW\PH$ and $\PZ\PH$ channels, where the NNLO corrections to
the differential $\PZ\PH$ cross section were not yet available in \Bref{Dittmaier:2012vm}.

QCD corrections beyond NLO that are not of Drell--Yan type are widely known only for 
total cross sections. The most prominent contribution of this kind comprises $\PZ\PH$ production
via gluon fusion which is mediated via quark loops. This part shows up first at the 
NNLO level~\cite{Brein:2003wg}
where it adds $\sim3\%(5\%)$ to the total cross section at $7\UTeV(14\UTeV)$ for $\MH=126\UGeV$
with a significant scale uncertainty of $\sim 30{-}60\%$.
This uncertainty has been reduced recently~\cite{Altenkamp:2012sx} upon adding the NLO corrections
to the $\Pg\Pg$ channel in the heavy-top-quark limit (which is the dominant contribution of NNNLO
to the $\Pp\Pp$ cross section). This contribution, which roughly doubles the impact of the $\Pg\Pg$ channel%
\footnote{The fact that this $100\%$ correction exceeds the above-mentioned scale uncertainty of
$\sim60\%$ is similar to the related well-known situation observed for $\Pg\Pg\to\PH$, where the LO scale
uncertainty underestimates the size of missing higher-order corrections as well.}
and mildly reduces its scale uncertainty to $\sim20{-}30\%$, was not yet taken into account
in the predictions documented in \Bref{Dittmaier:2011ti}, but is included in the
results on total cross sections below.

\begin{sloppypar}
Both $\PW\PH$ and $\PZ\PH$ production receive non-Drell--Yan-like corrections in the quark/antiquark-initiated
channels at the NNLO level where the Higgs boson is radiated off a top-quark loop. After the completion of 
report~\cite{Dittmaier:2011ti}, they were calculated
in \Bref{Brein:2011vx} and amount to $1{-}2\%$ for a Higgs-boson mass of $\MH\lsim150\UGeV$.
These effects are taken into account in the total-cross-section predictions below.
\end{sloppypar}

\item
Electroweak corrections, in contrast to QCD corrections, are quite different from the ones to 
Drell--Yan processes already at NLO and, in particular, distinguish between the various leptonic
decay modes of the $\PW^\pm$ and $\PZ$ bosons.
The NLO corrections to total cross sections already revealed EW effects of the size of
$-7\%(-5\%)$ for $\PW\PH$ ($\PZ\PH$) production~\cite{Ciccolini:2003jy}, 
almost independent from the collider energy for $\MH=126\UGeV$.
The NLO EW corrections to differential cross sections~\cite{Denner:2011id}, which were calculated 
with the {\HAWK} Monte Carlo program~\cite{HAWK,Ciccolini:2007jr,Ciccolini:2007ec} for the full processes
$\Pp\Pp\to\PW\PH\to\PGn_{\Pl}\Pl\PH$ and
$\Pp\Pp\to\PZ\PH\to\Pl^-\Pl^+\PH/\PGn_{\Pl}\PAGn_{\Pl}\PH$, i.e.\ including the W/Z decays,
get even more pronounced in comparison to the ones for the total cross sections.
Requiring a minimal transverse momentum of the Higgs boson of $200\UGeV$ in the so-called
``boosted-Higgs regime'' leads to EW corrections of about $-(10{-}15)\%$ with a trend of
further increasing in size at larger transverse momenta.

The EW corrections depend only very weakly on the hadronic environment, i.e.\ on the PDF choice and 
factorization scale, which suggests to include them in the form of
relative corrections factors to QCD-based predictions as detailed below.
\end{itemize}

The following numerical results are based on the same input parameters as used for the
total and differential cross sections in 
\Brefs{Dittmaier:2011ti} and \cite{Dittmaier:2012vm}, respectively, if not stated otherwise.
The same applies to the theoretical setup of the calculations, such as the EW
input parameter scheme, scale choices, etc.

\subsection{Predictions for total cross sections}
\label{sec:YRHX2_WHZH_txs}

\begin{sloppypar}
The following numerical results for the total cross sections
are obtained with the program {\sc VH@NNLO}~\cite{Brein:2012ne},
which includes the full QCD corrections up to NNLO, the NLO corrections to the $\Pg\Pg$ channel,
and the NLO EW corrections (with the latter taken from \Bref{Ciccolini:2003jy} in parametrized form).
In detail the QCD and EW corrections are combined as follows,
\begin{equation}
\sigma_{\PV\PH} = \sigma_{\PV\PH}^{\mbox{\footnotesize\sc NNLO QCD(DY)}}
\times (1 + \delta_{\PV\PH,\rm EW})
+\sigma_{\PV\PH}^{\mbox{\footnotesize\sc NNLO QCD(non-DY)}},
\label{eq:sigwhzh}
\end{equation}
i.e.\ the EW corrections are incorporated as relative correction factor to
the NNLO QCD cross section based on Drell--Yan-like corrections,
$\sigma_{\PV\PH}^{\mbox{\footnotesize\sc NNLO QCD(DY)}}$.
Electroweak corrections induced by initial-state photons, which are at the
level of $1\%$ (see below), are not included here.


\Trefs{tab:YRHXS3_WHZH_wh71}--\ref{tab:YRHXS3_WHZH_wh76} and
\ref{tab:YRHXS3_WHZH_wh81}--\ref{tab:YRHXS3_WHZH_wh86} display numerical
values for the $\PW\PH$ production cross section as evaluated according
to (\ref{eq:sigwhzh}).  Note that the cross sections for $\PW^+\PH$ and
$\PW^-\PH$ production are added here. The scale uncertainty is obtained
by varying the renormalization and the factorization scale independently
within the interval $[Q/3,3Q]$, where $Q\equiv\sqrt{Q^2}$ is the
invariant mass of the $\PV\PH$ system. The PDF uncertainties are
calculated by following the PDF4LHC recipe, using
MSTW2008~\cite{Martin:2009iq}, CT10~\cite{Lai:2010vv}, and
NNPDF2.3~\cite{Ball:2012cx}; the total uncertainties are just the
linear sum of the PDF and the scale uncertainties.

Similarly, \Trefs{tab:YRHXS3_WHZH_zh71}--\ref{tab:YRHXS3_WHZH_zh76}
and \ref{tab:YRHXS3_WHZH_zh81}--\ref{tab:YRHXS3_WHZH_zh86}
show up-to-date results for $\PZ\PH$ production. The gluon-fusion
channel $\sigma_{\Pg\Pg\to \PZ\PH}$ is listed separately in the last
column. It is obtained by calculating the radiative correction factor of
this channel through order $\alphas^3$ in the heavy-top limit, and
multiplying it with the exact LO result, as described in
\Bref{Altenkamp:2012sx}.  The scale uncertainty of $\sigma_{\Pg\Pg\to
  \PZ\PH}$ is obtained by varying the renormalization and the
factorization scales of the NLO term simultaneously by a factor of three
around $\sqrt{Q^2}$. The PDF uncertainty of $\sigma_{\Pg\Pg\to \PZ\PH}$
is evaluated only at LO, and its total uncertainty is simply the sum of
the scale and the PDF uncertainty.  The uncertainties arising from all
terms except for gluon fusion are obtained in analogy to the $\PW\PH$
process, see above. 
%they are listed in columns ``Scale'' and ``PDF''.
%Adding these linearly to the gluon fusion uncertainties results in column ``Total''.
The sum of all scale and PDF uncertainties are listed in columns
``Scale'' and ``PDF''. Adding them linearly results in column ``Total''. 
The second columns in
\Trefs{tab:YRHXS3_WHZH_zh71}--\ref{tab:YRHXS3_WHZH_zh76} and
\ref{tab:YRHXS3_WHZH_zh81}--\ref{tab:YRHXS3_WHZH_zh86}
contain the cross section including all
available radiative corrections.


Note that the uncertainties are symmetrized around the central values
which in turn are obtained with the MSTW2008 PDF set and by setting the
central renormalization and the factorization scales equal to
$Q$, the invariant mass of the $\PV\PH$ system.

\end{sloppypar}






\subsection{Predictions for differential cross sections}
\label{sec:YRHX2_WHZH_dxs}

We first briefly recall the salient features in the definition of the cross sections
with leptonic $\PW/\PZ$ decays. A detailed description can be found in Section~7.2
of \Bref{Dittmaier:2012vm}.
All results are given for a specific leptonic decay mode without
summation over lepton generations. For charged leptons $\Pl$ in the final state
we distinguish two different treatments of photons that are collinear to those leptons.
While the ``bare'' setup assumes perfect isolation of photons and leptons, which is reasonable
only for muons, in the ``rec'' setup we mimic electromagnetic
showers in the detector upon recombining photons and leptons to ``quasi-leptons'' for
$R_{\Pl\PGg}<0.1$, where $R_{\Pl\PGg}$ is the usual distance in the plane spanned by
rapidity and the azimuthal angle in the transverse plane. 
After the eventual recombination procedure the following cuts are applied if not stated otherwise,
\begin{eqnarray}
&& p_{\mathrm{T},\Pl} > 20\UGeV, \qquad
|y_{\Pl}|< 2.5, \qquad
p_{\mathrm{T,miss}} > 25\UGeV, \\
&& p_{\mathrm{T},\PH} > 200\UGeV, \qquad
p_{\mathrm{T},\PW/\PZ} > 190\UGeV,
\label{eq:VHpTcuts}
\end{eqnarray}
where $p_{\mathrm{T}}$ is the transverse momentum of the respective particle and
$p_{\mathrm{T,miss}}$ the total transverse momentum of the involved neutrinos.

Similar to the procedure for the total cross section, QCD-based predictions are dressed
with relative EW correction factors,
\begin{equation}
\sigma=\sigma^{\mbox{\footnotesize\sc NNLO QCD(DY)}}\times
\left( 1 + \delta_{\mathrm{EW}}^\mathrm{bare/rec} \right)
+ \sigma_\gamma\, ,
\end{equation}
where $\sigma^{\mbox{\footnotesize\sc NNLO QCD(DY)}}$ is the NNLO QCD cross-section prediction of 
\Brefs{Ferrera:2011bk,Ferrera_ZH} and 
$\delta_{\mathrm{EW}}^\mathrm{bare/rec}$ the EW correction factor obtained 
with {\HAWK}~\cite{HAWK,Ciccolini:2007jr,Ciccolini:2007ec,Denner:2011id}.
Note that the relative EW correction is not included on an event-by-event basis during the 
phase-space integration, but used as reweighting factor in the histograms bin by bin.
The contribution $\sigma_\gamma$, which is induced by processes with photons
in the initial state, also delivered by {\HAWK}, is found to be at the level
of $1\%$ (see \Brefs{Denner:2011id,Dittmaier:2012vm}).
All cross-section predictions of this section are based on the
MSTW2008 NNLO PDF set~\cite{Martin:2009iq}, but the EW correction factor
hardly depends on the PDF choice.
We recall that the non-Drell--Yan-like corrections, which are included in the predictions
for total cross sections (see previous section), are not (yet) available for differential quantities.

Figure~\ref{fig:WHZH-dxs-abs} shows the
distributions for the various $\PV\PH$ production channels at the LHC with a CM
energy of $8\UTeV$
for a Higgs-boson mass of $\MH=126\UGeV$ in the boosted-Higgs scenario, where the cuts
(\ref{eq:VHpTcuts}) on the Higgs and $\PW/\PZ$ transverse momenta
are applied. The only differences to the results
shown in Fig.~56 of \Bref{Dittmaier:2012vm} concerns the new value of $\MH$
and the transition from NLO QCD to NNLO QCD for $\PZ\PH$ production.
Qualitatively the results look very similar, so that the discussion 
presented in \Bref{Dittmaier:2012vm} still holds.
This applies, in particular, to the
respective EW corrections which are depicted in \refF{fig:WHZH-dxs-ew} for
the ``bare'' and ``rec'' treatments of radiated photons. 
The smallness of the difference between the two variants, which is about
$1{-}3\%$, shows that the bulk of the EW corrections, which are typically 
$-(10{-}15)\%$, is of pure weak origin.
%
\begin{figure}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_b126_8T_abs_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_b126_8T_abs_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_b126_8T_abs_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_b126_8T_abs_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlpm_plot_b126_8T_abs_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlp_plot_b126_8T_abs_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_b126_8T_abs_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_b126_8T_abs_Z_YR3.eps}
\vspace{0.5cm}
\caption{
Predictions for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for boosted Higgs bosons at
the $8\UTeV$ LHC for $\MH=126\UGeV$. 
}
\label{fig:WHZH-dxs-abs}
\end{figure}%
%
\begin{figure}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_b126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_b126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_b126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_b126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlpm_plot_b126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlp_plot_b126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_b126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_b126_8T_rel_Z_YR3.eps}
\vspace*{1.5cm}
\caption{
Relative EW corrections for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for boosted Higgs bosons at
the $8\UTeV$ LHC for $\MH=126\UGeV$.
}
\label{fig:WHZH-dxs-ew}
\end{figure}%
%
While the EW corrections to rapidity distributions are flat and resemble the 
ones to the respective integrated cross sections, the corrections to $\pT$
distributions show the typical tendency to larger negative values with
increasing $\pT$ (weak Sudakov logarithms).
Finally, for comparison we show the EW corrections in the boosted-Higgs regime,
where the transverse momenta of the Higgs and $\PW/\PZ$ bosons are
$\gsim200\UGeV$, to the scenario of \refF{fig:WHZH-dxs-ew2}
where only basic isolation cuts are kept,
i.e.\ the cuts (\ref{eq:VHpTcuts}) on $p_{\mathrm{T},\PH}$ and $p_{\mathrm{T},\PW/\PZ}$ are dropped.
%
\begin{figure}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_i126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/pth_plot_i126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_i126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptV_plot_i126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlpm_plot_i126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/ptlp_plot_i126_8T_rel_Z_YR3.eps}
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_i126_8T_rel_W_YR3.eps}
\hfill
\includegraphics[width=7.5cm]{YRHXS3_WHZH/yh_plot_i126_8T_rel_Z_YR3.eps}
\vspace*{1.5cm}
\caption{
Relative EW corrections for the
$p_{\mathrm{T},\PH}$, $p_{\mathrm{T},\PV}$,
$p_{\mathrm{T},\Pl}$, and $y_{\PH}$ distributions (top to bottom)
for Higgs strahlung off \PW\ bosons (left) and \PZ\ bosons (right)
for the basic cuts at the $8\UTeV$ LHC for $\MH=126\UGeV$.
}
\label{fig:WHZH-dxs-ew2}
\end{figure}%
%
As already noted in \Bref{Dittmaier:2012vm},
switching from the basic cuts to the boosted-Higgs scenario
increases the size of the EW corrections by about $5\%$ in the negative 
direction. 

In spite of the theoretical improvement by the transition from NLO QCD to
NNLO QCD in the $\PZ\PH$ channel, the estimate of the relative uncertainties
shown in Table~19 of \Bref{Dittmaier:2012vm} remains valid, because in
the predictions for the differential cross sections the contribution of the
$\Pg\Pg\to\PZ\PH$ channel are not (yet) included. The change from $\MH=120\UGeV$
to $\MH=126\UGeV$ leaves the error estimate untouched as well.


