LATEX    = latex
BIBTEX   = bibtex
DVIPS    = dvips

BASENAME = YRHXS3

default: testlatex

testlatex:
	latex  ${BASENAME}
	latex  ${BASENAME}
	bibtex ${BASENAME}
	latex  ${BASENAME}
	latex  ${BASENAME}
# - convert dvi file into ps and pdf files
	dvips -j0 ${BASENAME}.dvi -o ${BASENAME}.ps
	ps2pdf13 -sPAPERSIZE=a4 ${BASENAME}.ps
#	dvipdf -sPAPERSIZE=a4 -dPDFSETTINGS=/prepress ${BASENAME}
#	dvipdf -sPAPERSIZE=letter -dPDFSETTINGS=/prepress ${BASENAME}
# - directly conert dvi file to pdf file (necessary when using figure in pdf format)
#	equivalently dvipdfm, dvipdfmx or xdvipdfmx should work 
#	dvipdfmx -p a4 ${BASENAME}.dvi
#	xdvipdfmx -p a4 ${BASENAME}.dvi
#	xdvipdfmx -p letter ${BASENAME}.dvi 
#
# - commit LaTeX outputs to SVN
	svn ci -m "update YRHXS3.pdf" YRHXS3.pdf
	echo "Successfully committed LaTeX outputs to SVN."

testpdflatex:
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}
	bibtex    ${BASENAME}
	pdflatex  ${BASENAME}
	pdflatex  ${BASENAME}

#
# standard Latex targets
#

%.dvi:	%.tex 
	$(LATEX) $<

%.bbl:	%.tex *.bib
	$(LATEX) $*
	$(BIBTEX) $*

%.ps:	%.dvi
	$(DVIPS) $< -o $@

%.pdf:	%.tex
	$(PDFLATEX) $<

.PHONY: clean

clean:
#	rm -f *.aux *.log *.bbl *.blg *.brf *.cb *.ind *.idx *.ilg *.inx *.dvi *.toc *.out *~ ~* spellTmp 
	rm -f *.pdf *.ps *.dvi *.bbl *.blg *.log *.toc *.aux *.brf *.cb *.ind *.idx *.ilg *.inx *.out *~ ~* spellTmp 

