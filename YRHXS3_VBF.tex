\providecommand{\muR}{\mathswitch {\mu_{\mathrm{R}}}}
\providecommand{\muF}{\mathswitch {\mu_{\mathrm{F}}}}
\providecommand\HAWK{{\sc HAWK}}
\providecommand\VBFNLO{{\sc VBFNLO}}
\providecommand\POWHEG{{\sc POWHEG}}
\providecommand\POWHEGBOX{{\sc POWHEG BOX}}
\providecommand\HERWIG{{\sc HERWIG}}
\providecommand\PYTHIA{{\sc PYTHIA}}
\providecommand{\kT}{\ensuremath{k\sb{\scriptstyle\mathrm{T}}}}

\section{VBF production mode\footnote{%
    A.~Denner, P.~Govoni, C.~Oleari, D.~Rebuzzi (eds.);
    P.~Bolzoni, S.~Dittmaier, S.~Frixione,
         %B.~Quayle, 
    F.~Maltoni, C.~Mariotti, \mbox{S.-O.~Moch},
    A.~M\"uck, P.~Nason, 
         % G.~Passarino, 
    M.~Rauch, 
         % R.~Tanaka, 
    P.~Torrielli, 
         %G.~Steele,
    M.~Zaro.}} 
\label{sec:VBF}

\subsection{Programs and Tools} 
\label{sec:programs-sub}

\subsubsection{HAWK}
\label{sec:HAWK-sub-sub}

\HAWK{} is a parton-level event generator for Higgs production in
vector-boson fusion~\cite{Ciccolini:2007jr, Ciccolini:2007ec,HAWK},
$\Pp\Pp\to\PH jj$, and Higgs-strahlung \cite{Denner:2011id},
$\Pp\Pp\to\PW\PH\to\PGn_{\Pl}\,\Pl\,\PH$ and
$\Pp\Pp\to\PZ\PH\to\Pl^-\Pl^+\PH/\PGn_{\Pl}\PAGn_{\Pl}\PH$. Here we
summarize its most important features for the VBF channel. \HAWK{}
includes the complete NLO QCD and EW corrections and all weak-boson
fusion and quark--antiquark annihilation diagrams, i.e.~$t$-channel
and $u$-channel diagrams with VBF-like vector-boson exchange and
$s$-channel Higgs-strahlung diagrams with hadronic weak-boson decay,
as well as all interferences. External fermion masses are neglected
and the renormalization and factorization scales are set to $\MW$ by
default. Recently also anomalous Higgs-boson--vector-boson couplings
have been implemented.

For the results presented below, $s$-channel contributions have been
switched off.  These contributions are at the level of a few per mille
once VBF cuts are applied \cite{Ciccolini:2007ec}. Interferences
between $t$- and $u$-channel diagrams are included at LO and NLO,
while contributions of $\PQb$-quark parton distribution functions
(PDFs) and final-state $\PQb$ quarks, which can amount to about $2\%$,
are taken into account at LO.  Contributions from photon-induced
processes, which are part of the real EW corrections and at the level
of $1{-}2\%$, can be calculated by HAWK as well, but have not been
included, since photon PDFs are not supported by the PDF sets used.
\HAWK{} allows for an on-shell Higgs boson or for an off-shell Higgs
boson (with optional decay into a pair of gauge singlets). For an
off-shell Higgs boson, besides the fixed-width scheme [see
\Eref{eq:VBF_BW_PWG_fix}] the complex-pole scheme (CPS)
\cite{Passarino:2010qk,Goria:2011wa,Anastasiou:2011pi} is supported
with a Higgs propagator of the form
\begin{equation} 
\label{eq:VBF_BW}
\frac{1}{\pi} \frac{M \, \GH(M)} {\left(M^2 -
\MH^2\right)^2 + ( \MH\GH )^2 },
\end{equation} 
where $\MH$ and $\GH$ are the Higgs mass and width in the complex-mass
scheme and $\GH(M)$ is the Higgs width for the off-shell mass $M$ as taken
from the tables in \Bref{Dittmaier:2011ti}.


\subsubsection{VBFNLO}
\label{sec:VBFNLO-sub-sub}

\VBFNLO{} \cite{Arnold:2012xn,Arnold:2011wj,Arnold:2008rz} is a
parton-level Monte Carlo event generator which can simulate vector-boson
fusion, double and triple vector-boson production in hadronic collisions
at next-to-leading order in the strong coupling constant, as well as
Higgs-boson plus two jets and double vector-boson production via gluon
fusion at the one-loop level. 
For Higgs-boson production via VBF, both the NLO QCD and EW
corrections to the $t$-channel can be included in the SM (and also in
the (complex) MSSM), and the NLO QCD corrections are included for
anomalous couplings between the Higgs and a pair of vector bosons.

\VBFNLO{} can also simulate the Higgs decays $\PH \rightarrow
\gamma\gamma, \PGmp \PGmm, \PGtp \PGtm, \PQb \overline{\PQb}$ in the
narrow-width approximation, either calculating the appropriate branching
ratios internally at LO (or, in the case of the MSSM not considered in
this section, taking them from an input SLHA file).
The Higgs-boson decays $\PH \rightarrow \PW^{+} \PW^{-} \rightarrow
\Pl^{+} \PGnl \Pl^{-} \PAGn_{\Pl}$ and $\PH \rightarrow \PZ\PZ
\rightarrow \Pl^{+} \Pl^{-} \Pl^{+} \Pl^{-}, \Pl^{+} \Pl^{-} \PGnl
\overline{\PGnl}$ are calculated using a Breit--Wigner distribution for
the Higgs boson and the full LO matrix element for the $\PH \rightarrow
4f$ decay. Initial- and final-state $\PQb$ quarks can be included at NLO
QCD for the neutral-current diagrams, where no external top quarks
appear. For PDF sets which support photon PDFs, their effects are
automatically included as well. As the used PDF sets do not include
photon PDFs, this does however not play any role here.
Interference effects between VBF-Higgs and continuum production in
leptonic or semi-leptonic $\PW^{+} \PW^{-}$ or $\PZ\PZ$ VBF processes
are contained in VBFNLO as part of the di-boson VBF processes.

For the results presented here, a modified version of \VBFNLO{} was used,
that simulates off-shell Higgs boson using the complex-pole
scheme (see Eq.~\ref{eq:VBF_BW}).

\subsubsection{POWHEG}
\label{sec:POWHEG-sub-sub}
The \POWHEG{} method is a prescription for interfacing NLO calculations with
parton-shower generators, like \HERWIG{} and \PYTHIA{}. It was first
presented in~\Bref{Nason:2004rx} and was described in great detail
in~\Bref{Frixione:2007vw}.  In~\Bref{Nason:2009ai}, Higgs-boson production in
VBF has been implemented in the framework of
the \POWHEGBOX{}~\cite{Alioli:2010xd}, a computer framework that implements
in practice the theoretical construction of~\Bref{Frixione:2007vw}.

All the details of the implementation can be found
in~\Bref{Nason:2009ai}. Here we briefly recall that, in the
calculation of the partonic matrix elements, all partons have been
treated as massless.  This gives rise to a different treatment of
quark flavors for diagrams where a $\PZ$ boson or a $\PW$ boson is
exchanged in the $t$-channel.  In fact, for all $\PZ$-exchange
contributions, the $\PQb$ quark is included as an initial and/or
final-state massless parton.  For the (dominant) $\PW$-exchange
contributions, no initial $\PQb$ quark has been considered, since it
would have produced mostly a $\PQt$ quark in the final state, which would
have been misleadingly treated as massless.  The
 Cabibbo--Kobayashi--Maskawa~(CKM) matrix $V_{\rm \scriptscriptstyle
  CKM}$ elements can be assigned by the user. The default setting used to
  compute the results in this section is
\begin{equation}
\begin{array}{c}
\\
V_{\rm  \scriptscriptstyle CKM}=
\end{array}
\begin{array}{c c}
& \PQd\quad\quad\quad \ \PQs\ \quad \quad\quad \PQb\ \\
\begin{array}{c}
\PQu\\
\PQc\\
\PQt
\end{array} 
&
\left(
\begin{array}{c c c}
$0.9748$ & $0.2225$ & $0.0036$\\
$0.2225$ & $0.9740$ & $0.041$ \\
$0.009$ & $0.0405$ & $0.9992$
\end{array}
\right).
\end{array}
\end{equation}
We point out that, as long as no final-state hadronic flavor is
tagged, this is practically equivalent to the result obtained using
the identity matrix, due to unitarity.

The Higgs-boson virtuality $M^2$ can be generated according to three
different schemes:
\begin{enumerate}
\item the fixed-width scheme
\begin{equation}
\label{eq:VBF_BW_PWG_fix}
\frac{1}{\pi} \frac{\MH \GH}{\left(M^2 - \MH^2\right)^2 + \MH^2 \GH^2  }\,,
\end{equation}
 with fixed decay  width $\GH$,
\item the running-width scheme
\begin{equation}
\label{eq:VBF_BW_PWG_run}
\frac{1}{\pi} \frac{M^2 \,\GH
  / \MH}{\left(M^2 - \MH^2\right)^2 + ( M^2\, \GH / \MH)^2 }\,,
\end{equation}

\item the complex-pole scheme, as given in \Eref{eq:VBF_BW}.

\end{enumerate}
As a final remark, we recall that the renormalization $\muR$ and
factorization $\muF$ scales used in the calculation of the \POWHEG{}
$\bar{B}$ function (i.e.~the function that is used to generate the underlying
Born variables to which the \POWHEGBOX{} attaches the radiation ones) are
arbitrary and can be set by the user.  For the results in this section, they
have been taken equal to $\MW$.  The renormalization scale for the evaluation
of the strong coupling associated to the radiated parton is set to its
transverse momentum, as the \POWHEG{} method requires. The transverse
momentum of the radiated parton is taken, in the case of initial-state
radiation, as exactly equal to the transverse momentum of the parton with
respect to the beam axis. For final-state radiation one takes instead
\begin{equation}
\pT^2=2E^2(1-\cos\theta),
\end{equation}
where $E$ is the energy of the radiated parton and $\theta$ the angle it
forms with respect to the final-state parton that has emitted it, both taken
in the partonic centre-of-mass frame.  


\subsubsection{VBF@NNLO}
\label{sec:VBFNNLO-sub-sub}

{\sc VBF@NNLO}~\cite{Bolzoni:2010xr,Bolzoni:2011cu} computes VBF total Higgs cross sections 
at LO, NLO, and NNLO in QCD via the structure-function approach.  This
approach~\cite{Han:1992hr} consists  in considering VBF process as a
double deep-inelastic scattering (DIS) attached to the colorless pure
electroweak vector-boson fusion into a Higgs boson.  According to this
approach one can include NLO QCD corrections to the VBF process employing the
standard DIS structure functions $F_i(x,Q^2);\,i=1,2,3$ at
NLO~\cite{Bardeen:1978yd} or similarly the corresponding structure functions 
at NNLO~\cite{Kazakov:1990fu,Zijlstra:1992kj,Zijlstra:1992qd,Moch:1999eb}.

The effective factorization underlying the structure-function approach does not include all types of contributions.
At LO an additional contribution arises from the
interferences between identical final-state quarks (e.g.,\
$\PQu\PQu\rightarrow \PH\PQu\PQu$) or between processes where either a $\PW$
or a $\PZ$ can be exchanged (e.g.,\ $\PQu\PQd\rightarrow \PH\PQu\PQd$).  These
LO contributions have been added to the NNLO results presented here, even if they are very small. 
Apart from such contributions, the structure-function approach is exact also at NLO.  
At NNLO, however, several types of diagrams violate the underlying factorization.
Their impact on the total rate has been computed or estimated in~\Bref{Bolzoni:2011cu} and found to be negligible.
Some of them are color suppressed and kinematically
suppressed~\cite{vanNeerven:1984ak,Blumlein:1992eh,Figy:2007kv}, others have
been shown in \Bref{Harlander:2008xn} to be small enough not to produce
a significant deterioration of the VBF signal.

At NNLO QCD, the theoretical QCD uncertainty is reduced to less than
$2\%$. Electroweak corrections, which are at the level of $5\%$, are
not included in {\sc VBF@NNLO}.
%
The Higgs boson can either be produced on its mass-shell, or off-shell effects can be included in the 
complex-pole scheme.


\subsubsection{\textsc{aMC@NLO}}
\label{sec:aMCNLO-sub-sub}

The \textsc{aMC@NLO} generator \cite{Alwall:2013xx,website} is a program that
implements the matching of a generic NLO QCD computation to parton-shower
simulations according to the MC@NLO formalism \cite{Frixione:2002ik}; its
defining feature is that all ingredients of such matching and computation are
fully automated.  The program is developed within the \textsc{MadGraph5}
\cite{Alwall:2011uj} framework and, as such, it does not necessitate of any
coding by the user, the specification of the process and of its basic physics
features (such as particle masses or phase-space cuts) being the only external
informations required: the relevant computer codes are then generated
on-the-fly, and the only practical limitation is represented by CPU
availability.

\textsc{aMC@NLO} is based on different building blocks, each devoted to the
generation and evaluation of a specific contribution to an NLO-matched
computation. \textsc{MadFKS} \cite{Frederix:2009yq} deals with the Born and
real-emission terms, and in particular it performs in a general way the
analytical subtraction of the infrared singularities that appear in the
latter matrix elements according to the FKS prescription
\cite{Frixione:1995ms,Frixione:1997np}; moreover, it is also responsible for
the process-independent generation of the so-called Monte Carlo subtraction
terms, namely the contributions ensuring the avoidance of double-counting in
the MC@NLO cross sections; \textsc{MadLoop} \cite{Hirschi:2011pa} computes
the finite part of the virtual contributions, based on the OPP
\cite{Ossola:2006us} one-loop integrand-reduction method and on its
implementation in \textsc{CutTools} \cite{Ossola:2007ax}.

The first applications of \textsc{aMC@NLO}\footnote{These results were still
based on version 4 of \textsc{MadGraph} \cite{Alwall:2007st}.} have been
presented in \Brefs{Frederix:2011zi,Frederix:2011qg,Frederix:2011ss,
Frederix:2011ig,Frederix:2012dh,Frederix:2012dp},
and a novel technique for merging different multiplicities of NLO-matched
samples has been developed in the \textsc{aMC@NLO} environment and documented
in \Bref{Frederix:2012ps}.

As the MC@NLO method is Monte-Carlo dependent (through the Monte Carlo
subtraction terms), a different subtraction has to be performed for each
parton shower one wants to interface a computation to. So far this has been
achieved in \textsc{aMC@NLO} for the \textsc{HERWIG6} and \textsc{HERWIG++}
event generators (which amounts to the automation of the implementations
detailed in \Brefs{Frixione:2002ik,Frixione:2010ra}, respectively), 
and for the virtuality-ordered version of \textsc{PYTHIA6} (the proof of
concept of which was given in \Bref{Torrielli:2010aw}). The present
publication is the first in which \textsc{aMC@NLO} results are presented for
\textsc{PYTHIA6} in a process involving final-state radiation, and for
\textsc{HERWIG++}.

In the \textsc{aMC@NLO} framework, $t$-channel VBF can be generated with NLO 
accuracy in QCD.
%with the following commands in the \textsc{MadGraph5} command 
%interface:
%\begin{verbatim}
% import model loop_sm-no_b_mass
% define p = u u~ d d~ c c~ s s~ b b~ g
% define j = p
% generate p p > h j j $$ w+ w- z [QCD]
%\end{verbatim}
%where the first three lines are to import the NLO model with massless
%$\PQb$ quark and to include the latter in the definition of proton and jet,
%respectively. The '\texttt{\$\$}' sign forbids diagrams with either a
%W or a Z boson in the $s$-channel.  
A $V_{\tiny{\mbox{CKM}}}=1$ is
employed, as there is no flavor tagging in our analysis.  This
computation includes all interferences between $t$- and $u$-channel
diagrams, such as those occurring for same-flavor quark scattering
and for partonic channels that can be obtained by the exchange of
either a Z or a W boson (e.g. \texttt{u d > h u d}).  Only vertex
loop-corrections are included (the omitted loops are however totally
negligible \cite{Ciccolini:2007ec}).  The Les-Houches parton-level
event file thus generated also contains the necessary information for
the computation of scale and PDF uncertainties without the need of
extra runs \cite{Frederix:2011ss}.  The Higgs boson is considered as
stable.

 
\subsection{VBF Parameters and Cuts}
\label{sec:cuts-sub}

The numerical results presented in \refS{sec:results-sub} have been
computed using the values of the EW parameters given in Appendix A of
\Bref{Dittmaier:2011ti}. The electromagnetic coupling is fixed in the
$\GF$ scheme, to be
\beq
 \alpha_{\GF} = \sqrt{2}\GF\MW^2(1-\MW^2/\MZ^2)/\pi = 1/132.4528\ldots\,.
\eeq
The weak mixing angle is defined in the on-shell scheme,
\begin{equation}
\sin^2\theta_{\mathrm{w}}=1-\MW^2/\MZ^2=0.222645\ldots \,.
\end{equation}
The renormalization and factorization scales are set equal to the
$\PW$-boson mass,
\begin{equation}
\label{eq:VBF_ren_fac_scales}
\muR = \muF= \MW,
\end{equation}
and both scales are varied independently in the range $\MW/2 < \mu < 2\MW$.

In the calculation of the inclusive cross sections, we have used the
MSTW2008~\cite{Martin:2009iq}, CT10 \cite{Lai:2010vv}, and NNPDF2.1
\cite{Cerutti:2011au} PDFs, for the calculation of the differential
distributions we employed MSTW2008nlo PDFs~\cite{Martin:2009iq}.

Contributions from $s$-channel diagrams and corresponding
interferences have been neglected throughout.

For the differential distributions presented in
\refS{sec:differential-sub-sub} the following reconstruction scheme
and cuts have been applied.
Jets are constructed according to the anti-$\kT$ algorithm, with
the rapidity--azimuthal angle separation $\Delta R=0.5$, using the default
recombination scheme ($E$ scheme).  Jets are subject to the
cuts
\begin{equation}
\label{eq:VBF_cuts1}
{\pT}_j > 20\UGeV, \qquad  |y_j| < 4.5\,,
\end{equation}
where $y_j$ denotes the rapidity of the (massive) jet.
% of pseudorapidity $|\eta|<5$. 
Jets are ordered according to their $\pT$ in decreasing
progression.  The jet with highest $\pT$ is called leading jet, the
one with next highest $\pT$ subleading jet, and both are the tagging
jets.  Only events with at least two jets are kept.  They must satisfy
the additional constraints
%
\begin{equation}
\label{eq:VBF_cuts2}
|y_{j_1} - y_{j_2}| > 4\,, \qquad m_{jj} > 600\UGeV.
\end{equation}
%The Higgs boson is generated off shell
%according to \Erefs{eq:VBF_BW} or~\refE{eq:VBF_BW_PWG}, 
%and there are no cuts applied to its decay products.

%For the calculation of EW corrections, real photons are recombined
%with jets according to the same  anti-$\kT$ algorithm as used
%for jet recombination.



\subsection{Results}
\label{sec:results-sub}


\subsubsection{Inclusive Cross Sections with CPS}
\label{sec:inclusive-sub-sub}

In the following we present results for VBF inclusive cross sections
at $7\UTeV$ (for Higgs-boson masses below $300\UGeV$) and $8\UTeV$ (over the full Higgs-boson mass range)
calculated in the CPS, as described above. 

Tables \ref{tab:YRHXS3_7VBF_NNLO1}--\ref{tab:YRHXS3_7VBF_NNLO7} list the
VBF inclusive cross sections at $7\UTeV$ as a function of the
Higgs-boson mass, while Tables \ref{tab:YRHXS3_VBF_NNLO1}--\ref{tab:YRHXS3_VBF_NNLO7}
display results at $8\UTeV$.
The pure NNLO QCD results (second column), obtained
with {\sc VBF@NNLO}, the relative NLO EW corrections (fourth column),
obtained with {\HAWK}, and the combination of NNLO QCD and NLO EW
corrections (third column) are given, together with uncertainties from
PDF + $\Oas$ and from QCD scale variations.  The combination has been
performed under the assumption that QCD and EW corrections factorize
completely, i.e.\
\begin{equation}
\sigma^{\mathrm{NNLO}+\mathrm{EW}} =
\sigma^{\mathrm{NNLO}}_{\mathrm{VBF@NNLO}}
\times (1 + \delta^{\mathrm{EW}}_{\mathrm{HAWK}})\,,
\end{equation} 
where $\sigma^{\mathrm{NNLO}}_{\mathrm{VBF@NNLO}}$ is the NNLO QCD result and
$\delta^{\mathrm{EW}}_{\mathrm{HAWK}}$ the relative EW correction determined in the limit
$\alphas=0$.

Figure \ref{fig:YRHXS3_VBF_xsec} summarizes the VBF results at
$8\UTeV$ for the full and for the low mass range. The inclusive cross
section including the combined NNLO QCD and NLO EW corrections are
shown as a function of the Higgs-boson mass. The band represents the
PDF + $\Oas$ uncertainties, calculated from the envelope of the three
PDF sets, CT10, MSTW2008, and NNPDF2.1, according to the PDF4LHC
prescription.

\begin{figure}
\includegraphics[width=0.5\textwidth]{YRHXS3_VBF/YRHXS3_VBF.8TeV_cpx.eps}
\includegraphics[width=0.5\textwidth]{YRHXS3_VBF/YRHXS3_VBF.8TeV_cpx_vlm.eps}
%\vspace*{-0.3cm}
\caption{VBF inclusive cross sections at the LHC at $8\UTeV$ for the
  full (left) and low (right) Higgs-boson mass 
  range. The uncertainty band represents the PDF + $\Oas$ envelope
  estimated according to the PDF4LHC prescription \cite{Botje:2011sn}.}
\label{fig:YRHXS3_VBF_xsec}
\end{figure}

In \refT{tab:YRHXS3_VBF_NNLO8_13} we compare the predictions for the
Higgs cross section in VBF from different calculations at NLO QCD and
including EW corrections at $8$ and $13\UTeV$.  Using MSTW2008 PDFs,
results are presented at NLO from VBF@NNLO, HAWK, and VBFNLO.  The NLO
QCD corrections agree within 0.6\% between HAWK and VBF@NLO.  The NLO
EW corrections amount to $-5\%$ while the NNLO QCD corrections are
below 0.3\%.

All results are obtained in the CPS. Note that for Higgs-boson masses
in the range $120{-}130\UGeV$ the cross section calculated in the
complex-pole scheme is larger than the one for an on-shell Higgs boson
by $\sim1.4\%$ at $8\UTeV$ and $\sim2.2\%$ at $13\UTeV$. The
presented results in the CPS assume that the Higgs distribution is
integrated over all kinematically allowed invariant Higgs masses. If
cuts on the invariant mass are applied, the cross section decreases
down to the on-shell value.


%In Tables \ref{tab:YRHXS3_VBF_NNLO7}--\ref{tab:YRHXS3_VBF_NNLO12}
%comparisons among different NLO computations are given for $8\UTeV$. HAWK
%results are compared to VBFNLO and VBF@NNLO ones for the pure QCD part
%and for NLO QCD + EW corrections.


\subsubsection{Differential Distributions}
\label{sec:differential-sub-sub}

In this section we present results relevant to the production
of a $125\UGeV$ Standard Model Higgs boson through a VBF mechanism
at the $8\UTeV$ LHC. These have been obtained with
\textsc{aMC@NLO}~\cite{Frixione:2013mta} and 
\textsc{POWHEG}~\cite{Nason:2009ai,Alioli:2010xd}. As \textsc{aMC@NLO} and
\textsc{POWHEG} perform the NLO matching with parton showers by means of two
different prescriptions (see~\Brefs{Frixione:2002ik}
and~\cite{Nason:2004rx,Frixione:2007vw}, respectively), the comparison
of the two allows one to assess the matching systematics that affects
VBF Higgs production; it also constitutes a non-trivial validation 
of the public, fully-automated code \textsc{aMC@NLO}.
Our predictions are
obtained with the MSTW2008nlo68cl PDF set~\cite{Martin:2009iq}, and by 
setting the
renormalization and factorization scales equal to the $\PW$-boson mass $\MW$. Matching
with \textsc{HERWIG6}~\cite{Corcella:2000bw}, virtuality-ordered
\textsc{PYTHIA6}~\cite{Sjostrand:2006za}, and
\textsc{HERWIG++}~\cite{Bahr:2008pv} has been considered in both schemes with
default settings for such event generators; the only
exception is for \textsc{aMC@NLO+PYTHIA6}, where \texttt{PARP(67)} and
\texttt{PARP(71)} have been set to \texttt{1D0}.
No simulation of the underlying event has been performed. 

%In order to improve numerical stability, \textsc{aMC@NLO} events have been generated 
%requiring at least two jets with transverse momentum larger than $2\UGeV$ 

%\footnote{These technical generation cuts have been checked not to bias in any 
%way the results shown in the following.}. Moreover, the \textsc{aMC@NLO} default 
%behavior has been slightly modified in order to integrate the finite part of the 
%virtual corrections together with the rest of the cross section: since VBF virtuals 
%are relatively fast to evaluate, this has the advantage of reducing the amount of 
%negative weights without sizably affecting the generation speed.\\

Plots for the most relevant distributions are shown using the following
pattern. The curves in the main frame represent \textsc{aMC@NLO} matched with
\textsc{HERWIG6} (solid, black), \textsc{PYTHIA6} (dashed, red), and
\textsc{HERWIG++} (dot-dashed, blue). The upper inset carries the same
information as the main frame, displayed as ratio of the three
\textsc{aMC@NLO} curves (with the same colors and patterns as described
above) over \textsc{aMC@NLO+HERWIG6}. The middle inset contains the ratio of
the \textsc{POWHEG} events showered with the three Monte Carlos (again, using
the same colors and patterns) over \textsc{aMC@NLO} +\textsc{HERWIG6}. Finally, the
lower inset displays the PDF (dot-dashed, black) and scale (dashed, red) 
uncertainties obtained automatically \cite{Frederix:2011ss} after the
\textsc{aMC@NLO+HERWIG6} run. The scale band is the envelope of the nine
curves corresponding to $(\muR,\muF)=(\kappa_R,\kappa_F) \MW$, with
$\kappa_R$ and $\kappa_F$ varied independently in the range $\{1/2,1,2\}$.
The parton-distribution band is obtained by following the asymmetric Hessian
prescription given by the PDF set in~\Bref{Martin:2009iq}. We remind the reader
that all results presented in this section are obtained by imposing the cuts
introduced in \refS{sec:cuts-sub}.

In \refF{figvbf:ptyh} the transverse-momentum and rapidity
distributions of the Higgs boson are shown. These observables are
mildly affected by extra QCD radiation, essentially because they are
genuinely NLO (in the sense that they are non-trivial in their full
kinematical ranges already at the Born level ${\cal
  O}(\alpha_\mathrm{s}^0)$). This explains why there is a general good
agreement (in terms of shape) among the results obtained with
different parton showers (main frame and upper inset), and between
those obtained with \textsc{aMC@NLO} and \textsc{POWHEG} (middle
inset). The discrepancy in the normalization of the various curves is
due to the impact of the VBF cuts on the radiation generated by the
different showers, as can be deduced by looking at the numbers
reported in Table \ref{tablevbf:ratios}. The scale and PDF bands are
fairly constant and both of the order of $\pm 3\%$, with a marginal
increase of the former at large rapidities and transverse momentum.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=1.03\textwidth]{YRHXS3_VBF/YRHXS3_VBF.HiggspTvbfcuts.eps}
\end{minipage}
\hspace{1mm}
\begin{minipage}{0.49\textwidth}
\centering
\includegraphics[width=1.08\textwidth]{YRHXS3_VBF/YRHXS3_VBF.Higgsyvbfcuts.eps}
\end{minipage}
\vspace{-1.8cm}
\caption{Higgs-boson transverse-momentum (left) and rapidity (right) distributions. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
\label{figvbf:ptyh}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\caption{Cross section after VBF cuts normalized to \textsc{aMC@NLO+HERWIG6}.}
\label{tablevbf:ratios}
\begin{tabular}{cccc}
\hline
 & \textsc{HERWIG6} & \textsc{PYTHIA6} & \textsc{HERWIG++} \\ \hline
\textsc{aMC@NLO} & 1.00 & 0.96 & 0.90 \\
\textsc{POWHEG} & 0.99 & 0.93 & 0.90 \\
\hline
\end{tabular}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Figures~\ref{figvbf:pt12}, \ref{figvbf:mphi12}, and the left panel of
\Fref{figvbf:dy12n} display quantities related to the two
hardest (tagging) jets, namely their transverse momenta, the pair
invariant mass and azimuthal distance, and the absolute value of their
rapidity difference, respectively. In spite of their being directly
related to QCD radiation, these observables are described at the NLO,
which translates in the overall agreement, up to the normalization
effect discussed above, of the results. Predictions obtained with
\textsc{PYTHIA6} tend to be marginally harder with respect to the two
\textsc{HERWIG} curves for the first-jet transverse momentum both in
\textsc{POWHEG} and in \textsc{aMC@NLO}, while the second-jet
transverse momentum is slightly softer in \textsc{aMC@NLO}. The
theoretical-uncertainty bands are constant with a magnitude of about
$\pm 3\%$ for all observables, with the exceptions of a slight
increase at large pair invariant mass ($\pm 5\%$), and of a more
visible growth, up to $\pm 10{-}15\%$, towards the upper edge of the
rapidity-difference range. For the parton distributions, this is due
to the larger uncertainties in the high-$x$ region, while the growth
in the scale error may be related to $\MW$ not being a representative
scale choice for such extreme kinematical configurations.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.j1pTvbfcuts.eps}
    \end{minipage}
    \hspace{1mm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.j2pTvbfcuts.eps}
    \end{minipage}
\vspace{-1.8cm}
    \caption{Transverse-momentum distributions for the hardest (left) and next-to-hardest  (right) jets. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:pt12}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.mj1j2vbfcuts.eps}
    \end{minipage}
    \hspace{1mm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.Dphij1-j2vbfcuts.eps}
    \end{minipage}
\vspace{-1.8cm}
    \caption{Invariant mass (left) and azimuthal separation (right) of the two tagging jets. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:mphi12}
    \centering
   %\includegraphics[width=0.48\textwidth]{}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.Dyj1-j2vbfcuts.eps}
    \end{minipage}
    \hspace{1mm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.njetsvbfcuts.eps}
    \end{minipage}
\vspace{-1.8cm}
        \caption{Rapidity separation of the two tagging jets (left) and exclusive jet-multiplicity (right). Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:dy12n}
    \centering
   %\includegraphics[width=0.48\textwidth]{}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The right panel \Fref{figvbf:dy12n} shows the exclusive
jet-multiplicity. This observable is described with NLO accuracy in the 2-jet
bin, and at the LO in the 3-jet bin. For the 2-jet bin, the pattern displayed
in the upper and middle inset closely follows that of Table
\ref{tablevbf:ratios}, while in the 3-jet bin discrepancies are of the order
of $10{-}20\%$, with \textsc{POWHEG} predicting less events than
\textsc{aMC@NLO}. Consistently with this picture, and with the formal accuracy
of the two bins, scale uncertainties are $\pm3\%$ and $\pm10\%$,
respectively. The differences between \textsc{POWHEG} and \textsc{aMC@NLO}, of
the same order of (or slightly larger than) the scale variations, can be
considered as an independent way of estimating the theoretical uncertainty
associated with higher-order corrections.

From the 4-jet bin onwards, the description is completely driven by the
leading-logarithmic (LL) accuracy of the showers, and by the tunes employed,
which is reflected in the large differences that can be observed in the
predictions given by the different event generators (for both
\textsc{aMC@NLO} and \textsc{POWHEG}). For such jet multiplicities,
theoretical-uncertainty bands are completely unrepresentative.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.j3pTvbfcuts.eps}
    \end{minipage}
    \hspace{1mm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1.1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.j3yvbfcuts.eps}
    \end{minipage}
\vspace{-1.8cm}
        \caption{Third-hardest jet transverse momentum (left) and rapidity (right) distributions. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:pty3}
    \centering
   %\includegraphics[width=0.48\textwidth]{}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The left panel of \Fref{figvbf:pty3} shows the
transverse-momentum distribution of the third-hardest jet, which is a
LO variable that can be affected by the different radiation patterns
produced by the Monte Carlos. This is indeed what can be observed in
the plot. In particular the \textsc{aMC@NLO} results display a $10{-}15\%$
dependence on the parton shower adopted. The \textsc{POWHEG} curves
are slightly closer to each other, since in this case the hardest
extra emission (the most relevant for quantities related to the
overall third-hardest jet) is performed independently of the actual
Monte Carlo employed. Furthermore, the two matching schemes differ
quite sizably for this observable, up to $\pm20{-}25\%$ at large
transverse momentum, with \textsc{POWHEG} predicting a softer spectrum
with respect to \textsc{aMC@NLO}. This discrepancy is consistent with
the normalizations of the 3-jet bin in the right panel of
\Fref{figvbf:dy12n}. Different settings in \textsc{PYTHIA6} have
been checked to induce a variation in the curves within the previously
mentioned discrepancy range, which is thus to be considered as a
measure of the matching systematics affecting the prediction of this
quantity. Scale uncertainties are quite large, compatibly with the LO
nature of this observable, and growing from $\pm10\%$ to $\pm25\%$
with increasing transverse momentum.  The rapidity distribution of the
third-hardest jet, shown in the right panel of
\Fref{figvbf:pty3} has similar features, with the
\textsc{POWHEG} samples more central than the \textsc{aMC@NLO} ones.


\Fref{figvbf:ptyv} displays features of the veto jet, defined as the hardest jet 
with rapidity ($y_{j_{\tiny\mbox{veto}}}$) lying between those ($y_{j
_1}$ and $y_{j_2}$) 
of the two tagging jets:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{equation}
    \min\left(y_{j_1},y_{j_2}\right) < y_{j_{\tiny\mbox{veto}}} < \max\left(y_{j_1},y_{j_2}\right).
\end{equation}%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.jvetopTvbfcuts.eps}
    \end{minipage}
    \hspace{1mm}
    \begin{minipage}{0.5\textwidth}
    \centering
   \includegraphics[width=1.1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.jvetoyvbfcuts.eps}
    \end{minipage}
    \vspace{-1.8cm}
        \caption{Veto-jet transverse momentum (left) and rapidity (right) distributions. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:ptyv}
    \centering
  % \includegraphics[width=0.48\textwidth]{}
\end{figure}%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This definition implies that the more central the third jet, the larger the probability 
that it be the veto jet. Since \textsc{POWHEG} predicts a more central third jet with 
respect to \textsc{aMC@NLO}, the veto condition has
the effect that the two predictions for the veto jet
are closer to each other than for the third jet; this is interesting
in view of the fact that the differences between the two approaches
can to a large extent be interpreted as matching systematics.\\
To further investigate this point, \Fref{figvbf:pve} shows the veto
probability     
$P_{\tiny\mbox{veto}}(p_{\tiny\mbox{T,veto}})$, defined as
\begin{equation}
P_{\tiny\mbox{veto}}(p_{\tiny\mbox{T,veto}})=\frac{1}{\sigma_{\tiny\mbox{NLO}}}\int_{p_{\tiny\mbox{T,veto}}}^\infty
\rd p_{\tiny\mbox T} \,\frac{\rd\sigma}{\rd p_{\tiny\mbox T}},
\end{equation}
where $\sigma_{\tiny\mbox{NLO}}=(0.388\pm0.002)\Upb$ is the (fixed-order) 
NLO cross section within VBF cuts \footnote{The \textsc{aMC@NLO+HERWIG6} cross 
section within VBF cuts is $0.361\Upb$. The cross sections relevant to the other parton showers and to \textsc{POWHEG} 
can be deduced using the ratios in Table \ref{tablevbf:ratios}.}.\\
The excess of events between $p_{\tiny\mbox T}\sim20\UGeV$ and
$p_{\tiny\mbox T}\sim40\UGeV$ in \textsc{aMC@NLO} matched to
\textsc{PYTHIA6} and \textsc{HERWIG++} translates in a slightly larger
cumulative probability of passing the veto cut. The \textsc{POWHEG}
curves are lower, with up to $20{-}25\%$ discrepancy with respect to
\textsc{aMC@NLO} at medium-high transverse momentum. Scale variation
are compatible with a LO prediction, with a fairly constant magnitude
of $\pm15\%$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
%    \vspace{5cm}
    \begin{minipage}{0.98\textwidth}
    \centering
   \includegraphics[width=1\textwidth]{YRHXS3_VBF/YRHXS3_VBF.P-vetovbfcuts.eps}
    \end{minipage}
\vspace{-3cm}
        \caption{Veto probability. Main frame and upper inset: \textsc{aMC@NLO} results; middle inset: ratio of \textsc{POWHEG} results over \textsc{aMC@NLO+HERWIG6}; lower inset: \textsc{aMC@NLO+HERWIG6} scale and PDF variations.}
    \label{figvbf:pve}
    \centering
  % \includegraphics[width=0.48\textwidth]{}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 \begin{table}
\caption{VBF inclusive cross sections at $8$ and $13\UTeV$ calculated
  using MSTW2008NLO PDF set.  Comparison among different calculations 
at NLO QCD for a selected number of Higgs-boson masses. The
$t$--$u$-channel interferences and the $\PQb$-quark
contributions are included in the computation.}
\label{tab:YRHXS3_VBF_NNLO8_13}%
%\renewcommand{\arraystretch}{1.1}%
\setlength{\tabcolsep}{1.5ex}%
\begin{center}
\begin{tabular}{lcccc}
\hline \rule{0ex}{2.7ex}
 $ \MH $  &   $ \sigma^{\mathrm{NLO QCD}}_{\mathrm{VBF@NNLO}} $ &  $
 \sigma^{\mathrm{NLO QCD}}_{\mathrm{HAWK}}  $  & $ \sigma^{\mathrm{NLO
     QCD} + \mathrm{NLO EW}}_{\mathrm{HAWK}} $  &  $
 \sigma^{\mathrm{NLO QCD}}_{\mathrm{VBFNLO}}  $   \\ 
  $\mbox{[GeV]}$   &   [fb]  &  [fb]   & [fb] &  [fb]   \\ 
 \hline 
\multicolumn{4}{l}{$8\UTeV$} \\
\hline
120 &  1737 $\pm$       0.6 & 1732 $\pm$ 0.8 & 1651 $\pm$ 0.9  & 1743 $\pm$ 0.5  \\
122 &  1706 $\pm$       0.6 & 1701 $\pm$ 0.7 & 1623 $\pm$ 0.8  & 1711 $\pm$ 0.5  \\
124 &  1675 $\pm$       0.5 & 1671 $\pm$ 0.7 & 1594 $\pm$ 0.8  & 1681 $\pm$ 0.5  \\
126 &  1646 $\pm$       0.5 & 1641 $\pm$ 0.7 & 1566 $\pm$ 0.8  & 1651 $\pm$ 0.5  \\
128 &  1617 $\pm$       0.5 & 1613 $\pm$ 0.7 & 1539 $\pm$ 0.8  & 1622 $\pm$ 0.5  \\
130 &  1588 $\pm$       0.5 & 1585 $\pm$ 0.7 & 1513 $\pm$ 0.8  & 1593 $\pm$ 0.5  \\
\hline
\multicolumn{4}{l}{$13\UTeV$} \\
\hline
120 & 4164 $\pm$    2.5 & 4162 $\pm$ 2.2 & 3938 $\pm$ 2.5 & 4168 $\pm$ 2.1  \\
122 & 4096 $\pm$    2.1 & 4098 $\pm$ 2.2 & 3878 $\pm$ 2.5 & 4103 $\pm$ 2.0  \\
124 & 4034 $\pm$    1.8 & 4035 $\pm$ 2.2 & 3820 $\pm$ 2.4 & 4040 $\pm$ 1.8  \\
126 & 3975 $\pm$    2.0 & 3974 $\pm$ 2.2 & 3762 $\pm$ 2.4 & 3979 $\pm$ 1.8  \\
128 & 3914 $\pm$    2.3 & 3913 $\pm$ 2.3 & 3705 $\pm$ 2.4 & 3919 $\pm$ 1.7  \\
130 & 3854 $\pm$    1.6 & 3857 $\pm$ 2.0 & 3651 $\pm$ 2.2 & 3860 $\pm$ 1.6  \\
\hline

\end{tabular}

\end{center}
\end{table}

%\subsubsubsection{aMC@NLO Comparisons}
%\label{sec:aMCNLOcomp-sub-sub-sub}

%\newpage

