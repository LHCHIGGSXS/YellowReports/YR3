
% Introduction
% -------------

\section{Introduction%
\footnote{S.~Heinemeyer, C.~Mariotti, G.~Passarino and R.~Tanaka.}} 

\newcommand{\ggF}{\ensuremath{\Pg\Pg \to \PH}}
\newcommand{\VBF}{\ensuremath{\PQq\PQq' \to \PQq\PQq'\PH}}
\newcommand{\VH}{\ensuremath{\PQq\PAQq \to \PW\PH/\PZ\PH}}
\newcommand{\ttH}{\ensuremath{\PQq\PAQq/\Pg\Pg \to \PQt\PAQt\PH}}

% Introduction and LHC

% Higgs search results 2012
% -------------

The 4th of July 2012 ATLAS and CMS announced that they had discovered
a new particle with a mass around $125 \UGeV$~\cite{Aad:2012tfa,Chatrchyan:2012ufa}.
This came after only a bit more than one year of data taken at center of
mass energies of $7$ and $8\UTeV$. 
The discovery has been made while searching for the Higgs boson, the particle
linked to the Brout-Englert-Higgs
mechanism~\cite{Englert:1964et,Higgs:1964ia,Higgs:1964pj,Guralnik:1964eu,Higgs:1966ev,Kibble:1967sv}.   
The outstanding performance of the LHC in 2012
delivered in total four times as much 8 TeV data 
as was used in the discovery analyses.  The  experiments were able
thus to present new preliminary results confirming the existence of
the particles and measuring the properties. 
The new particle is a boson, since it decays into two photons, $2\,\PZ$ and
$2\,\PW$ bosons, and it could possibly be the Standard Model (SM) Higgs boson.
More data and new theoretical approaches will help us in the future
to establish the nature of this particle and, more important, if there is new physics 
around the corner. 

At the Moriond conference this year, 2013, ATLAS and CMS presented 
the results on five main decay modes:
$\PH \to 2 \PGg$, $\PH \to \PZ\PZ \to 4\ell$,  $\PH \to \PW\PW \to
\ell\nu \ell\nu$, $\PH \to \PGt\PGt$, and $\PH \to \PAQb\PQb$ channels, with 
an integrated luminosities of up to 5 fb$^{-1}$ at $\sqrt{s}= 7\UTeV$ and up to
21 fb$^{-1}$ at $\sqrt{s} = 8\UTeV$.  
The $\PH \to 2\PGg$ and $\PH \to \PZ\PZ \to 4\ell$ channels allow to
measure the mass of the boson with very high precision. 
ATLAS measures $125.5 \pm 0.2 ({\rm stat.}) ^{+0.5}_{-0.6} ({\rm syst.}) \UGeV$~\cite{ATLAS-CONF-2013-014}, 
CMS measures a mass of $125.7 \pm 0.3 ({\rm stat.}) \pm 0.3 ({\rm
  syst.}) \UGeV$~\cite{CMS-PAS-HIG-13-005}.  
Figure~\ref{fig:mu_intro} shows the signal strength for the various
decay channels for ATLAS and CMS.
For ATLAS the combined signal strength is determined to be 
$\mu=1.30 \pm 0.13 ({\rm stat.}) \pm 0.14 ({\rm syst.})$ 
at the measured mass value~\cite{ATLAS-CONF-2013-034}.
For CMS the combined signal strength is determined to be 
$\mu = 0.80 \pm 0.14$ at the measured mass value~\cite{CMS-PAS-HIG-13-005}.  

Whether or not the new particle is a Higgs boson is demonstrated by how it 
interacts with other particles and its own quantum properties. For example, a 
Higgs boson is postulated to have no spin and in the SM its parity,
a measure of how its mirror image behaves, should be positive. ATLAS and CMS
have compared a number of alternative spin-parity ($\mathrm{J}^\mathrm{P}$) assignments for this
particle and, in pairwise hypothesis tests, the hypothesis of no spin and
positive parity  ($0^+$) is consistently favored against the
alternative hypotheses \cite{Chatrchyan:2012jja,CMS-PAS-HIG-13-005}. 

\medskip
This report presents improved inclusive cross section calculation at
$7$ and $8\UTeV$ in
the SM and its minimal supersymmetric extension, together with the calculation
of the relevant decay channels, in particular around the measured mass value
of $125 - 126 \UGeV$. 
Results and extensive discussions are also presented for
a heavy (SM-like) Higgs boson, including the correct treatment via the Complex
Pole Scheme (CPS) as well as interference effects in particular in the 
$\PW\PW$ and $\PZ\PZ$ channels.

In view of the newly discovered particle the property determination becomes
paramount. This report presents the interim recommendation to extract
couplings as well as the spin and the parity of the new particle. 
For the determination of the coupling strength factors the correlated
uncertainties for the decay calculations have to be taken into account and
corresponding descriptions and results for the uncertainties at the level of
partial widths are included in this report.
Given that now the theoretical uncertainties from QCD scale and
PDF+$\alphas$ dominate the experimental systematic 
uncertainties, a new proposal is presented on how to treat and evaluate the
theory uncertainties due to the QCD scale.

The report furthermore includes the state-of-the-art description of the relevant
Monte Carlo generators, in particular for the difficult task of
simulating the Higgs production together with (two) jets in gluon gluon fusion
as compared to Vector boson fusion. The treatment of jet bin uncertainties is
also discussed.

%The best way to consider the uncertainties on the partial widths.
%It presents the first ad interim recommendation to extract couplings and the spin and parity.
%It present also the status of the art of the Monte Carlo generator for
%the difficult task of simulating the H produced in gluongluon 
%fusion plus 2 jets, as  background to the measurements of the Higgs when produced in  Vector boson Fusion.
%An extensive discussion on the high mass Higgs is presented. CPS,
%interference etc that open the challenging chapter on BSM Higgs.
%Finally, given that the statistical uncertainties is not dominating
%anymore, a new proposal on how to treat more 
%correctly the theoretical uncertainty due to the QCD scale.
%The future:  HARD? IS THERE ANY?

\medskip
This report tries to lay the path towards a further exploration of the Higgs
sector of the (yet to be determined) underlying model. First, this includes the
(re)quest for even more refined calculations of inclusive and differential
cross sections in the SM, for heavy (SM-like) Higgs bosons, as well as in as
many BSM models as possible. Dedicated efforts are needed to match the
required accuracy in the SM, the MSSM and other BSM models. Methods for a
reliable estimate of remaining uncertainties are needed. 
Second, this includes prescriptions for the correct extraction of the
properties of the newly discovered particle. While we report on substantial
progress on these topics, it also becomes clear that a lot of dedicated effort
will be needed to match the challenges electroweak symmetry breaking holds for
us. 

\begin{figure}
\begin{center}
\includegraphics[width=0.45\textwidth]{YRHXS3_Introduction/atlas_mu_decay.eps}
\includegraphics[width=0.45\textwidth]{YRHXS3_Introduction/CMS_mu_decay.eps}
\caption{The signal strength for the individual channel and their
  combination. The values of $\mu$ are given for $\MH=125.5 \UGeV$
  for ATLAS and for $\MH=125.7 \UGeV$ for CMS. }
\label{fig:mu_intro}
\end{center}
\end{figure}


